package com.example.project04.model.checkout.dto;

import java.util.Date;

public class CheckoutDTO {
	private String pname;
	private int price2;
	private int cseq;
	private String id;
	private int pseq;
	private int sz230mm;
	private int sz240mm;
	private int sz250mm;
	private int sz260mm;
	private int sz270mm;
	private int sz280mm;
	private int amount;
	private int total;
	private String image_url;
	private String result;
	private Date indate;
	private String show;
	
	public CheckoutDTO() {
	}

	public CheckoutDTO(String pname, int price2, int cseq, String id, int pseq, int sz230mm, int sz240mm, int sz250mm,
			int sz260mm, int sz270mm, int sz280mm, int amount, int total, String image_url, String result, Date indate,
			String show) {
		super();
		this.pname = pname;
		this.price2 = price2;
		this.cseq = cseq;
		this.id = id;
		this.pseq = pseq;
		this.sz230mm = sz230mm;
		this.sz240mm = sz240mm;
		this.sz250mm = sz250mm;
		this.sz260mm = sz260mm;
		this.sz270mm = sz270mm;
		this.sz280mm = sz280mm;
		this.amount = amount;
		this.total = total;
		this.image_url = image_url;
		this.result = result;
		this.indate = indate;
		this.show = show;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public int getPrice2() {
		return price2;
	}

	public void setPrice2(int price2) {
		this.price2 = price2;
	}

	public int getCseq() {
		return cseq;
	}

	public void setCseq(int cseq) {
		this.cseq = cseq;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getPseq() {
		return pseq;
	}

	public void setPseq(int pseq) {
		this.pseq = pseq;
	}

	public int getSz230mm() {
		return sz230mm;
	}

	public void setSz230mm(int sz230mm) {
		this.sz230mm = sz230mm;
	}

	public int getSz240mm() {
		return sz240mm;
	}

	public void setSz240mm(int sz240mm) {
		this.sz240mm = sz240mm;
	}

	public int getSz250mm() {
		return sz250mm;
	}

	public void setSz250mm(int sz250mm) {
		this.sz250mm = sz250mm;
	}

	public int getSz260mm() {
		return sz260mm;
	}

	public void setSz260mm(int sz260mm) {
		this.sz260mm = sz260mm;
	}

	public int getSz270mm() {
		return sz270mm;
	}

	public void setSz270mm(int sz270mm) {
		this.sz270mm = sz270mm;
	}

	public int getSz280mm() {
		return sz280mm;
	}

	public void setSz280mm(int sz280mm) {
		this.sz280mm = sz280mm;
	}

	public int getAmount() {
		amount = sz230mm+sz240mm+sz250mm+sz260mm+sz270mm+sz280mm;
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	public String getShow() {
		return show;
	}

	public void setShow(String show) {
		this.show = show;
	}

	@Override
	public String toString() {
		return "CheckoutDTO [pname=" + pname + ", price2=" + price2 + ", cseq=" + cseq + ", id=" + id + ", pseq=" + pseq
				+ ", sz230mm=" + sz230mm + ", sz240mm=" + sz240mm + ", sz250mm=" + sz250mm + ", sz260mm=" + sz260mm
				+ ", sz270mm=" + sz270mm + ", sz280mm=" + sz280mm + ", amount=" + amount + ", total=" + total
				+ ", image_url=" + image_url + ", result=" + result + ", indate=" + indate + ", show=" + show + "]";
	}

}
