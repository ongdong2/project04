package com.example.project04.model.product.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.dealer.dto.DealerDTO;
import com.example.project04.model.product.dto.ProductDTO;

@Repository
public class ProductDAOImpl implements ProductDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<ProductDTO> listProduct(String keyword,int start,int end) throws Exception{
		Map<String,Object> map=new HashMap<>();
		map.put("keyword", "'%"+keyword+"%'");
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("product.list",map);
	}

	@Override
	public ProductDTO detailProduct(int pseq) {
		return sqlSession.selectOne("product.detail_product", pseq);
	}

	@Override
	public void updateProduct(ProductDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteProduct(int pseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertProduct(ProductDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String fileInfo(int pseq) {
		return sqlSession.selectOne("product.fileInfo", pseq);
	}

	@Override
	public DealerDTO brandProduct(int dseq) {
		return sqlSession.selectOne("product.brand", dseq);
	}

	@Override
	public List<ProductDTO> home_list() {					//Home
		return sqlSession.selectList("product.home_list");
	}

	@Override
	public List<ProductDTO> man_list() {
		return sqlSession.selectList("product.man_list");
	}

	@Override
	public List<ProductDTO> woman_list() {
		return sqlSession.selectList("product.woman_list");
	}

	@Override
	public int countArticle(String keyword) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("keyword", "'%"+keyword+"%'");
		return sqlSession.selectOne("product.countArticle",map);
	}

	@Override
	public List<ProductDTO> public_list() {
		return sqlSession.selectList("product.public_list");
	}

	@Override
	public List<ProductDTO> home_newlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("product.home_newlist");
	}

}
