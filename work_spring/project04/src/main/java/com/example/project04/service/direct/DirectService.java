package com.example.project04.service.direct;

import java.util.List;

import com.example.project04.model.direct.dto.DirectDTO;

public interface DirectService {
	public List<DirectDTO> directMoney();
	public void insert(DirectDTO dto);
	public List<DirectDTO> listDirect(String id);
	public void show(String id);
	public int sumMoney(String id);
	public void delete(String id);
}
