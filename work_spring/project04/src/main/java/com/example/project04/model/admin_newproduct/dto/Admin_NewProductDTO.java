package com.example.project04.model.admin_newproduct.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class Admin_NewProductDTO {
	private int nseq;
	private String nname;
	private String sort;
	private int price1;
	private int price2;
	private int dseq;
	private String content;
	private String image_url;
	private MultipartFile image;
	
	private Date indate;
	private int sz230mm;
	private int sz240mm;
	private int sz250mm;
	private int sz260mm;
	private int sz270mm;
	private int sz280mm;
	private int total;
	private int sumtotal;
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getSumtotal() {
		return sumtotal;
	}
	public void setSumtotal(int sumtotal) {
		this.sumtotal = sumtotal;
	}
	public int getNseq() {
		return nseq;
	}
	public void setNseq(int nseq) {
		this.nseq = nseq;
	}
	public String getNname() {
		return nname;
	}
	public void setNname(String nname) {
		this.nname = nname;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getPrice1() {
		return price1;
	}
	public void setPrice1(int price1) {
		this.price1 = price1;
	}
	public int getPrice2() {
		return price2;
	}
	public void setPrice2(int price2) {
		this.price2 = price2;
	}
	public int getDseq() {
		return dseq;
	}
	public void setDseq(int dseq) {
		this.dseq = dseq;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	public int getSz230mm() {
		return sz230mm;
	}
	public void setSz230mm(int sz230mm) {
		this.sz230mm = sz230mm;
	}
	public int getSz240mm() {
		return sz240mm;
	}
	public void setSz240mm(int sz240mm) {
		this.sz240mm = sz240mm;
	}
	public int getSz250mm() {
		return sz250mm;
	}
	public void setSz250mm(int sz250mm) {
		this.sz250mm = sz250mm;
	}
	public int getSz260mm() {
		return sz260mm;
	}
	public void setSz260mm(int sz260mm) {
		this.sz260mm = sz260mm;
	}
	public int getSz270mm() {
		return sz270mm;
	}
	public void setSz270mm(int sz270mm) {
		this.sz270mm = sz270mm;
	}
	public int getSz280mm() {
		return sz280mm;
	}
	public void setSz280mm(int sz280mm) {
		this.sz280mm = sz280mm;
	}
	@Override
	public String toString() {
		return "Admin_NewProductDTO [nseq=" + nseq + ", nname=" + nname + ", sort=" + sort + ", price1=" + price1
				+ ", price2=" + price2 + ", dseq=" + dseq + ", content=" + content + ", image_url=" + image_url
				+ ", image=" + image + ", indate=" + indate + ", sz230mm=" + sz230mm + ", sz240mm=" + sz240mm
				+ ", sz250mm=" + sz250mm + ", sz260mm=" + sz260mm + ", sz270mm=" + sz270mm + ", sz280mm=" + sz280mm
				+ ", total=" + total + ", sumtotal=" + sumtotal + "]";
	}
	
	
}
