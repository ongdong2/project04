package com.example.project04.service.admin;

import java.util.List;

import com.example.project04.model.admin.dto.AdminMemberDTO;
import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_member.dto.Admin_MemberDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

public interface AdminService {
	public String loginCheck(AdminMemberDTO dto);
	public List<Admin_MemberDTO> listMember();
	public List<Admin_DealerDTO> listDealer();
	public List<Admin_ProductDTO> listProduct();
	public List<Admin_NewProductDTO> listNewproduct();
	public List<Admin_ProductDTO> orderlist();
	public List<Admin_ProductDTO> orderstocklist();
	
}
