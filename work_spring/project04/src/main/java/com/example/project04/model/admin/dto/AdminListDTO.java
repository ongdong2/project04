package com.example.project04.model.admin.dto;

public class AdminListDTO {
	public String id;
	public String mname;
	public String email;
	public int dseq;
	public String dname;
	public String phone;
	public int pseq;
	public String pname;
	public String price2;
	public AdminListDTO() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getDseq() {
		return dseq;
	}
	public void setDseq(int dseq) {
		this.dseq = dseq;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getPseq() {
		return pseq;
	}
	public void setPseq(int pseq) {
		this.pseq = pseq;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getPrice2() {
		return price2;
	}
	public void setPrice2(String price2) {
		this.price2 = price2;
	}
	@Override
	public String toString() {
		return "AdminListDTO [id=" + id + ", mname=" + mname + ", email=" + email + ", dseq=" + dseq + ", dname="
				+ dname + ", phone=" + phone + ", pseq=" + pseq + ", pname=" + pname + ", price2=" + price2 + "]";
	}
	
}
