package com.example.project04.model.dealer.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.dealer.dto.DealerDTO;

@Repository
public class DealerDAOImpl implements DealerDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<DealerDTO> listDealer() {
		return sqlSession.selectList("dealer.brand_list");
	}

}
