<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
</head>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
	$(function() {
		
		//문의하기 작성란으로 이동
		$("#btnInquiry").click(function(){
			location.href="${path}/inquiry/inquiry.do";
		});
		
	
		/* 1번!!!btnAnswer버튼을누르면함수가실행되고
		btnAnswer2함수가실행되게한다
		btnAnswer2함수가실행될때 ajax를 사용한다 */
		
		/* 2번!!!히든태그를사용해서해당게시물에대한qseq를알아낸다 */
	
	});
	function list(page,search_option,keyword){
		location.href="${path}/inquiry/list.do?curPage="+page+"&search_option="+search_option+"&keyword="+keyword;
	}
	
	function show(qseq) {	
		hideandshow = $("#hidden_" + qseq).css("display");
			console.log(qseq);
		if (hideandshow == "none") {
			$("#hidden_" + qseq).css("display", "");
			
			var param={"qseq":qseq}
			console.log(param)
			$.ajax({
				type:"post",
				url:"${path}/answer/answertext.do",
				data:param,
				success: function(result){
					console.log(result)
					 $("#div1_"+qseq).html(result); 
				}
			});
		} else {
			$("#hidden_" + qseq).css("display", "none");
		}
	}
	
	function delete2(qseq) {	
		if(confirm("삭제하시겠습니까?")){
			location.href="${path}/inquiry/delete.do?qseq="+qseq;
		}
	}
	/* var keyword=${map.keyword}
	function Search(keyword){
		document.form2.action="${path}/inquriy/list.do?keyword="+keyword;
		document.form2.submit();
	} */
	
 	function answer(qseq){
		location.href="${path}/answer/insert.do?qseq="+qseq;
	}
	 
</script>

<title>Insert title here</title>
<!-- catg header banner section -->
<section id="aa-catg-head-banner">
	<div class="aa-catg-head-banner-area">
		<div class="container"></div>
	</div>
</section>
<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-18">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table">
               <div class="table-responsive">
			<h2 align="center">상품문의 게시판</h2>
                  <form name="form1" method="post" action="${path}/inquiry/list.do">
			<table class="table table-bordered table-hover"style="width:1000px"  align="center" >
			<tr>
			<td colspan="5">
			<Strong>검색 :</Strong> <select name="search_option">
		<option value="id"
<c:if test="${map.search_option == 'id'}">selected</c:if>
		>작성자</option>
		<option value="subject" 
<c:if test="${map.search_option == 'subject'}">selected</c:if>
		>문의제목</option>
		<option value="content" 
<c:if test="${map.search_option == 'content'}">selected</c:if>
		>문의내역</option>
		<option value="all" 
<c:if test="${map.search_option == 'all'}">selected</c:if>
		>이름+내용+제목</option>
	</select>
	<input name="keyword" value="${map.keyword}">
	<input class="btn btn-primary" type="submit" id="btnSearch" value="조회" >				
			</td>
			</tr>
			
			<tr align="center">
			<th align="center">번호</th>
					<th align="center">답변상태</th>
					<th align="center">문의제목</th>
					<th align="center">작성자</th>
					<th align="center">작성일자</th>
						</tr>
		<c:forEach var="row" items="${map.list}">
						<tr>
<td>${row.qseq}</td>
<c:if test="${row.answershow == '미답변'}">
<td><a href="${path}/inquiry/view.do?qseq=${row.qseq}&curPage=${map.pager.curPage}&id=${row.id}" style="color:red;">${row.answershow}</a></td>
</c:if>
<c:if test="${row.answershow == '답변완료'}">
<td><a style="color:green;" href="${path}/inquiry/view.do?qseq=${row.qseq}&curPage=${map.pager.curPage}&id=${row.id}">${row.answershow}</a></td>
</c:if>


<td><a href="#" class="viewhidden" onclick="show('${row.qseq}')" class="button">${row.subject}</a></td>
<td>${row.id}</td>

<td><fmt:formatDate value="${row.indate}" pattern="yyyy-MM-dd a HH:mm:ss" /></td>
									</tr>
					<tr id="hidden_${row.qseq}" style="display: none">
					<td colspan="5" align="center"><Strong>작성일자&nbsp;&nbsp;:&nbsp;&nbsp; </Strong><fmt:formatDate value="${row.indate}" pattern="yyyy-MM-dd a HH:mm:ss" /><br>
						<p align="left" style="color: red">문의</p>
					 	<p align="left"><strong>${row.id}&nbsp;&nbsp;:&nbsp;&nbsp;${row.content}</strong></p> 
						<!-- 답변달기 -->
							
							
							<!-- 내역삭제 -->	
					<div align="left" id="div1_${row.qseq}">
											
					</div>
				<c:if test="${sessionScope.id == 'admin1' }">
				<button type="button" onclick="delete2('${row.qseq}')">내역삭제</button><br>
				</c:if>
				</td>   
				</tr>
				</c:forEach>
			<tr>
		<td colspan="5" align="center">
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('1','${map.search_option}','${map.keyword}')"><Strong>[처음]</Strong></a>
			</c:if>
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('${map.pager.prevPage}','${map.search_option}','${map.keyword}')">
				[이전]</a>
			</c:if>
			<c:forEach var="num" 
				begin="${map.pager.blockBegin}"
				end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
					<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
						<span style="color:red;">${num}</span>
					</c:when>
					<c:otherwise>
						<a href="#" onclick="list('${num}','${map.search_option}','${map.keyword}')">${num}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${map.pager.curBlock < map.pager.totBlock}">
				<a href="#" 
				onclick="list('${map.pager.nextPage}','${map.search_option}','${map.keyword}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage < map.pager.totPage}">
				<a href="#" 
				onclick="list('${map.pager.totPage}','${map.search_option}','${map.keyword}')"><Strong>[마지막]</Strong></a>
			</c:if>
		</td>
	</tr>
	</table>
	<c:if test="${sessionScope.id != 'admin1' }">
			<div align="left" style="margin-left: 970px;"><button class="btn btn-primary pull-left" type="button" id="btnInquiry"><Strong>문의하기</Strong></button></div>
			</c:if>
	</form>
	
                </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   
 </section>
 <!-- / Cart view section -->




			
	
			







<%@ include file="../include/Footer.jsp"%>

</body>
</html>