package com.example.project04.service.product;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.dealer.dto.DealerDTO;
import com.example.project04.model.product.dao.ProductDAO;
import com.example.project04.model.product.dto.ProductDTO;

@Service
public class ProductServiceImpl implements ProductService {

	@Inject
	ProductDAO productDao;
	
	@Override
	public List<ProductDTO> listProduct(String keyword,int start,int end) throws Exception{
		return productDao.listProduct(keyword,start,end);
	}

	@Override
	public ProductDTO detailProduct(int pseq) {
		return productDao.detailProduct(pseq);
	}

	@Override
	public void updateProduct(ProductDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteProduct(int pseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void insertProduct(ProductDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String fileInfo(int pseq) {
		return productDao.fileInfo(pseq);
	}

	@Override
	public DealerDTO brandProduct(int dseq) {
		return productDao.brandProduct(dseq);
	}

	
	@Override
	public List<ProductDTO> home_list() {		//Home
		return productDao.home_list();
	}

	@Override
	public List<ProductDTO> man_list() {
		return productDao.man_list();
	}

	@Override
	public List<ProductDTO> woman_list() {
		return productDao.woman_list();
	}

	@Override
	public int countArticle(String keyword) throws Exception {
		return productDao.countArticle(keyword);
	}

	@Override
	public List<ProductDTO> public_list() {
		return productDao.public_list();
	}

	@Override
	public List<ProductDTO> home_newlist() {
		// TODO Auto-generated method stub
		return productDao.home_newlist();
	}

}
