package com.example.project04.service.email;

import com.example.project04.model.email.EmailDTO;

public interface EmailService {

	public void sendMail(EmailDTO dto);
	
}
