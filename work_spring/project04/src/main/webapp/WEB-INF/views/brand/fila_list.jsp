<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>
	<%@ include file="../include/header.jsp" %>  
	<%@ include file="../include/menu.jsp" %>
 
  <!-- catg header banner section -->

   <img src="${path}/resources/img/banner/fila_banner.gif"alt="fashion img" height="300" style="margin-left: auto; margin-right: auto; display: block;">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
<!--         <h2>Fashion</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">Women</li>
        </ol> -->
      </div>
     </div>
   </div>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
 

              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
              
                <!-- start single product item -->
                <c:forEach var="fila" items="${fila}">
                <c:if test=""></c:if>
                <li>
                  <figure>
                    <a class="aa-product-img" href="${path}/product/detail.do?pseq=${fila.pseq}"><img src="${path}/resources/img/shoes/${fila.image_url}" width="200px" height="300px" alt="이언경"></a>
                    <figcaption>
                      <h4 class="aa-product-title"><a href="${fila.pname}"></a></h4>
                      <span class="aa-product-price">￦<fmt:formatNumber value="${fila.price2}"/></span>
                      <p class="aa-product-descrip">${fila.content}</p>
                    </figcaption>
                  </figure>
				                        
                  <!-- product badge -->
                </li>
                </c:forEach>

              </ul>

              
            </div>
            <div class="aa-product-catg-pagination">
              <nav>
                <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li>
                    <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Category</h3>
              <ul class="aa-catg-nav">
                <li><a href="${path}/brand/catg1.do?dseq=2">남성신발</a></li>
                <li><a href="${path}/brand/catg2.do?dseq=2">여성신발</a></li>
                <li><a href="${path}/brand/catg3.do?dseq=2">남녀공용</a></li>
              </ul>
            </div>
           
            <!-- single sidebar -->
                        <!-- Category 하단부 없애기! -->
          </aside>
          </div>
          </div>
          </div>
  </section>
  <!-- Category 하반부 없애기! -->

	<%@ include file="../include/Footer.jsp" %>  
	
  </body>
</html>