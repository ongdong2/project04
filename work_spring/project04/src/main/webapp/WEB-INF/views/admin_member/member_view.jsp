<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
   
    $("#btnDelete").click(function(){
       
        if(confirm("선택 회원을 삭제하시겠습니까?")){
            document.form1.action = "${path}/project04/admin_member/delete.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_member/list.do";
    });
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">회원상세정보</div>
				<div class="pull-right">
					<span class="badge badge-info">${dto.id }</span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>아이디</th>
							<td>${dto.id }</td>
							</tr>
							<tr>
							<th>비밀번호</th>
							<td>${dto.passwd }</td>
							</tr>
							<tr>
							<th>이름</th>
							<td>${dto.mname }</td>
							</tr>
							<tr>
							<th>이메일</th>
							<td>${dto.email }</td>
							</tr>
							<tr>
							<th>휴대전화</th>
							<td>${dto.phone1 }</td>
							</tr>
							<tr>
							<th>자택우편번호</th>
							<td>${dto.zipcode1 }</td>
							</tr>
							<tr>
							<th>자택주소</th>
							<td>${dto.zip_num1 }</td>
							</tr>
							<tr>
							<th>자택주소상세</th>
							<td>${dto.address1 }</td>
							</tr>
							<tr>
							<th>직장우편번호</th>
							<td>${dto.zipcode2 }</td>
							</tr>
							<tr>
							<th>직장주소</th>
							<td>${dto.zip_num2 }</td>
							</tr>
							<tr>
							<th>직장주소상세</th>
							<td>${dto.address2 }</td>
							</tr>
							<tr>
							<th>가입일자</th>
							<td><fmt:formatDate value="${dto.indate }" pattern="yyyy-MM-dd
							HH:mm:ss" /></td>
							</tr>
							<tr>
							<td colspan="2" align="center">
							<input type="hidden" value="${dto.id }" name="id">
								<input type="button" value="삭제" id="btnDelete">
								<input type="button" value="목록" id="btnList">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>