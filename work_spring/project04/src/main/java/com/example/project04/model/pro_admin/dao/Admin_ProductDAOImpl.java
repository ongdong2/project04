package com.example.project04.model.pro_admin.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Repository
public class Admin_ProductDAOImpl implements Admin_ProductDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public List<Admin_ProductDTO> listProduct(int start,int end) {
		Map<String,Object> map=new HashMap<>();
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("admin_product.product_list",map);
	}

	@Override
	public void insertProduct(Admin_ProductDTO dto) {
		sqlSession.insert("admin_product.product_insert", dto);
		
	}

	@Override
	public Admin_ProductDTO viewProduct(int pseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_product.viewProduct", pseq);
	}

	@Override
	public void deleteProduct(int pseq) {
		sqlSession.delete("admin_product.product_delete",pseq);
		
	}

	@Override
	public void updateProduct(Admin_ProductDTO dto) {
		sqlSession.update("admin_product.product_edit", dto);
		
	}

	@Override
	public String fileInfo(int pseq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Admin_ProductDTO detailProduct(int pseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_product.detail_product", pseq);
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_product.countArticle");
	}

	

	@Override
	public void insert_Size(Admin_ProductDTO dto) {
		sqlSession.insert("admin_product.insert_size", dto);
		
	}

	@Override
	public Admin_ProductDTO orderProduct(int pseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_product.order_product", pseq);
	}

	@Override
	public void insert_stock(Admin_ProductDTO dto) {
		sqlSession.insert("admin_product.order_stock", dto);
		
	}
	@Override
	public List<Admin_ProductDTO> ordermoney() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_product.ordermoney");
	}

	@Override
	public void deleteOrder(int pseq) {
		// TODO Auto-generated method stub
		sqlSession.delete("admin_product.deleteorder", pseq);
	}

	@Override
	public List<Admin_ProductDTO> needlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_product.needlist");
	}

	

	

}
