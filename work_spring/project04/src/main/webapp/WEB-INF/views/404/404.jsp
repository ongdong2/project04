<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>


<!-- 404 error section -->
<section id="aa-error">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="aa-error-area"
					style="padding-top: 50px; padding-bottom: 50px;">
					<h2>404</h2>
					<img src="${path}/resources/img/banner/404.jpg"
						alt="cart_banner.jpg"><br> <span>서비스 이용에 불편을 드려
						죄송합니다. </span>
					<p>
						입력하신 페이지가 삭제되었거나 이름이 변경되었거나 일시적으로 사용할 수 없습니다.<br> 먼저 'F5'키를
						이용하여 새로고침 해보시고 페이지의 주소를 올바르게 입력했는지 다시 한번 확인해 보시기 바랍니다.
					</p>
					<a href="${path}/"> 초기화면으로 가기</a>
				</div>
			</div>
		</div>
		<br>
		<br>
		<!-- 밑공간 여백 -->
	</div>
</section>
<!-- / 404 error section -->




<%@ include file="../include/Footer.jsp"%>
</body>
</html>