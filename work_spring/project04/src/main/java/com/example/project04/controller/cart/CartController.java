package com.example.project04.controller.cart;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.project04.model.cart.dto.CartDTO;
import com.example.project04.service.cart.CartService;

@Controller
@RequestMapping("cart/*")
public class CartController {

	@Inject
	CartService cartService;
	
	@RequestMapping("insert.do")
	public String insert(HttpSession session, @ModelAttribute CartDTO dto) {
		String id=(String)session.getAttribute("id");
		dto.setId(id);
		cartService.insert(dto);
		return "redirect:/cart.do";
	}
	
	@RequestMapping("delete.do")
	public String delete(HttpSession session, @RequestParam int cseq) {
		if(session.getAttribute("id") != null)
			cartService.delete(cseq);
		return "redirect:/cart.do";
	}
	
	@RequestMapping("deleteSel.do")
	public String deleteSel(HttpSession session, int [] cseq) {
	
		for(int i=0; i<cseq.length; i++) {
			cartService.deleteSel(cseq[i]);
		}
		return "redirect:/cart.do";
	}

	
	@RequestMapping("deleteAll.do")
	public String deleteAll(HttpSession session) {
		String id=(String)session.getAttribute("id");
		if(id != null) {
			cartService.deleteAll(id);
		}
		return "redirect:/cart.do";
	}

}
