<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- footer -->  
  <footer id="aa-footer">
    <!-- footer bottom -->
    <div class="aa-footer-top">
     <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-top-area">
            <div class="row">
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <h3>Main Menu</h3>
                  <ul class="aa-footer-nav">
                    <li><a href="${path}/">홈으로</a></li>
                    <li><a href="#">꾸꾸의 서비스</a></li>
                    <li><a href="#">꾸꾸의 상품</a></li>
                    <li><a href="#">꾸꾸 회사소개</a></li>
                    <li><a href="#">꾸꾸에게 연락</a></li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Knowledge Base</h3>
                    <ul class="aa-footer-nav">
                      <li><a href="#">배송</a></li>
                      <li><a href="#">환불</a></li>
                      <li><a href="#">서비스</a></li>
                      <li><a href="#">할인 상품</a></li>
                      <li><a href="#">특별 제안</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Useful Links</h3>
                    <ul class="aa-footer-nav">
                      <li><a href="#">사이트맵</a></li>
                      <li><a href="#">검색</a></li>
                      <li><a href="#">강화검색</a></li>
                      <li><a href="#">공급 업체</a></li>
                      <li><a href="#">FAQ</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6">
                <div class="aa-footer-widget">
                  <div class="aa-footer-widget">
                    <h3>Contact Us</h3>
                    <address>
                      <p> 대한민국 서울시 강동구</p>
                      <p><span class="fa fa-phone"></span>+1 010-2032-3341</p>
                      <p><span class="fa fa-envelope"></span>KkuKku@Shop.com</p>
                    </address>
                    <div class="aa-footer-social">
                      <a href="#"><span class="fa fa-facebook"></span></a>
                      <a href="#"><span class="fa fa-twitter"></span></a>
                      <a href="#"><span class="fa fa-google-plus"></span></a>
                      <a href="#"><span class="fa fa-youtube"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     </div>
    </div>
    <!-- footer-bottom -->
    <div class="aa-footer-bottom">
      <div class="container">
        <div class="row">
        <div class="col-md-12">
          <div class="aa-footer-bottom-area">
            <p>Designed by <a href="http://www.markups.io/"> OnePig</a></p>
            <div class="aa-footer-payment">
              <span class="fa fa-cc-mastercard"></span>
              <span class="fa fa-cc-visa"></span>
              <span class="fa fa-paypal"></span>
              <span class="fa fa-cc-discover"></span>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- / footer -->

  <!-- Login Modal -->  
  <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">                      
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4>로그인</h4>
          <form class="aa-login-form" action="">
            <label for="">이름 또는 이메일<span>*</span></label>
            <input type="text" placeholder="Username or email">
            <label for="">비밀번호<span>*</span></label>
            <input type="password" placeholder="Password">
            <button class="aa-browse-btn" type="submit">Login</button>
            <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
            <p class="aa-lost-password"><a href="#">비밀번호찾기</a></p>
            <div class="aa-register-now">
        	      계정이 없으신가요? '^'♥<a href="account.html">계정 만들기!</a>
            </div>
          </form>
        </div>                        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div> 
  
     <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${path}/resources/js/bootstrap.js"></script>  
    <!-- SmartMenus jQuery plugin -->
    <script type="text/javascript" src="${path}/resources/js/jquery.smartmenus.js"></script>
    <!-- SmartMenus jQuery Bootstrap Addon -->
    <script type="text/javascript" src="${path}/resources/js/jquery.smartmenus.bootstrap.js"></script>  
    <!-- To Slider JS -->
    <script src="${path}/resources/js/sequence.js"></script>
    <script src="${path}/resources/js/sequence-theme.modern-slide-in.js"></script>  
    <!-- Product view slider -->
    <script type="text/javascript" src="${path}/resources/js/jquery.simpleGallery.js"></script>
    <script type="text/javascript" src="${path}/resources/js/jquery.simpleLens.js"></script>
    <!-- slick slider -->
    <script type="text/javascript" src="${path}/resources/js/slick.js"></script>
    <!-- Price picker slider -->
    <script type="text/javascript" src="${path}/resources/js/nouislider.js"></script>
    <!-- Custom js -->
    <script src="${path}/resources/js/custom.js"></script> 