package com.example.project04.model.notice.dto;

import java.util.Date;

public class NoticeDTO {
	private int nseq;    //공지사항 게시물 번호
	private String subject;//공지사항 제목
	private String content;//공지사항 내용
	private String adminid;//공지자
	private Date indate;//공지한날짜
	private int viewcnt;//조회수처리를위한 변수
	private String new1;
	
	public String getNew1() {
		return new1;
	}
	public void setNew1(String new1) {
		this.new1 = new1;
	}
	public int getNseq() {
		return nseq;
	}
	public void setNseq(int nseq) {
		this.nseq = nseq;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAdminid() {
		return adminid;
	}
	public void setAdminid(String adminid) {
		this.adminid = adminid;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	public int getViewcnt() {
		return viewcnt;
	}
	public void setViewcnt(int viewcnt) {
		this.viewcnt = viewcnt;
	}
	
	@Override
	public String toString() {
		return "NoticeDTO [nseq=" + nseq + ", subject=" + subject + ", content=" + content + ", adminid=" + adminid
				+ ", indate=" + indate + ", viewcnt=" + viewcnt + ", new1=" + new1 + "]";
	}
	
}

