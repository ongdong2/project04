package com.example.project04.controller.inquiry;



import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.inquiry.dto.AnswerDTO;
import com.example.project04.model.inquiry.dto.InQuiryDTO;
import com.example.project04.service.Inquiry.AnswerService;
import com.example.project04.service.Inquiry.InQuiryService;
import com.example.project04.service.Inquiry.Pager;
import com.mysql.cj.api.Session;

@RestController
@RequestMapping("answer/*")
public class AnswerController {
	
	@Inject
	AnswerService answerService;
	@Inject
	InQuiryService inquiryService;
	
	@RequestMapping("insert.do")
	public void insert(@ModelAttribute AnswerDTO dto, int qseq ,HttpSession session ) throws Exception {
		System.out.println("안나와:"+dto);
		String id=(String)session.getAttribute("id");
		dto.setAdminid(id);
		answerService.create(dto);
		inquiryService.answer(qseq);
	
	}
	@RequestMapping("answerlist.do")
	public ModelAndView list(ModelAndView mav,int qseq) {
		List<AnswerDTO> list=answerService.list(qseq);
		Map<String, Object> map=new HashMap<>();
		mav.setViewName("inquiry/answer_list");
		mav.addObject("list",list);
		System.out.println("엠에브이+"+mav);
		return mav;
		
	}
	@RequestMapping("answertext.do")
	public ModelAndView answerttext(ModelAndView mav , @RequestParam int qseq) {
		List<AnswerDTO> list2=answerService.answer(qseq);
		mav.setViewName("inquiry/answer");
		mav.addObject("answer",list2);
		return mav;
	}

}
