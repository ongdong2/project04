package com.example.project04.controller.inquiry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.inquiry.dto.InQuiryDTO;
import com.example.project04.service.Inquiry.AnswerService;
import com.example.project04.service.Inquiry.InQuiryService;
import com.example.project04.service.Inquiry.Pager;

@Controller

@RequestMapping("inquiry/*")
public class InQuiryController {
	@Inject
	InQuiryService inquiryService;
	@Inject
	AnswerService answerService;

	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue = "id") String search_option,
			@RequestParam(defaultValue ="") String keyword, @RequestParam(defaultValue = "1") int curPage)
			throws Exception {
		// 답변목록을 불러오기위한 변수
		
		int count = inquiryService.countPage(search_option, keyword);
		Pager pager = new Pager(count, curPage);
		int start = pager.getPageBegin();
		int end = pager.getPageEnd();
		
		// 답변을 불러오기위한텍스트
		/* List<AnswerDTO> answer=answerService.answer(qseq); */

		// 게시물 목록 뽑는 리스트. 검색옵션 페이지나누기 매개변수들
		List<InQuiryDTO> list = inquiryService.list(search_option, keyword, start, end);
		HashMap<String, Object> map = new HashMap<>();
		ModelAndView mav = new ModelAndView();
		map.put("list", list); // 맵에 자료저장
		map.put("count", count);
		map.put("pager", pager); // 페이지 나누기를 위한 변수
		map.put("search_option", search_option); // id,content,subject
		map.put("keyword", keyword); // 아이디와 콘텐츠와 서브젝트안에 값들 %이% 가운데에 이가 들어간 모든것
		mav.setViewName("inquiry/list");
		mav.addObject("map", map);
		return mav;
	}

	@RequestMapping("view.do")
	public ModelAndView view(int qseq, int curPage,String id) throws Exception {
		
		System.out.println("세상에는 정없는 사람이 너무 많다.");
		int count = 0;
		Pager pager = new Pager(count, curPage);
		
		ModelAndView mav = new ModelAndView();
		mav.setViewName("inquiry/view");
		Map<String, Object> map = new HashMap<>();
		map.put("pager", pager);
		map.put("id", id);
		map.put("dto", inquiryService.view(qseq));
		mav.addObject("map", map);
		System.out.println("44444444444444444444444444444444444444444444444"+mav);
		return mav;

	}

	@RequestMapping("update.do")
	public String update(InQuiryDTO dto,int curPage,Model model, HttpSession session) throws Exception {
		String id = (String)session.getAttribute("id");
		int count=0;
		Pager pager = new Pager(count, curPage);		
		model.addAttribute("curPage",curPage);
		Map<String,Object> map=new HashMap<>();
		map.put("curPage",curPage);
		inquiryService.update(dto);
		return "redirect:/inquiry/view.do?qseq="+dto.getQseq()+"&id="+id;
	}
	@RequestMapping("delete.do")
	public String delete(int qseq,@RequestParam(defaultValue="1")int curPage,Model model) throws Exception {
		System.out.println(qseq);
		int count=0;
		Pager pager = new Pager(count, curPage);
		model.addAttribute("curPage",curPage);
		inquiryService.delete(qseq);
		return "redirect:/inquiry/list.do?curPage="+curPage;
	}

	@RequestMapping("inquiry.do")
	public String inquiry() {
		return "inquiry/inquiry";
	}

	@RequestMapping("insert.do")
	public String insertInquiry(InQuiryDTO dto, HttpSession session) throws Exception {
		String id = (String) session.getAttribute("id");
		dto.setWriter(id);
		inquiryService.create(dto);

		return "redirect:/inquiry/list.do";

	}

	@RequestMapping("answer.do")
	public String answer(int qseq) throws Exception {
		inquiryService.answer(qseq);
		return "redirect:/board/list.do";
	}
	@RequestMapping("delete.go")
	public String delete(Model model,@RequestParam int ano,int qseq,@RequestParam(defaultValue="1") int curPage,HttpSession session) {
		int count=0;
		String id=(String)session.getAttribute("id");
		Pager page=new Pager(count, curPage);
		
		answerService.delete(ano);
		return "redirect:/inquiry/view.do?qseq="+qseq+"&curPage="+curPage+"&id="+id;
	}
	
}
