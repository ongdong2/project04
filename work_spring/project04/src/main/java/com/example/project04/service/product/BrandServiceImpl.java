package com.example.project04.service.product;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.product.dao.BrandDAO;
import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.model.product.dto.ProductDTO2;

@Service
public class BrandServiceImpl implements BrandService {

	@Inject
	BrandDAO brandDao; 
	
	@Override
	public List<ProductDTO> listBrand1() {
		return brandDao.listBrand1();
	}

	@Override
	public List<ProductDTO> listBrand2() {
		return brandDao.listBrand2();
	}

	@Override
	public List<ProductDTO> listBrand3() {
		return brandDao.listBrand3();
	}

	@Override
	public List<ProductDTO> listBrand4() {
		return brandDao.listBrand4();
	}

	@Override
	public List<ProductDTO> listBrand5() {
		return brandDao.listBrand5();
	}

	@Override
	public List<ProductDTO> listBrand6() {
		return brandDao.listBrand6();
	}

	@Override
	public List<ProductDTO> listBrand7() {
		return brandDao.listBrand7();
	}

	@Override
	public List<ProductDTO> listBrand8() {
		return brandDao.listBrand8();
	}

	@Override
	public List<ProductDTO> listBrand9() {
		return brandDao.listBrand9();
	}

	@Override
	public List<ProductDTO> listBrand10() {
		return brandDao.listBrand10();
	}

	@Override
	public List<ProductDTO> listBrand11() {
		return brandDao.listBrand11();
	}

	@Override
	public List<ProductDTO> listBrand12() {
		return brandDao.listBrand12();
	}
	
	@Override
	public List<ProductDTO> listBrand13() {
		return brandDao.listBrand13();
	}

	@Override
	public List<ProductDTO2> listCatg1(int dseq) {
		return brandDao.listCatg1(dseq);
	}
	
	@Override
	public List<ProductDTO2> listCatg2(int dseq) {
		return brandDao.listCatg2(dseq);
	}
	
	@Override
	public List<ProductDTO2> listCatg3(int dseq) {
		return brandDao.listCatg3(dseq);
	}

	@Override
	public List<ProductDTO2> listDseq1(String sort) {
		return brandDao.listDseq1(sort);
	}

	@Override
	public List<ProductDTO2> listDseq2(String sort) {
		return brandDao.listDseq2(sort);
	}

	@Override
	public List<ProductDTO2> listDseq3(String sort) {
		return brandDao.listDseq3(sort);
	}

	@Override
	public List<ProductDTO2> listDseq4(String sort) {
		return brandDao.listDseq4(sort);
	}

	@Override
	public List<ProductDTO2> listDseq5(String sort) {
		return brandDao.listDseq5(sort);
	}

	@Override
	public List<ProductDTO2> listDseq6(String sort) {
		return brandDao.listDseq6(sort);
	}

	@Override
	public List<ProductDTO2> listDseq7(String sort) {
		return brandDao.listDseq7(sort);
	}

	@Override
	public List<ProductDTO2> listDseq8(String sort) {
		return brandDao.listDseq8(sort);
	}

	@Override
	public List<ProductDTO2> listDseq9(String sort) {
		return brandDao.listDseq9(sort);
	}

	@Override
	public List<ProductDTO2> listDseq10(String sort) {
		return brandDao.listDseq10(sort);
	}

}
