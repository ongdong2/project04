package com.example.project04.model.admin_newproduct.dao;

import java.util.List;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

public interface Admin_NewProductDAO {
	public List<Admin_NewProductDTO> listNew(int start,int end);
	//public Admin_NewProductDTO viewNew(int nseq);
	public Admin_NewProductDTO detailProduct(int nseq);
	public void insertNew(Admin_NewProductDTO dto);
	public void insertNewSize(Admin_NewProductDTO dto);
	public Admin_NewProductDTO orderProduct(int nseq);
	public int countArticle() throws Exception;
	public void update(Admin_NewProductDTO dto);
	public List<Admin_NewProductDTO> ordermoney();
	/*public void deleteOrder(Admin_ProductDTO dto);*/
	public void insertStock(Admin_NewProductDTO dto);
}
