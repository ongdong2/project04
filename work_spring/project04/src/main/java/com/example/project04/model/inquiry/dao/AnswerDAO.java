package com.example.project04.model.inquiry.dao;

import java.util.List;

import com.example.project04.model.inquiry.dto.AnswerDTO;


public interface AnswerDAO {
	public List<AnswerDTO> list(int qseq);
	public int count(int qseq);
	public void create(AnswerDTO dto);
	public List<AnswerDTO> answer(int qseq);
	public void delete(int ano);
}
