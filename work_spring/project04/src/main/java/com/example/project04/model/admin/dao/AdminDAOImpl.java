package com.example.project04.model.admin.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.admin.dto.AdminMemberDTO;
import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_member.dto.Admin_MemberDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Repository
public class AdminDAOImpl implements AdminDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public String loginCheck(AdminMemberDTO dto) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin.login_check", dto);
	}

	@Override
	public List<Admin_MemberDTO> listMember() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.listmember");
	} 

	@Override
	public List<Admin_DealerDTO> listDealer() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.listdealer");
	}

	@Override
	public List<Admin_ProductDTO> listProduct() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.listproduct");
	}

	@Override
	public List<Admin_NewProductDTO> listNewproduct() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.listnewproduct");
	}

	@Override
	public List<Admin_ProductDTO> orderlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.orderlist");
	}

	@Override
	public List<Admin_ProductDTO> orderstocklist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.orderstocklist");
	}

	@Override
	public List<Admin_PayDTO> paylist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin.paylist");
	}

}
