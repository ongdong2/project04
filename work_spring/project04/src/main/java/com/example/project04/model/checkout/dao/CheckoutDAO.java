package com.example.project04.model.checkout.dao;

import java.util.List;


import com.example.project04.model.checkout.dto.CheckoutDTO;

public interface CheckoutDAO {
	public List<CheckoutDTO> checkoutMoney();
	public void insert(CheckoutDTO dto);
	public List<CheckoutDTO> listCheckout(String id);
	public void delete(int cseq);
	public void deleteAll(String id);
	public void update(int cseq);
	public int sumMoney(String id);			
	public int countCheckout(String id, int pseq);
	public void updateCheckout(CheckoutDTO dto);
	public void modifyCheckout(CheckoutDTO dto);
}
