package com.example.project04.service.order;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.order.dao.OrderDAO;
import com.example.project04.model.order.dto.OrderDTO;

@Service
public class OrderServiceImpl implements OrderService {

	@Inject
	OrderDAO orderDao;
	
	@Override
	public void cinsert(OrderDTO dto) {
		orderDao.cinsert(dto);
		
	}

	@Override
	public void oinsert(OrderDTO dto) {
		orderDao.oinsert(dto);
		
	}
	
	@Override
	public List<OrderDTO> clistOrder(String id) {
		return orderDao.clistOrder(id);
	}

	@Override
	public List<OrderDTO> olistOrder(String id) {
		return orderDao.olistOrder(id);
	}
	
	@Override
	public List<OrderDTO> listOrder(String id) {
		return orderDao.listOrder(id);
	}

	@Override
	public int sumMoney(String id) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public void update3(int odseq) {
		orderDao.update3(odseq);
	}

	@Override
	public void update4(int odseq) {
		orderDao.update4(odseq);
	}

	/*@Override
	public void cutstock(int pseq) {
		// TODO Auto-generated method stub
		orderDao.cutstock(pseq);
	}

	@Override
	public List<OrderDTO> listcut(OrderDTO dto) {
		// TODO Auto-generated method stub
		return orderDao.listcut(dto);
	}*/

}
