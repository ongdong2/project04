<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>



<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Register</title>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
/* 	$(function(){
		$("#loginmodal").click(function(){
			var inputid = $("#inputid").val();
		});
		document.form7.action=
		document.form2.submit();
		
	}); */
</script>



<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
	<%@ include file="../include/menu.jsp"%>
	<div class="container">
		<form method="post" action="register.do" id="form7" name="form7">
			<table class="table table-bordered table-hover"
				style="width: 760px; text-align: cetner; border: 1px solid #dddddd">

				<thead>
					<tr>
						<th style="width: 550px" colspan="3">
							<h4>환영합니다 ${name}님!<br>
							회원가입이 완료 되었습니다.
							</h4>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td style="width: 150px;"><h5>아이디</h5></td>
						<td><input class="form-control" type="text" readonly
							value="${id}" style="width: 350px" id="id" name="inputid"
							maxLength="20"></td>
					</tr>

					<tr>
						<td style="width: 150px;"><h5>이름</h5></td>
						<td><input class="form-control" type="text" readonly
							value="${name}" style="width: 350px" id="name" name="inputname"
							maxLength="20"></td>
					</tr>

					<tr>
						<td style="width: 150px;"><h5>이메일주소</h5></td>
						<td><input class="form-control" type="text" readonly
							value="${email}" style="width: 350px" id="email" name="inputemail"
							maxLength="20"></td>
					</tr>

					<tr>
						<td style="width: 150px;"><h5>전화번호</h5></td>
						<td><input class="form-control" type="text" readonly
							value="${phone1}" style="width: 350px" id="phone1" name="inputphone"
							maxLength="20"></td>
					</tr>

					<tr>
						<td style="width: 150px;"><h5>배송지 주소</h5></td>
						<td><input class="form-control" type="text" readonly
							value="${zip_num1}${address1}" style="width: 350px" id="zip_num" name="inputaddress"
							maxLength="20"></td>
					</tr>

					<tr>
						<td style="text-align: left" colspan="3"><input
							class="btn btn-primary pull-right"
							data-toggle="modal" data-target="#login-modal"
							type="button" id="loginmodal"
							value="로그인 하기">
					</tr>

				</tbody>
			</table>
		</form>
	</div>









	<%@ include file="../include/Footer.jsp"%>
</body>
</html>