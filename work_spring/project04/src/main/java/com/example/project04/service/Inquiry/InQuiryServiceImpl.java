package com.example.project04.service.Inquiry;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.inquiry.dao.InQuiryDAO;
import com.example.project04.model.inquiry.dto.InQuiryDTO;
@Service
public class InQuiryServiceImpl implements InQuiryService {
@Inject
InQuiryDAO inquiryDao;
	@Override
	public List<InQuiryDTO> list(String search_option,String keyword,int start,int end) throws Exception {
		return inquiryDao.list(search_option,keyword,start, end);
	}
	@Override
	public int countPage(String search_option,String keyword) throws Exception {
		return inquiryDao.countPage(search_option,keyword);
	}
	@Override
	public void create(InQuiryDTO dto) throws Exception {
		inquiryDao.create(dto);
		
	}
	@Override
	public void delete(int qseq) throws Exception {
			inquiryDao.delete(qseq);
	}
	@Override
	public void update(InQuiryDTO dto) throws Exception {
			inquiryDao.update(dto);
	}
	@Override
	public InQuiryDTO view(int qseq) throws Exception {
		return inquiryDao.view(qseq);
	}
	@Override
	public void answer(int qseq) throws Exception {
			inquiryDao.answer(qseq);
		
	}

}
