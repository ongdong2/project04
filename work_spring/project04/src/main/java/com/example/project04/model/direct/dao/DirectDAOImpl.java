package com.example.project04.model.direct.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.direct.dto.DirectDTO;

@Repository
public class DirectDAOImpl implements DirectDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<DirectDTO> directMoney() {
		return sqlSession.selectList("direct.direct_money");
	}

	@Override
	public void insert(DirectDTO dto) {
		sqlSession.insert("direct.insert", dto);
	}

	@Override
	public List<DirectDTO> listDirect(String id) {
		return sqlSession.selectList("direct.direct_list", id);
	}


	@Override
	public void show(String id) {
		sqlSession.delete("direct.show", id);
	}
	
	@Override
	public int sumMoney(String id) {
		return sqlSession.selectOne("direct.sumMoney", id);
	}

	@Override
	public void delete(String id) {
		sqlSession.delete("direct.delete", id);
	}

}
