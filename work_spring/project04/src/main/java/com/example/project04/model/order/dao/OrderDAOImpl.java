package com.example.project04.model.order.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.order.dto.OrderDTO;

@Repository
public class OrderDAOImpl implements OrderDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public void cinsert(OrderDTO dto) {
		System.out.println("##########################################################3"+dto);
		sqlSession.insert("order.cinsert", dto);
	}

	@Override
	public void oinsert(OrderDTO dto) {
		System.out.println("##########################################################3"+dto);
		sqlSession.insert("order.oinsert", dto);
	}
	
	@Override
	public List<OrderDTO> clistOrder(String id) {
		return sqlSession.selectList("order.order_clist", id);
	}

	@Override
	public List<OrderDTO> olistOrder(String id) {
		return sqlSession.selectList("order.order_olist", id);
	}
	
	@Override
	public List<OrderDTO> listOrder(String id) {
		return sqlSession.selectList("order.order_list", id);
	}

	@Override
	public int sumMoney(String id) {
		return sqlSession.selectOne("order.sumMoney", id);
	}
	@Override
	public void update3(int odseq) {
		sqlSession.update("order.confirm", odseq);
	}

	@Override
	public void update4(int odseq) {
		sqlSession.update("order.cancel", odseq);		
	}
	

	/*@Override
	public void cutstock(int pseq) {
		sqlSession.update("order.cutstock", pseq);
		
	}

	@Override
	public List<OrderDTO> listcut(OrderDTO dto) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("order.cutlist",dto);
	}*/

}
