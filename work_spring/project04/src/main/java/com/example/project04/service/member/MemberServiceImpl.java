package com.example.project04.service.member;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.example.project04.model.member.dao.MemberDAO;
import com.example.project04.model.member.dto.MemberDTO;

@Service
public class MemberServiceImpl implements MemberService {

	@Inject
	MemberDAO memberDao;

	@Override
	public boolean loginCheck(MemberDTO dto, HttpSession session) {
		boolean result = memberDao.loginCheck(dto);
		if (result) { // 로그인을성공한다면
			// 세션변수에 값을 저장한다
			MemberDTO dto2 = viewMember(dto.getId());

			session.setAttribute("id", dto.getId());
			session.setAttribute("mname", dto2.getMname());
			session.setAttribute("email", dto2.getEmail());
			session.setAttribute("passwd", dto2.getPasswd());
			session.setAttribute("phone1", dto2.getPhone1());
			session.setAttribute("zipcode1", dto2.getZipcode1());
			session.setAttribute("address1", dto2.getAddress1());
			session.setAttribute("zip_num1", dto2.getZip_num1());
			session.setAttribute("lv", dto2.getLv());
		}
		return result;
	}

	@Override
	public MemberDTO viewMember(String id) {
		return memberDao.viewMember(id);
	}

	@Override
	public void logout(HttpSession session) {
			session.invalidate();
	}

	@Override
	public String idcheck(String id) {
		return memberDao.idcheck(id);
	}

	@Override
	public void join(MemberDTO dto) {

		memberDao.join(dto);
	}

	@Override
	public void memberupdate1(MemberDTO dto, HttpSession session) {
		memberDao.memberupdate1(dto, session);
		MemberDTO dto2 = viewMember(dto.getId());
		session.setAttribute("mname", dto2.getMname());
		session.setAttribute("email", dto2.getEmail());
		session.setAttribute("passwd", dto2.getPasswd());
	}

	@Override
	public void memberupdate2(MemberDTO dto, HttpSession session) {
		String id = (String)session.getAttribute("id"); // 세션에 저장된 id를 가져옴
		//System.out.println("세션에 저장된 id:"+id); 
		dto.setId((String)session.getAttribute("id")); // dto 에 저장
		memberDao.memberupdate2(dto, session); // dto를 이용한 update
		
		MemberDTO dto2 = memberDao.viewMember(id);
		session.setAttribute("phone1", dto2.getPhone1());
		session.setAttribute("zipcode1", dto2.getZipcode1());
		session.setAttribute("address1", dto2.getAddress1());
		session.setAttribute("zip_num1", dto2.getZip_num1());

	}

	@Override
	public void memberupdate3(MemberDTO dto, HttpSession session) {
		String id = (String)session.getAttribute("id"); // 세션에 저장된 id를 가져옴
		//System.out.println("세션에 저장된 id:"+id); 
		dto.setId((String)session.getAttribute("id")); // dto 에 저장
		memberDao.memberupdate3(dto, session); // dto를 이용한 update
		
		MemberDTO dto2 = memberDao.viewMember(id);
		session.setAttribute("phone2", dto2.getPhone2());
		session.setAttribute("zipcode2", dto2.getZipcode2());
		session.setAttribute("address2", dto2.getAddress2());
		session.setAttribute("zip_num2", dto2.getZip_num2());
		
	}

	@Override
	public String memberfindid(MemberDTO dto) {
		return memberDao.findid(dto);
	}

	@Override
	public String memberfindpwd(MemberDTO dto) {
		return memberDao.findpwd(dto);
	}

	@Override
	public boolean deletecheck(String id, String passwd) {
		return memberDao.deletecheck(id,passwd);
		
	}

	@Override
	public void deleteEnd(String id) {
		memberDao.deleteEnd(id);
	}

	@Override
	public void temporaryPW(MemberDTO dto) {
		memberDao.temporaryPW(dto);
	}

	@Override
	public List<MemberDTO> memberinfo(String id) {
		return memberDao.memberinfo(id);
	}

	@Override
	public String findname(String id) {
		return memberDao.findname(id);
	}




}
