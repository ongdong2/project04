package com.example.project04.model.inquiry.dao;

import java.util.List;

import com.example.project04.model.inquiry.dto.InQuiryDTO;

public interface InQuiryDAO {
	public List<InQuiryDTO> list(String search_option,String keyword,int start ,int end) throws Exception;//문의목록
	public int countPage(String search_option,String keyword) throws Exception;//페이지 나누기 위한 메소드
	public void create(InQuiryDTO dto) throws Exception;//문의글 작성
	public void delete(int qseq) throws Exception;
	public void update(InQuiryDTO dto) throws Exception;
	public InQuiryDTO view(int qseq) throws Exception;
	public void answer(int qseq) throws Exception;
	
}
