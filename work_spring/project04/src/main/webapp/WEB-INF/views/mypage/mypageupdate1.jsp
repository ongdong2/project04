<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Mypage</title>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
$(function(){
		$("#passwdcheck1").click(function(){
			var passwd1 = $("#passwd1").val();
			var passwdcheck = $("#passwdcheck").val();
			var exp2 = /^[A-Za-z0-9]{8,15}$/;
			
			if (!exp2.test(passwd1)) {
				alert("비밀번호는 소문자,숫자 8자리 이상 입력하세요.");
				$("#passwd1").focus();
				return;
			}
			if (passwd1 != passwdcheck) {
				alert("비밀번호가 일치하지 않습니다.")
				$("#passwd1").focus();
				return;
			} 
		});
		
		$("#memberupdate1").click(function(){
			var mname = $("#mname").val();
			var email = $("#email").val();
			var passwd1 = $("#passwd1").val();
			var passwdcheck = $("#passwdcheck").val();
			var exp2 = /^[A-Za-z0-9]{8,15}$/;
			var exp3 =/^[a-z0-9]{2,}@[a-z0-9]{2,}\.[a-z]{2,}$/;
			
			if(!exp3.test(email)) {
				alert("이메일 형식이 잘못되었습니다.");
				$("#email").focus();
			}
			if (!exp2.test(passwd1)) {
				alert("비밀번호는 소문자,숫자 8자리 이상 입력하세요.");
				$("#passwd1").focus();
				return;
			}
			if (passwd1 != passwdcheck) {
				alert("비밀번호가 일치하지 않습니다.")
				$("#passwd1").focus();
				return;
			} 
			if (mname == "") {
				alert("이름은 필수 입력입니다.");
				$("#mname").focus();
				return;
			}
			document.form2.action="${path}/member/memberupdate11.do";
			document.form2.submit();
			alert("회원정보가 수정되었습니다.")
			
		});
		
		$("#memberupdate2").click(function(){
			
			document.form3.action="${path}/member/memberupdate22.do";
			document.form3.submit();
		});
		
});
</script>

<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@ include file="../include/menu.jsp"%>


<span class="container">
		<form method="post" action="register.do" id="form2" name="form2">
			<table class="table table-bordered table-hover"
				style="width: 480px; text-align: cetner; border: 1px solid #dddddd">
				<thead>
					<tr>
						<th style="width: 600px" colspan="3">
							<h4>${sessionScope.mname}(${sessionScope.id}) 님 회원정보</h4>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 150px;"><h5>아이디</h5></td>
						<td colspan="2"><input class="form-control" style="width: 350px" readonly
							value="${sessionScope.id}" id="id" name="id"  maxLength="20"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>비밀번호</h5></td>
						<td colspan="2"><input class="form-control" style="width: 350px" type="password"
							value="${sessionScope.passwd}" id="passwd1" name="passwd"  maxLength="20"></td>
					</tr>
						<tr>
						<td style="width: 150px;"><h5>비밀번호 확인</h5></td>
						<td colspan="2"><input class="form-control" style="width: 350px" type="password"
							value="${sessionScope.passwd}" id="passwdcheck" name="passwdcheck"  maxLength="20">
							<!-- <input class="btn btn-primary pull-right" type="button" id="sign" value="회원가입"> -->
							<input class="btn btn-primary pull-right" type="button" id="passwdcheck1" value="비밀번호확인"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>이름</h5></td>
						<td colspan="2"><input class="form-control" type="text"
							value="${sessionScope.mname}" style="width: 350px" id="mname"
							name="mname"  maxLength="20"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>이메일</h5></td>
						<td colspan="2">
							<input class="form-control" type="text" 
							value="${sessionScope.email}"
							style="width: 350px" id="email" name="email" 
							maxLength="20">
							<input class="btn btn-primary  pull-right" type="button" onclick="updatepasswd()" value="이메일 인증"></td>
					</tr>
					<tr>
						<td style="text-align: left" colspan="3"><input
							class="btn btn-primary pull-right" type="button" id="memberupdate1"
							value="회원 정보 수정">
					</tr>
				</tbody>
			</table>
		</form>
	</span>
		
		







<%@ include file="../include/Footer.jsp"%>
</body>
</html>