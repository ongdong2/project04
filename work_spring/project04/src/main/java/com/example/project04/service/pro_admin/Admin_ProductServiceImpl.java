package com.example.project04.service.pro_admin;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dao.Admin_ProductDAO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Service
public class Admin_ProductServiceImpl implements Admin_ProductService {
	
	@Inject
	Admin_ProductDAO productDao;

	@Override
	public List<Admin_ProductDTO> listProduct(int start,int end) {
		// TODO Auto-generated method stub
		return productDao.listProduct(start,end);
	}
	@Transactional
	@Override
	public void insertProduct(Admin_ProductDTO dto) {
		
		productDao.insert_Size(dto);
		productDao.insert_stock(dto);
		
	}

	@Override
	public Admin_ProductDTO viewProduct(int pseq) {
		// TODO Auto-generated method stub
		return productDao.viewProduct(pseq);
	}
	
	@Override
	public void deleteProduct(int pseq) {
		productDao.deleteProduct(pseq);
		
	}

	@Override
	public void updateProduct(Admin_ProductDTO dto) {
		productDao.updateProduct(dto);
		
	}

	@Override
	public Admin_ProductDTO detailProduct(int pseq) {
		// TODO Auto-generated method stub
		return productDao.detailProduct(pseq);
	}

	@Override
	public String fileInfo(int pseq) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return productDao.countArticle();
	}
	
	@Override
	public Admin_ProductDTO orderProduct(int pseq) {
		// TODO Auto-generated method stub
		return productDao.orderProduct(pseq);
	}
	@Override
	public List<Admin_ProductDTO> ordermoney() {
		// TODO Auto-generated method stub
		return productDao.ordermoney();
	}
	@Override
	public void deleteOrder(int pseq) {
		// TODO Auto-generated method stub
		productDao.deleteOrder(pseq);
	}
	@Override
	public List<Admin_ProductDTO> needlist() {
		// TODO Auto-generated method stub
		return productDao.needlist();
	}
	

	

}
