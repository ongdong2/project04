package com.example.project04.service.Inquiry;

import java.util.List;

import com.example.project04.model.inquiry.dto.AnswerDTO;

public interface AnswerService {
	public List<AnswerDTO> list(int qseq);
	public int count(int qseq);
	public void create(AnswerDTO dto);
	public List<AnswerDTO> answer(int qseq);
	public void delete(int ano);
	
}
