package com.example.project04.controller.dealer;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.dealer.dto.DealerDTO;
import com.example.project04.service.dealer.DealerService;

@Controller
@RequestMapping("dealer/*")
public class DealerController {

	@Inject
	DealerService dealerService;

	@RequestMapping("list.do")
	public ModelAndView list1(ModelAndView mav) {
		List<DealerDTO> list=dealerService.listDealer();
		mav.setViewName("brand/men_list");
		mav.addObject("dealer", list);
		return mav;
	}
	
}
