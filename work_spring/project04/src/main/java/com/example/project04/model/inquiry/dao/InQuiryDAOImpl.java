package com.example.project04.model.inquiry.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.inquiry.dto.InQuiryDTO;
@Repository
public class InQuiryDAOImpl implements InQuiryDAO {
@Inject
SqlSession sqlSession;
	@Override
	public List<InQuiryDTO> list(String search_option,String keyword,int start,int end) throws Exception {
		Map<String,Object> map=new HashMap<>();
		map.put("search_option",search_option);
		map.put("keyword", "%"+keyword+"%");
		map.put("start", start);
		map.put("end",end);
		return sqlSession.selectList("Inquiry.listall",map);
		
		
	}
	@Override
	public int countPage(String search_option,String keyword) throws Exception {
		Map<String, Object> map=new HashMap<>();
		map.put("search_option", search_option);
		map.put("keyword", "%"+keyword+"%");
		return sqlSession.selectOne("Inquiry.countPage",map);
	}
	@Override
	public void create(InQuiryDTO dto) throws Exception {
		sqlSession.insert("Inquiry.insert",dto);
		
	}
	@Override
	public void delete(int qseq) throws Exception {
		 sqlSession.delete("Inquiry.delete",qseq);
	}
	@Override
	public void update(InQuiryDTO dto) throws Exception {
		sqlSession.update("Inquiry.update",dto);
	}
	@Override
	public InQuiryDTO view(int qseq) throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Inquiry.view",qseq);
	}
	@Override
	public void answer(int qseq) throws Exception {
		sqlSession.selectOne("Inquiry.answer",qseq);
		
	}

}
