<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
</head>
<script type="text/javascript">
	 
var qseq="${map.dto.qseq}";
var curPage="${map.pager.curPage}";
/* var realcur = ${curPage} */
	$(function() {
		listAnswer();
		
		
		//답변등록
		$("#btnAnswer").click(function(){
			var answertext=$("#answertext").val();//답변내용
			
			var param={"answertext": answertext,"qseq": qseq};
			console.log(param);
			$.ajax({
				type:"post",
				url:"${path}/answer/insert.do",
				data: param,
				success: function(){
					alert("답변완료.");
					listAnswer();
					var answertext=$("#answertext").val("");
				}
			});
		});
		
		//답변목록 호출
		function listAnswer(){
			$.ajax({
				type: "get",
				url:"${path}/answer/answerlist.do?qseq="+qseq,
				success:function(result){
					$("#answerlist").html(result);
				}		
			});
		}
		//문의내역삭제
		
		$("#btnDelete").click(function() {
			
			alert("문의 내역을 삭제하겠습니까?");
			document.form212.action ="${path}/inquiry/delete.do?qseq="+qseq+"&curPage="+curPage;
			document.form212.submit();
		});
		//문의내역수정
		$("#btnUpdate").click(function(){
			alert("수정하시겠습니까?");
			document.form212.action ="${path}/inquiry/update.do?qseq="+qseq+"&curPage="+curPage;/* +"&id="+${id}; */
			//${path}/inquiry/view.do?qseq=${row.qseq}&curPage=${map.pager.curPage}" style="color:red;">${row.answershow}
			
			document.form212.submit();
		});
		
		$("#btnList").click(function(){
			document.form212.action="${path}/inquiry/list.do?curPage="+curPage;
			document.form212.submit();
		});
	});
	
</script>
<body>
<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-24">
         <div class="cart-view-area" align="center">
           <div class="cart-view-table aa-wishlist-table" >
           						<h1 align="center">게시물 보기</h1>
							<!-- 이거는 내일 물어봐야함 -->
		<form id="form212" name="form212" method="post" >
		<div>
		<Strong>문의날짜</Strong>
		<div><span style=color:green><fmt:formatDate value="${map.dto.indate}" pattern="yyyy-MM-dd a HH:mm:ss" /></span></div>
		<div><h4>작성자<Strong>&nbsp;&nbsp;:&nbsp;&nbsp;${map.dto.writer}</Strong></h4></div>
		
		<div align="left" style="margin-left:228px;"><Strong>문의제목</Strong></div><input name="subject" id="subject" size="80.5" value="${map.dto.subject}"
		placeholder="문의하실 내역의 제목을 입력해주세요" <c:if test="${sessionScope.id != map.id}">readonly</c:if>>
		</div><br>
		<div style="width: 900px;">
		<div align="left" style="margin-left:120px;"><Strong>문의 내역</Strong></div>
		<textarea id="content" name="content" rows="7" cols="80"
				 placeholder="문의하실 내용을 입력해주세요" <c:if test="${sessionScope.id != map.id}">readonly</c:if>>${map.dto.content}</textarea>
				</div>
		
		<div style="text-align: center;">
		
			<c:if test="${sessionScope.id == map.id}">
			<button class="btn btn-primary" type="button" id="btnDelete">삭제</button>
			</c:if>
			<c:if test="${sessionScope.id == map.id}">
			<button class="btn btn-primary" type="button" id="btnUpdate">수정</button><br><br>
			</c:if>
		<c:if test="${sessionScope.id == 'admin1' }">
		<textarea rows="5" cols="75" id="answertext"  placeholder="관리자 답변"></textarea><br>
		<button type="button" class="btn btn-primary"  id="btnAnswer"><strong>답변등록</strong></button><br>		
		</c:if>	
			<br>
			<button id="btnList" class="btn btn-primary" >문의목록</button>
					</div>
				<div id="answerlist"></div>
	
			</form>	
    </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
<%@ include file="../include/Footer.jsp"%>
</html>