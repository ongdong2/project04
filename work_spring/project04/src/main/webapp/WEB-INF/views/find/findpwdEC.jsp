<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Find ID/PWD</title>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
	$(function(){
		$("#emailconfirm").click(function(){
			var email=$("#email").val();
			if(email == ""){
				alert("이메일을 양식에 맞게 작성해 주세요.")
				return "member/findpwdEC.do";
		}else{
			var id=$("#id").val();
			alert("입력한 주소로 인증을 위한 이메일이 발송됩니다.")
		}
			document.form7.action = "${path}/email/send2.do";
			document.form7.submit();
		});
	});
	
	$(function() {
		$("#btncheck").click(function() {
			var num = $("#emailcheck").val();
			var emailnum="${number}";
			if(emailnum == ""){
				alert("이메일 인증을 받으세요.")
			}else if (num == "") {
				alert("인증번호를 입력하세요.")
			}else if(num != emailnum){
				alert("인증번호가 일치하지 않습니다.")
			}else if(num == emailnum){
				var id = $("#id").val();
				var passwd = $("#passwd").val();
					alert("이메일인증이 완료되었습니다..")
			document.form7.action = "${path}/member/FindPwdEmailCheck.do";
			document.form7.submit();
			}
		});
	});
</script>
<script>
	$(function(){
		$("#notfind").click(function(){
			document.form7.action="${path}/member/find.do";
			document.form7.submit();
		});
		$("#findid").click(function(){
			document.form7.action="${path}/member/index.do";
			document.form7.submit();
		});
	});
	
</script>
<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<%@ include file="../include/menu.jsp"%>
	<form method="post" action="FindPwdEmailCheck.do" id="form7" name="form7">
		<table class="table table-bordered table-hover" style="width: 500px" align="center">
			<thead>
				<tr>
					<th style="width: 500px" colspan="3">
						<h4>비밀번호를 찾기 위한 이메일인증</h4>
				</tr>
			</thead>
			<tbody>
						<c:if test="${message == 'notfind'}">
							<tr>
								<td>
							검색된 정보가 없습니다.
						<input class="btn btn-primary pull-right" type="button" id="notfind" value="돌아가기">
								</td>
							</tr>
							
						</c:if>
						
						<c:if test="${message == 'findpwd'}">
						<c:choose>
<c:when test="${receiveMail == null }">
	<tr>
		<td style="width: 150px" align = "center"><br><h5>이메일</h5></td>
		
		<td colspan="2">
			<input class="form-inline; form-control" type="text" placeholder="example@kkukkushop.com"
				style="width: 350px" id="email" name="email" maxLength="30">
			<input class="btn btn-primary" type="button" id="emailconfirm" value="이메일인증">
		</td>
	</tr>
		</c:when>
	<c:otherwise>
	<tr>
		<td style="width: 150px;"><h5>이메일</h5></td>
		<td colspan="2">
			<input class="form-inline; form-control" type="email" value="${receiveMail}"
				style="width: 350px" id="email" name="email" maxLength="30">
			<input class="btn btn-primary" type="button" id="emailconfirm" value="이메일인증">
		</td>
	</tr>
</c:otherwise>
</c:choose>
<tr>
		<td style="width: 150px" align = "center"><br><h5>이메일 인증번호</h5></td>
		<td colspan="2">
			<input class="form-inline; form-control" type="text" placeholder="인증번호 4자리 입력해주세요."
				style="width: 350px" id="emailcheck" name="emailcheck" maxLength="4">
			<input class="btn btn-primary" type="button" id="btncheck" value="인증하기">
			<input type="hidden" id="id" name="id" value="${result}">
			<input type="hidden" id="passwd" name="passwd" value="${passwd}">
		</td>
	</tr>
					</c:if>
			</tbody>
		</table>
	</form>

	<%@ include file="../include/Footer.jsp"%>
</body>
</html>