<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../include/header.jsp" %>
</head>
<body>
<%@ include file="../include/menu.jsp" %>

	<section id="aa-error">
		<div class="container">
		<div class="row">
		<div class="col-md-12">
		<div class="aa-error-area">
		  <span>정상적으로 처리되었습니다.</span>
		  <p>이용해주셔서  감사합니다.</p>
		  <a href="${path}/member/index.do">홈으로 가기</a>
		</div>
		</div>
		</div>
		</div>
	</section>
	
<section id="aa-subscribe">
	<div class="aa-subscribe-area">
	</div>
</section>
<%@ include file="../include/Footer.jsp" %>
</body>
</html>