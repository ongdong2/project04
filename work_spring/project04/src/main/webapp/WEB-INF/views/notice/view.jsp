<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<script type="text/javascript">
var curPage=${map.pager.curPage};
var nseq=${map.dto.nseq};
$(function(){
	$("#btnList").click(function(){
		location.href="${path}/notice/list.do";
	
		
	});
	$("#btnUpdate").click(function(){
		alert("수정하시겠습니까?");
		document.form3.action="${path}/notice/update.do?nseq="+nseq+"&curPage="+curPage;
		document.form3.submit();
	});
	$("#btnDelete").click(function(){
		alert("삭제합니다");
		document.form3.action="${path}/notice/delete.do?nseq="+nseq+"&curPage="+curPage;
		document.form3.submit();
	});
});
	
</script>
<title>Insert title here</title>
<section id="aa-catg-head-banner">
	<div class="aa-catg-head-banner-area">
		<div class="container"></div>
</section>
<section id="aa-blog-archive">
		<div class="col-md-12">
			<div class="aa-blog-archive-area">
				<div class="row">
					<div classs="col-md-9">
						<div class="aa-blog-content">
	<h2>공지사항</h2>
	<form id="form3" name="form3" method="post">
	<div>작성자:${map.dto.adminid}</div>
	<div>제목 : <input name="subject" id="subject" size="80" value="${map.dto.subject}">
	</div>
	<div>조회수 : ${map.dto.viewcnt} </div>
	<div style="width:800px;">
	내용 : <textarea id="content" name="content" rows="3" cols="80">
	${map.dto.content}
	</textarea>
	<input type="hidden" name="nseq" value="${map.dto.nseq}"><br>
	
	<button type="button" id="btnUpdate">수정</button>
	<button type="button" id="btnDelete">삭제</button><br>
	<button type="button" id="btnList">공지목록</button>
	</form>
	</div>
	</div>
	</div>
	<div class="aa-blog-archive-pagination" align="center">
				<div class="col-md-3">
			 <div class="aa-sidebar-widget">
	<div class="aa-sidebar-widget"></div>
				</div>
				</div>
				</div>
	</div>
	</div>
	</div>
	</section>
<%@ include file="../include/Footer.jsp"%>

</body>
</html>