<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Mypage</title>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
$(function(){
		$("#memberupdate1").click(function(){
			document.form2.action="${path}/member/memberupdate1.do";
			document.form2.submit();		
		});
		$("#memberupdate2").click(function(){
			document.form2.action="${path}/member/memberupdate2.do";
			document.form2.submit();		
		});

});
</script>

<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@ include file="../include/menu.jsp"%>
<section id="aa-subscribe">
<div class="aa-subscribe-area">
<span class="container">
<form method="post" action="register.do" id="form2" name="form2">
	<table class="table table-bordered table-hover"
		style="width: 1000px", align="center", border: 1px solid #dddddd">
<thead>
	<tr>
		<th style="width: 350px" colspan="3">
		<h4>${sessionScope.name}(${sessionScope.id}) 님 회원정보</h4>
		</th>
		<th style="width: 350px" colspan="3">
		<h4>연락처, 배송지관리</h4>
		</th>
	</tr>
</thead>
<tbody>
	<tr>
	<td style="width: 350px;">
	<h5>아이디</h5>
	</td>
	<td colspan="2">
	<input class="form-control" style="width: 350px" value="${sessionScope.id}"
		id="id" name="id" readonly maxLength="20"></td>
	<td style="width: 350px;">
	<h5>전화번호1</h5>
	</td>
	<td colspan="2">
	<input class="form-control" type="text" value="${sessionScope.phone1}"
		style="width: 350px" id="phone1" name="phone1" readonly maxLength="20">
	</td>
	</tr>

		<tr>
		<td style="width: 350px;">
		<h5>비밀번호</h5>
		</td>
		<td colspan="2">
		<input class="form-control" style="width: 350px" type="password"
			value="${sessionScope.passwd}" id="passwd1" name="passwd" 
			readonly maxLength="20">
		</td>
		<%-- <td><input class="form-control" style="width: 350px"
			value="${sessionScope.passwd}"
			id="passwd1" name="passwd" readonly maxLength="20"></td> --%>
		<td style="width: 350px;">
		<h5>배송지 주소 1</h5>
		</td>
		<td colspan="2">
		<input class="form-control" type="text" 
			value="(${sessionScope.zipcode1}) ${sessionScope.zip_num1} ${sessionScope.address1}"
			style="width: 350px" id="zip_num1" name="zip_num1" readonly maxLength="60">
		</td>
		</tr>
	
	<tr>
	<td style="width: 350px;">
	<h5>이름</h5>
	</td>
	<td colspan="2">
	<input class="form-control" type="text" value="${sessionScope.mname}"
		style="width: 350px" id="mname" name="mname" readonly maxLength="20">
	</td>
	<td style="width: 350px;">
	<h5>전화번호2</h5>
	</td>
	<td colspan="2">
	<input class="form-control" type="text" value="${sessionScope.phone2}"
		style="width: 350px" id="phone2" name="phone2" readonly maxLength="20">
	</td>
	</tr>
	
		<tr>
		<td style="width: 350px;">
		<h5>이메일</h5>
		</td>
		<td colspan="2">
		<input class="form-control" type="text" value="${sessionScope.email}"
			style="width: 350px" id="email" name="email" readonly maxLength="20">
		</td>
		<td style="width: 350px;">
		<h5>배송지 주소 2</h5>
		</td>
		<td colspan="2">
		<input class="form-control" type="text"
			value="(${sessionScope.zipcode2}) ${sessionScope.zip_num2} ${sessionScope.address2}"
			style="width: 350px" id="zip_num2" name="zip_num2" readonly maxLength="60"></td>
		</tr>
	
	<tr>
	<td style="text-align: left" colspan="3">
	<input class="btn btn-primary pull-right" type="button" id="memberupdate1" value="회원 정보 수정">
	</td>
	<td style="text-align: right" colspan="3">
	<input class="btn btn-primary pull-right" type="button" id="memberupdate2" value="회원 정보 수정">
	</td>
	</tr>
</tbody>
</table>
</form>
</span>
</div>
</section>
<pre>
<font color="gray">
<h5>계정을 지우고 싶으신가요? <a href="${path}/member/deletepage.do"><b><u>회원탈퇴 바로가기</u>▷</b></a></h5></font>
</pre>

<%@ include file="../include/Footer.jsp"%>
</body>
</html>