<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Account Page</title>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>


<script>
	$(function() {
		// 아이디 찾기
		$("#findid").click(function() {
			var mname = $("#mname").val();
			var email = $("#email").val();

			if (mname == "") {
				alert("이름을 입력 해 주세요.");
				$("#mname").focus();
				return;
			}
			if (email == "") {
				alert("가입하신 이메일 주소를 입력 해 주세요.");
				$("#email").focus();
				return;
			}
			document.form5.action = "${path}/member/findid.do";
			document.form5.submit();
		});
	});

	$(function() {
		// 비밀번호 찾기
		$("#findpwd").click(function() {
			var id = $("#id").val();
			var mname = $("#mname2").val();
			var email = $("#email2").val();

			if (id == "") {
				alert("아이디를 입력 해 주세요.");
				$("#id").focus();
				return;
			}
			if (mname == "") {
				alert("이름을 입력 해 주세요.");
				$("#mname2").focus();
				return;
			}
			if (email == "") {
				alert("가입하신 이메일 주소를 입력 해 주세요.");
				$("#email2").focus();
				return;
			}
			document.form5.action = "${path}/member/findpwd.do";
			document.form5.submit();
		});
	});
</script>

<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">

</head>
<body>

	<%@ include file="../include/menu.jsp"%>
	<br><br><br><br>
	<form method="post" id="form5" name="form5">
	<br><br><br><br><br><br>
	<div></div>	
		<table class="table table-bordered table-hover" style="width: 500px"
			align="center">
			<thead>
				<tr>
					<th style="width: 500px">
						<h4>아이디 찾기</h4>
					</th>
					<th style="width: 500px">
						<h4>비밀번호 찾기</h4>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><input class="form-control" type="text"
						placeholder="이름을 입력하세요." style="width: 100%" id="mname" name="mname">
						<input class="form-control" type="text"
						placeholder="이메일 주소를 입력하세요." style="width: 100%" id="email"
						name="email"></td>

					<td><input class="form-control" type="text"
						placeholder="아이디를 입력하세요." style="width: 100%" id="id" name="id">
						<input class="form-control" type="text" placeholder="이름을 입력하세요."
						style="width: 100%" id="mname2" name="mname"> <input
						class="form-control" type="text" placeholder="이메일 주소를 입력하세요."
						style="width: 100%" id="email2" name="email"></td>
				</tr>
				<tr>
					<td><input class="btn btn-primary pull-right" type="button"
						id="findid" value="아이디 찾기"></td>
					<td><input class="btn btn-primary pull-right" type="button"
						id="findpwd" value="비밀번호 찾기"></td>
				</tr>
			</tbody>
		</table>
		<br>
	</form>


	<%@ include file="../include/Footer.jsp"%>
</body>
</html>