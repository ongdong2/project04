package com.example.project04.model.admin_dealer.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Service
public class Admin_DealerDAOImpl implements Admin_DealerDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public List<Admin_DealerDTO> listDealer(int start,int end) {
		Map<String,Object> map=new HashMap<>();
		map.put("start", start);
		map.put("end", end);
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_dealer.dealer_list",map);
	}

	@Override
	public void insertDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public Admin_DealerDTO viewDealer(int dseq) {
		// TODO Auto-generated method stub
		
		return sqlSession.selectOne("admin_dealer.dealer_view",dseq);
	}

	@Override
	public void deleteDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.delete("admin_dealer.dealer_delete", dto);
	}

	@Override
	public void updateDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.update("admin_dealer.dealer_update", dto);
	}

	@Override
	public Admin_ProductDTO viewproDealer(int dseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_product.viewProduct",dseq);
	}

	@Override
	public List<Admin_ProductDTO> prolistDealer(int dseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_dealer.viewProduct",dseq);
	}

	@Override
	public Admin_DealerDTO detailDealer(int dseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_dealer.detail_dealer", dseq);
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_dealer.countArticle");
	}

	@Override
	public void addDealer(Admin_DealerDTO dto) {
		sqlSession.insert("admin_dealer.addDealer", dto);
		
	}

}
