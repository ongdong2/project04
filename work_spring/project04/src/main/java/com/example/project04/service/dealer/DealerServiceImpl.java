package com.example.project04.service.dealer;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.dealer.dao.DealerDAO;
import com.example.project04.model.dealer.dto.DealerDTO;

@Service
public class DealerServiceImpl implements DealerService {

	@Inject
	DealerDAO dealerDao;
	
	@Override
	public List<DealerDTO> listDealer() {
		return dealerDao.listDealer();
	}

}
