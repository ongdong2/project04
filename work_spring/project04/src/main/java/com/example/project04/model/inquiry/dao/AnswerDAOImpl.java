package com.example.project04.model.inquiry.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.inquiry.dto.AnswerDTO;

@Repository
public class AnswerDAOImpl implements AnswerDAO {
	
@Inject
SqlSession sqlSession;

	@Override
	public List<AnswerDTO> list(int qseq) {
		return sqlSession.selectList("answer.listanswer",qseq);
	}

	@Override
	public int count(int qseq) {
		return 0;
	}

	@Override
	public void create(AnswerDTO dto) {
		System.out.println("DAO임플 dto:"+dto);
		sqlSession.insert("answer.insertAnswer",dto);
	}

	@Override
	public List<AnswerDTO> answer(int qseq) {
		return sqlSession.selectList("answer.answertext",qseq);
		
	}

	@Override
	public void delete(int ano) {
		sqlSession.delete("answer.answerdelete",ano);
		
	}

}
