
package com.example.project04.controller.email;

import java.util.Random;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project04.model.email.EmailDTO;
import com.example.project04.model.member.dto.MemberDTO;
import com.example.project04.service.email.EmailService;

@Controller
@RequestMapping("email/*")
public class EmailController {

	@Inject
	EmailService emailService;

	@RequestMapping("email.do")
	public String write() {
		return "send.do";
	}

	@RequestMapping("send.do")
	public String send(@ModelAttribute EmailDTO dto, MemberDTO dto2, Model model) {

		Random rand = new Random();
		int number1 = rand.nextInt(10);

		String numberA = String.valueOf(number1);

		int number2 = rand.nextInt(10);
		String numberB = String.valueOf(number2);

		int number3 = rand.nextInt(10);
		String numberC = String.valueOf(number3);

		int number4 = rand.nextInt(10);
		String numberD = String.valueOf(number4);

		String number = numberA + numberB + numberC + numberD;
		System.out.println("이메일디티오"+dto);
		System.out.println("이메일멤버디티오"+dto2);

		String senderName = "Daily||Shop";
		String senderMail = "emailcheck@naver.com";
		String subject = "비밀번호를 인증해주세요.";
		String message = "인증번호는 " + number + " 입니다.";
		String receiveMail=dto2.getEmail();
		try {
			dto.setSenderName(senderName);
			dto.setSenderMail(senderMail);
			dto.setSubject(subject);
			dto.setMessage(message);
			
			
			dto.setReceiveMail(receiveMail);
			
			emailService.sendMail(dto);
			model.addAttribute("message", "이메일이 발송되었습니다.");
			model.addAttribute("number", number);
			model.addAttribute("receiveMail", dto.getReceiveMail());

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "이메일 발송 실패...");
		}
		return "register/registerEC";
	}
	
	@RequestMapping("send2.do")
	public String send2(@ModelAttribute EmailDTO dto, MemberDTO dto2, Model model) {
		Random rand = new Random();
		int number1 = rand.nextInt(10);

		String numberA = String.valueOf(number1);

		int number2 = rand.nextInt(10);
		String numberB = String.valueOf(number2);

		int number3 = rand.nextInt(10);
		String numberC = String.valueOf(number3);

		int number4 = rand.nextInt(10);
		String numberD = String.valueOf(number4);

		int number5 = rand.nextInt(10);
		String numberE = String.valueOf(number5);
		
		int number6 = rand.nextInt(10);
		String numberF = String.valueOf(number5);

		String number = numberA + numberB + numberC + numberD;
		
		String Ipwd = String.valueOf(numberD+numberA);
		String Ipwd2 = String.valueOf(numberC+numberE);
		String Ipwd3 = String.valueOf(numberB+numberF);
		
		char ch = (char) ((Math.random() * 26) + 65);
		
		String pwd2 = Ipwd+Ipwd2+Ipwd3+ch;
		String pwd = String.valueOf(pwd2);
		
		
		String passwd = pwd;

		String senderName = "Daily||Shop";
		String senderMail = "emailcheck@naver.com";
		String subject = "비밀번호를 인증해주세요.";
		String message = "인증번호는 " + number + " 입니다.";
		String receiveMail=dto2.getEmail();
		System.out.println("이메일디티오"+dto);
		System.out.println("이메일멤버디티오"+dto2);
		try {
			dto.setSenderName(senderName);
			dto.setSenderMail(senderMail);
			dto.setSubject(subject);
			dto.setMessage(message);
			
			String result =dto2.getId();
			dto.setReceiveMail(receiveMail);
			
			emailService.sendMail(dto);
			model.addAttribute("message", "findpwd");
			model.addAttribute("number", number);
			model.addAttribute("passwd", passwd);
			model.addAttribute("result", result);
			model.addAttribute("receiveMail", dto.getReceiveMail());

		} catch (Exception e) {
			e.printStackTrace();
			model.addAttribute("message", "이메일 발송 실패...");
		}
		return "find/findpwdEC";
	}

	@RequestMapping("test1234.do")
	public String send1() {
		return "email/sendemail";
	}

}
