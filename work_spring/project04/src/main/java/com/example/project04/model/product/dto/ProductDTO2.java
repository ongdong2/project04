package com.example.project04.model.product.dto;

import java.util.Date;

public class ProductDTO2 {
	private int pseq;
	private String pname;
	private String sort;
	private int price1;
	private int price2;
	private int dseq;
	private String dname;
	private String phone;
	private String address;
	private String content;
	private String image_url;
	private String useryn;
	private String bestyn;
	private Date indate;
	
	public ProductDTO2() {
	}

	public int getPseq() {
		return pseq;
	}

	public void setPseq(int pseq) {
		this.pseq = pseq;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public int getPrice1() {
		return price1;
	}

	public void setPrice1(int price1) {
		this.price1 = price1;
	}

	public int getPrice2() {
		return price2;
	}

	public void setPrice2(int price2) {
		this.price2 = price2;
	}

	public int getDseq() {
		return dseq;
	}

	public void setDseq(int dseq) {
		this.dseq = dseq;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUseryn() {
		return useryn;
	}

	public void setUseryn(String useryn) {
		this.useryn = useryn;
	}

	public String getBestyn() {
		return bestyn;
	}

	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	@Override
	public String toString() {
		return "ProductDTO2 [pseq=" + pseq + ", pname=" + pname + ", sort=" + sort + ", price1=" + price1 + ", price2="
				+ price2 + ", dseq=" + dseq + ", dname=" + dname + ", phone=" + phone + ", address=" + address
				+ ", content=" + content + ", image_url=" + image_url + ", useryn=" + useryn + ", bestyn=" + bestyn
				+ ", indate=" + indate + "]";
	}

}
