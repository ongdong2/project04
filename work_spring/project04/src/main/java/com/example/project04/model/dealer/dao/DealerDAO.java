package com.example.project04.model.dealer.dao;

import java.util.List;

import com.example.project04.model.dealer.dto.DealerDTO;

public interface DealerDAO {
	List<DealerDTO> listDealer();
}
