<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>
	<%@ include file="../include/header.jsp" %>  
	<%@ include file="../include/menu.jsp" %>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>	
	
<script>
function list(page){
	location.href="${path}/product/list.do?curPage="+page;
}
</script>
	<!-- Start slider -->
<section id="aa-slider">
	<div class="aa-slider-area">
		<div id="sequence" class="seq">
			<div class="seq-screen">
				<ul class="seq-canvas">
				
				
				<!-- single slide item -->
					<li>
						<div class="seq-model">
							<img data-seq src="${path}/resources/img/banner/slider3.jpg"
								alt="Women Jeans slide img" />
						</div>
						<div class="seq-title">					
							<a data-seq href="${path}/product/detail.do?pseq=13" class="aa-shop-now-btn aa-secondary-btn" style="margin-top: 350px;">
							구매하기</a>
						</div>
					
					</li>
				
				<!-- single slide item -->
					<li>
						<div class="seq-model">
							<img data-seq src="${path}/resources/img/banner/slider2.jpg"
								alt="Women Jeans slide img" />
						</div>
						<div class="seq-title">					
							<a data-seq href="${path}/brand/unisex.do" class="aa-shop-now-btn aa-secondary-btn" style="margin-top: 350px;">
							쇼핑하기</a>
						</div>
					
					</li>
					
					<!-- single slide item -->

					<li>
						<div class="seq-model">
							<img data-seq src="${path}/resources/img/slider/5.jpg"
								alt="Male Female slide img" />
						</div>
						<div class="seq-title">
							<h2 data-seq>커플상품!!</h2>
							<p data-seq>따뜻한 봄! 사랑하는 연인과 커플아이템</p>
							
							<%-- <p data-seq><%=application.getRealPath("/WEB-INF/views/images/") %></p> --%>
							
							<a data-seq href="#" class="aa-shop-now-btn aa-secondary-btn style=" style="margin-top: 225px;">
							쇼핑하기</a>
						</div>
					</li>
				</ul>
			</div>
			<!-- slider navigation btn -->
			<fieldset class="seq-nav" aria-controls="sequence"
				aria-label="Slider buttons">
				<a type="button" class="seq-prev" aria-label="Previous"><span
					class="fa fa-angle-left"></span></a> <a type="button" class="seq-next"
					aria-label="Next"><span class="fa fa-angle-right"></span></a>
			</fieldset>
		</div>
	</div>
</section>
<!-- / slider -->


  <!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            <div class="aa-product-catg-head">
              
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>

            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
              
                <!-- start single product item -->
                <c:forEach var="product" items="${map.list}">
                
                <li>
                  <figure>
                    <a class="aa-product-img" href="${path}/product/detail.do?pseq=${product.pseq}">
                    <img src="${path}/resources/img/shoes/${product.image_url}" width="200px" height="300px" alt="이언경"></a>
                    <figcaption>
                      <h4 class="aa-product-title"><a href="${product.pname}"></a></h4>
                      <span class="aa-product-price">￦<fmt:formatNumber value="${product.price2}"/></span>
                      <p class="aa-product-descrip">${product.content}</p>
                    </figcaption>
                  </figure>
				                        
                  
                  <!-- product badge -->
                </li>
                </c:forEach>

              </ul>
              
              
              
 



</div>
</div>
</div>
</div>
</div>
</section>




	<%@ include file="../include/Footer.jsp" %>  
	
  </body>
</html>