package com.example.project04.service.checkout;

import java.util.List;


import com.example.project04.model.checkout.dto.CheckoutDTO;
import com.example.project04.model.checkout.dto.CheckoutDTO;

public interface CheckoutService {
	public List<CheckoutDTO> checkoutMoney();
	
	public void insert(CheckoutDTO dto);
	public List<CheckoutDTO> listCheckout(String id);
	public void delete(int cseq);
	public void pdelete(int pseq);
	public void iddelete(String id);
	public void deleteAll(String id);
	public void update(int cseq);
	
	//얘가안됨
	public int sumMoney(String id);
	
	
	public int countCheckout(String id, int pseq);
	public void updateCheckout(CheckoutDTO dto);
	public void modifyCheckout(CheckoutDTO dto);
}
