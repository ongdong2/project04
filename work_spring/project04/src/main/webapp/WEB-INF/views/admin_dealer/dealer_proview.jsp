<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
   
    $("#btnDelete").click(function(){
       
        if(confirm("선택 상품을 삭제하시겠습니까?")){
            document.form1.action = "${path}/project04/admin_product/product_delete.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_product/productList.do";
    });
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">신발목록</div>
				<div class="pull-right">
					<span class="badge badge-info"></span>

				</div>
			</div>
			<div class="block-content collapse in">
				<%-- <c:if test="${sessionScope.adminId != null}"> --%>
					<button type="button" id="btnAdd">상품등록</button>
					<br>
				<%-- </c:if> --%>
				<table class="table table-striped">
					<thead>
						<tr>
						<th>&nbsp;</th>
							<th>상품코드</th>
							<th>상품명</th>
							<th>원가</th>
							<th>판매가</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="row" items="${list}">
							<tr>
								<td align="center"><img src="${path}/resources/images/${row.image_url}"
								width="100px" height="100px"></td>
								<td align="center">${row.pseq }</td>
								<td align="center"><a
									href="${path}/admin_product/product_detail/${row.pseq}">
										${row.pname }</a><br>
										<%--  <c:if test="${sessionScope.adminId != null}"> --%>
                			<a href="${path}/admin_product/product_edit/${row.pseq}">[상품편집]</a>
           								<%--  </c:if> --%>
										</td>
								<td align="center"><fmt:formatNumber value="${row.price1 }"
										pattern="###,###,###" />원</td>
								<td align="center"><fmt:formatNumber value="${row.price2 }"
										pattern="###,###,###" />원</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>