<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<script>
// 태그.prop("태그의 속성")
$(document).ready(function() {
	//모두 선택,해제 체크박스
	$("#chkAll").click(function() {
		//체크 상태이면
		if ($("#chkAll").prop("checked")) {
			//모두 선택
			$("input[name=cseq]").prop("checked", true);
		} else { //해제 상태이면
			//모두 해제
			$("input[name=cseq]").prop("checked", false);
		}
	});
	$("#btnAllDel").click(function() {
		if(confirm("장바구니를 비우시겠습니까?")){
			location.href="${path}/cart/deleteAll.do";
		}
	});
	$("#btnSelDel").click(function() {
	      var cseq=$("#cseq").val();
	      if (cseq==null ) {
	         alert("삭제할 상품을 선택하세요.");
	         return ;
	      }
	      document.form1.action = "${path}/cart/deleteSel.do";
	      document.form1.submit();
	   });
});
/* function delete(cseq) {
	location.href = "${path}/cart/delete.do?cseq="+cseq;
} */
</script>

<!-- catg header banner section -->


	<img src="${path}/resources/img/banner/cart_banner.jpg" alt="cart_banner.jpg" height="300" style="margin-left: auto; margin-right: auto; display: block;">
	<div class="aa-catg-head-banner-area">
		<div class="container">
			<div class="aa-catg-head-banner-content">
			</div>
		</div>
	</div>
<!-- / catg header banner section -->

<!-- Cart view section -->
<section id="cart-view">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cart-view-area">
					<div class="cart-view-table">
						<form id="form1" name="form1">
							<div class="table-responsive">
								<table class="table">
<!-- 		<form name="form1" method="post">
			<table border="1" width="400px">
 -->
 			<thead>
 				<tr>
 					<p><th><input type="checkbox" id="chkAll" name="chkAll"></th>
 					<th>&nbsp;</th>
					<th>상 품 명</th>
					<th>사 이 즈</th>
					<th>가 격</th>
					<th>수 량</th>
					<th>합 계</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="row" items="${map.list}">
				<tr>
					<td><input type="checkbox" id="cseq" name="cseq" value="${row.cseq}"></td>
					<td><a href="${path}/product/detail.do?pseq=${row.pseq}">
					<img src="${path}/resources/img/shoes/${row.image_url}" alt="img"></a></td>
					<td><a href="${path}/product/detail.do?pseq=${row.pseq}">${row.pname}</td>
					<td>
					<c:choose>
						<c:when test="${row.sz230mm!=0}">
							<v>230(${row.sz230mm})</v><br>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${row.sz240mm!=0}">
							<v>240(${row.sz240mm})</v><br>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${row.sz250mm!=0}">
							<v>250(${row.sz250mm})</v><br>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${row.sz260mm!=0}">
							<v>260(${row.sz260mm})</v><br>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${row.sz270mm!=0}">
							<v>270(${row.sz270mm})</v><br>
						</c:when>
					</c:choose>
					<c:choose>
						<c:when test="${row.sz280mm!=0}">
							<v>280(${row.sz280mm})</v><br>
						</c:when>
					</c:choose>
					</td>
					<td>￦<fmt:formatNumber value="${row.price2}"/></td>
					<td>${row.amount}</td>
					<td>￦<fmt:formatNumber value="${row.total}"/></td>
					<td>
						<a href="${path}/cart/delete.do?cseq=${row.cseq}">삭제</a>
					</td>
				</tr></p>
			</c:forEach>
			</tbody>
	
			</table>
			<div align="right">
			<button id="btnSelDel">선택삭제</button>
			<button type="button" id="btnAllDel">장바구니 비우기</button>	<br><br><br><br>
			</div>
							</div>
						</form>
						<!-- Cart Total view -->
						<div class="cart-view-total">
							<table class="aa-totals-table">
								<tbody>
									<tr>
										<th>장바구니 합계</th>
										<td>￦<fmt:formatNumber value="${map.sumMoney}"/></td>
									</tr>
								</tbody>
							</table>
							<!-- Proced to Checkout -->
							<a href="${path}/checkout.do" class="aa-cart-view-btn">구 매</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><br><br>
</section>
<!-- / Cart view section -->



<%@ include file="../include/Footer.jsp"%>
</body>
</html>