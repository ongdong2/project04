<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- views/board/reply_list.jsp -->
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../include/header.jsp" %>
</head>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>

</script>
<body>
<% pageContext.setAttribute("newLineChar", "\n"); %>

<form id="form1234" name="form1234" method="post">
<table style="width:700px">
<c:forEach var="row" items="${list}">  

	<tr>
		<td>
		<hr style="border:solid 1px"><br>
		<strong>답변일자&nbsp;&nbsp;:&nbsp;&nbsp;</strong><fmt:formatDate value="${row.indate}" pattern="yyyy-MM-dd a HH:mm:ss" /><br>
			<strong>관리자ID&nbsp;&nbsp;:&nbsp;&nbsp;${row.adminid}</strong><br>
			<span style="color:green"><strong>답변</strong>&nbsp;&nbsp;:&nbsp;&nbsp;${row.answertext}</span><br>
			<input class="btn btn-primary" type="button" value="삭제" name="delete" align="right" style="margin-left: 700px;" onclick="location.href='${path}/inquiry/delete.go?ano=${row.ano}&qseq=${row.qseq}'">
			<%-- <a href="${path}/inquiry/delete.go?ano=${row.ano}&qseq=${row.qseq}">삭제</a> --%>
		</td>
	</tr>
</c:forEach>	
</table>

</form>
</body>
</html>