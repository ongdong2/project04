package com.example.project04.service.product;

import java.util.List;

import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.model.product.dto.ProductDTO2;

public interface BrandService {
	List<ProductDTO> listBrand1();
	List<ProductDTO> listBrand2();
	List<ProductDTO> listBrand3();
	List<ProductDTO> listBrand4();
	List<ProductDTO> listBrand5();
	List<ProductDTO> listBrand6();
	List<ProductDTO> listBrand7();
	List<ProductDTO> listBrand8();
	List<ProductDTO> listBrand9();
	List<ProductDTO> listBrand10();
	List<ProductDTO> listBrand11();
	List<ProductDTO> listBrand12();
	List<ProductDTO> listBrand13();
	
	List<ProductDTO2> listCatg1(int dseq);
	List<ProductDTO2> listCatg2(int dseq);
	List<ProductDTO2> listCatg3(int dseq);
	
	List<ProductDTO2> listDseq1(String sort);
	List<ProductDTO2> listDseq2(String sort);
	List<ProductDTO2> listDseq3(String sort);
	List<ProductDTO2> listDseq4(String sort);
	List<ProductDTO2> listDseq5(String sort);
	List<ProductDTO2> listDseq6(String sort);
	List<ProductDTO2> listDseq7(String sort);
	List<ProductDTO2> listDseq8(String sort);
	List<ProductDTO2> listDseq9(String sort);
	List<ProductDTO2> listDseq10(String sort);
}
