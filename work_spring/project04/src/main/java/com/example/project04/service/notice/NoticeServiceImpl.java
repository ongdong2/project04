package com.example.project04.service.notice;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.example.project04.model.notice.dao.NoticeDAO;
import com.example.project04.model.notice.dto.NoticeDTO;

@Service
public class NoticeServiceImpl implements NoticeService{
@Inject
NoticeDAO noticeDao;

	@Override
	public List<NoticeDTO> list(int start,int end) throws Exception {
		return noticeDao.list(start,end);
	}
	@Override
	public int countPage() {
		return noticeDao.countPage();
	}
	@Override
	public void create(NoticeDTO dto) {
		noticeDao.create(dto);
	}
	@Override
	public void delete(int nseq) {
		noticeDao.delete(nseq);
	}

	@Override
	public void update(NoticeDTO dto) throws Exception{
		noticeDao.update(dto);
	}

	@Override
	public NoticeDTO view(int nseq) {
		return noticeDao.view(nseq);
	}
	@Override
	public void viewCnt(int nseq,HttpSession session) throws Exception {
		long update_time=0;
		if(session.getAttribute("update_time_"+nseq)!=null) {
			update_time=(long)session.getAttribute("update_time_"+nseq);
		}
		long current_time=System.currentTimeMillis();
		if(current_time - update_time > 10*1000) {
			
			noticeDao.viewCnt(nseq);
			
			session.setAttribute("update_time_"+nseq, current_time);
		}
		
	}
	

}
