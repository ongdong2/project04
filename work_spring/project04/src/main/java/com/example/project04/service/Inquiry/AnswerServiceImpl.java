package com.example.project04.service.Inquiry;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.inquiry.dao.AnswerDAO;
import com.example.project04.model.inquiry.dto.AnswerDTO;


@Service
public class AnswerServiceImpl implements AnswerService {
	@Inject
	AnswerDAO answerDao;

	@Override
	public List<AnswerDTO> list(int qseq) {
		return answerDao.list(qseq);
	}

	@Override
	public int count(int qseq) {
		return 0;
	}

	@Override
	public void create(AnswerDTO dto) {
		answerDao.create(dto);
		
	}

	@Override
	public List<AnswerDTO> answer(int qseq) {
		return answerDao.answer(qseq);
	}

	@Override
	public void delete(int ano) {
		answerDao.delete(ano);
	}

}
