package com.example.project04.model.checkout.dao;

import java.util.List;


import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.checkout.dto.CheckoutDTO;
import com.example.project04.model.checkout.dto.CheckoutDTO;

@Repository
public class CheckoutDAOImpl implements CheckoutDAO {

	@Inject
	SqlSession sqlSession;

	@Override
	public List<CheckoutDTO> checkoutMoney() {
		return sqlSession.selectList("checkout.checkout_money");
	}

	@Override
	public void insert(CheckoutDTO dto) {
		sqlSession.insert("checkout.insert", dto);
	}

	@Override
	public List<CheckoutDTO> listCheckout(String id) {
		return sqlSession.selectList("checkout.checkout_list", id);
	}

	@Override
	public void delete(int cseq) {
		// TODO Auto-generated method stub

	}


	@Override
	public void deleteAll(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(int cseq) {
		// TODO Auto-generated method stub

	}

	@Override
	public int sumMoney(String id) {
		return sqlSession.selectOne("checkout.sumMoney", id);
	}

	@Override
	public int countCheckout(String id, int pseq) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCheckout(CheckoutDTO dto) {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyCheckout(CheckoutDTO dto) {
		// TODO Auto-generated method stub

	}

}
