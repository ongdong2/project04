<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
   
    $("#btnDelete").click(function(){
       
        if(confirm("현재 정보로 배송하시겠습니까?")){
        	var pseq= $("#pseq").val();
    		var pname = $("#pname").val();
    		var mname = $("#mname").val();
    		var total = $("#total").val();
    		var id = $("#id").val();
    		var sz230mm = $("#sz230mm").val();
    		var sz240mm = $("#sz240mm").val();
    		var sz250mm = $("#sz250mm").val();
    		var sz260mm = $("#sz260mm").val();
    		var sz270mm = $("#sz270mm").val();
    		var sz280mm = $("#sz280mm").val();
    		
    	
    		var price1 = $("#price1").val();
    		var price2 = $("#price2").val();
        	
            document.form1.action = "${path}/project04/admin_pay/done.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_pay/pay_list.do";
    });
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">배송지정보</div>
				<div class="pull-right">
					<span class="badge badge-info">${dto.mname }</span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
							<tr>
							<th>받으실분</th>
							<td>${dto.mname }</td>
							</tr>
							<tr>
							<th>휴대전화</th>
							<td>${dto.phone1 }</td>
							</tr>
							<tr>
							<th>자택우편번호</th>
							<td>${dto.zipcode1 }</td>
							</tr>
							<tr>
							<th>자택주소</th>
							<td>${dto.zip_num1 }</td>
							</tr>
							<tr>
							<th>자택주소상세</th>
							<td>${dto.address1 }</td>
							</tr>
							<tr>
							<%-- <th>직장전화</th>
							<td>${dto.phone2 }</td>
							</tr>
							<tr>
							<th>직장우편번호</th>
							<td>${dto.zipcode2 }</td>
							</tr>
							<tr>
							<th>직장주소</th>
							<td>${dto.zip_num2 }</td>
							</tr>
							<tr>
							<th>직장주소상세</th>
							<td>${dto.address2 }</td>
							</tr> --%>
							<tr>
							<th>주문일자</th>
							<td><fmt:formatDate value="${dto.indate }" pattern="yyyy-MM-dd
							HH:mm:ss" /></td>
							</tr>
							<tr>
							<th>결제금액</th>
							<td>${dto.total }</td>
							</tr>
							<tr>
							<td><img src="${path}/resources/images/${dto.image_url}"
							width="200px" height="200px"></td>
							
							<td><table>
							<tr>
							<th align="justify">사이즈</th>
							<td>230mm</td>
							<td>240mm</td>
							<td>250mm</td>
							<td>260mm</td>
							<td>270mm</td>
							<td>280mm</td>
							</tr>
							<tr>
							<th align="justify">주문수량</th>
							<td>${dto.sz230mm }</td>
							<td>${dto.sz240mm }</td>
							<td>${dto.sz250mm }</td>
							<td>${dto.sz260mm }</td>
							<td>${dto.sz270mm }</td>
							<td>${dto.sz280mm }</td>
							
							</tr>
							</table> </td>
							
							</tr>
							
							<tr>
							<td colspan="2" align="center">
							<input type="hidden" name="odseq" id="odseq"
							value="${dto.odseq}">
							<input type="hidden" name="pseq" id="pseq"
							value="${dto.pseq}">
							
							<input type="hidden" name="pname" id="pname"
							value="${dto.pname}">
							<input type="hidden" name="mname" id="mname"
							value="${dto.mname}">
							<input type="hidden" name="total" id="total"
							value="${dto.total}">
							<input type="hidden" name="price2" id="price2"
							value="${dto.price2}">
							<input type="hidden" name="price1" id="price1"
							value="${dto.price1}">
							
							<input type="hidden" value="${dto.id }" name="id">
							<input type="hidden" value="${dto.sz230mm }" name="sz230mm">
							<input type="hidden" value="${dto.sz240mm }" name="sz240mm">
							<input type="hidden" value="${dto.sz250mm }" name="sz250mm">
							<input type="hidden" value="${dto.sz260mm }" name="sz260mm">
							<input type="hidden" value="${dto.sz270mm }" name="sz270mm">
							<input type="hidden" value="${dto.sz280mm }" name="sz280mm">
								<input type="button" value="배송보내기" id="btnDelete">
								<input type="button" value="확인" id="btnList">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>