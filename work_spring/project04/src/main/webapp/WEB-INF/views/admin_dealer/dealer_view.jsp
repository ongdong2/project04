<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<style>
#map {
	height: 400px;
	width: 700px;
}
</style>
</head>
<body>

	<script>
		var map;
		function initMap() {
				var decente = {
					lat : 37.497587,
					lng : 127.046732
				};
				var nike = {
					lat : 37.503472,
					lng : 127.037504
				};
				var adidas = {
					lat : 37.4976898,
					lng : 127.025783
				};
				var puma = {
					lat : 37.496807,
					lng : 127.925503
				};
				var vans = {
					lat : 37.554312,
					lng : 126.922956
				};
				var newbalance = {
					lat : 37.4562905,
					lng : 126.984923
				};
				var excelsior = {
					lat : 37.552120,
					lng : 127.936881
				};
				var converse = {
					lat : 37.564115,
					lng : 127.984436
				};
				var fila = {
					lat : 37.536543,
					lng : 127.989402
				};
				var kolka = {
					lat : 37.487311,
					lng : 127.991039
				};
				var posi="";
				switch (${dto.dseq}){
				case 1:
					posi=nike;
					break;
				case 2:
					posi=fila;
					break;
				case 3:
					posi=kolka;
					break;
				case 4:
					posi=adidas;
					break;
				case 5:
					posi=puma;
					break;
				case 6:
					posi=decente;
					break;
				case 7:
					posi=vans;
					break;
				case 8:
					posi=converse;
					break;
				case 9:
					posi=excelsior;
					break;
				case 10:
					posi=newbalance;
					break;
				}
			var map = new google.maps.Map(document.getElementById('map'), {
				center : posi,
				zoom : 18
			});
			
			var marker = new google.maps.Marker({
				position : 
					posi,
				map : map
			});
			/* var marker = new google.maps.Marker({
				position : adidas,
				map : map
			});
			var marker = new google.maps.Marker({
				position : vans,
				map : map
			});
			var marker = new google.maps.Marker({
				position : puma,
				map : map
			});
			var marker = new google.maps.Marker({
				position : newbalance,
				map : map
			});
			var marker = new google.maps.Marker({
				position : excelsior,
				map : map
			});
			var marker = new google.maps.Marker({
				position : converse,
				map : map
			});
			var marker = new google.maps.Marker({
				position : fila,
				map : map
			});
			var marker = new google.maps.Marker({
				position : kolka,
				map : map
			});
			var marker = new google.maps.Marker({
				position : fila,
				map : map
			}); */
		}
	</script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxJHXO5wsTm5AiCLqR2QUHUu-fbeOWjLk&callback=initMap"
		async defer></script>
	<script type="text/javascript">
$(function(){
   
    $("#btnDelete").click(function(){
       
        if(confirm("선택 거래처를 삭제하시겠습니까?")){
            document.form1.action = "${path}/project04/admin_dealer/dealer_delete.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_dealer/list.do";
    });
    
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">거래처상세정보</div>
				<div class="pull-right">
					<span class="badge badge-info">${dto.dname }</span>

				</div>
			</div>
			<div class="block-content collapse in">

				<form name="form1" method="post">
					<table class="table table-striped">
						<thead>

							<td align="center">
							<tr>
								<th>거래처코드</th>
								<td>${dto.dseq }</td>
							</tr>
							<tr>
								<th>거래처명</th>
								<td>${dto.dname}</td>
							</tr>
							<tr>
								<th>전화번호</th>
								<td>${dto.phone}</td>
							</tr>
							<tr>
								<th>우편번호</th>
								<td>${dto.zip_num}</td>
							</tr>
							<tr>
								<th>거래처주소</th>
								<td>${dto.address1}</td>
							</tr>
							<tr>
								<th>거래처 상세주소</th>
								<td>${dto.address2}</td>
							</tr>
					</table>
					</td>

					</tr>
					<tr>
						<td colspan="2" align="center"><input type="hidden"
								value="${dto.dseq }" name="dseq"> <input type="button"
								value="삭제" id="btnDelete"> <input type="button"
								value="목록" id="btnList"> <a
							href="${path}/admin_dealer/prolist.do/${dto.dseq}"> <input
									type="button" value="거래처상품보기"></a></td>

					</tr>
					</thead>

					</table>
					<div id="map"></div>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	<%-- <div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">거래처상품목록</div>
				<div class="pull-right">
					<span class="badge badge-info">165</span>

				</div>
			</div>
			<div class="block-content collapse in">
				<c:if test="${sessionScope.adminId != null}">
					
					<br>
				</c:if>
				<table class="table table-striped">
					<thead>
						<tr>
						<th>&nbsp;</th>
							<th>상품코드</th>
							<th>상품명</th>
							<th>원가</th>
							<th>판매가</th>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="row" items="${dto}">
							<tr>
								<td align="center"><img src="${path}/resources/images/${row.image_url}"
								width="100px" height="100px"></td>
								<td align="center">${row.pseq }</td>
								<td align="center"><a
									href="${path}/admin_product/product_detail/${row.pseq}">
										${row.pname }</a><br>
										 <c:if test="${sessionScope.adminId != null}">
                			<a href="${path}/admin_product/product_edit/${row.pseq}">[상품편집]</a>
           								 </c:if>
										</td>
								<td align="center"><fmt:formatNumber value="${row.price1 }"
										pattern="###,###,###" />원</td>
								<td align="center"><fmt:formatNumber value="${row.price2 }"
										pattern="###,###,###" />원</td>
							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
		<!-- /block -->
	</div> --%>
	<hr>
	<footer> </footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>