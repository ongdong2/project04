package com.example.project04.service.checkout;

import java.util.List;


import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.checkout.dao.CheckoutDAO;
import com.example.project04.model.checkout.dto.CheckoutDTO;
import com.example.project04.model.checkout.dao.CheckoutDAO;
import com.example.project04.model.checkout.dto.CheckoutDTO;

@Service
public class CheckoutServiceImpl implements CheckoutService {

	@Inject
	CheckoutDAO checkoutDao;
	
	@Override
	public List<CheckoutDTO> checkoutMoney() {
		return checkoutDao.checkoutMoney();
	}

	@Override
	public void insert(CheckoutDTO dto) {
		checkoutDao.insert(dto);
	}

	@Override
	public List<CheckoutDTO> listCheckout(String id) {
		return checkoutDao.listCheckout(id);
	}

	@Override
	public void delete(int cseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pdelete(int pseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void iddelete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(int cseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int sumMoney(String id) {
		return checkoutDao.sumMoney(id);
	}

	@Override
	public int countCheckout(String id, int pseq) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCheckout(CheckoutDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyCheckout(CheckoutDTO dto) {
		// TODO Auto-generated method stub
		
	}

}
