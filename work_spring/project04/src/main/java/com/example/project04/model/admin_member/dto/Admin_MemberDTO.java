package com.example.project04.model.admin_member.dto;

import java.util.Date;

public class Admin_MemberDTO {
	private String id;
	private String passwd;
	private String mname;
	private String email;
	private String zipcode1;
	private String zip_num1;
	private String address1;
	private String zipcode2;
	private String zip_num2;
	private String address2;
	private String phone1;
	private String phone2;
	private String useryn;
	private Date indate;
	private int lv;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getzipcode1() {
		return zipcode1;
	}
	public void setzipcode1(String zipcode1) {
		this.zipcode1 = zipcode1;
	}
	public String getZip_num1() {
		return zip_num1;
	}
	public void setZip_num1(String zip_num1) {
		this.zip_num1 = zip_num1;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getzipcode2() {
		return zipcode2;
	}
	public void setzipcode2(String zipcode2) {
		this.zipcode2 = zipcode2;
	}
	public String getZip_num2() {
		return zip_num2;
	}
	public void setZip_num2(String zip_num2) {
		this.zip_num2 = zip_num2;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getUseryn() {
		return useryn;
	}
	public void setUseryn(String useryn) {
		this.useryn = useryn;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	public int getLv() {
		return lv;
	}
	public void setLv(int lv) {
		this.lv = lv;
	}
	@Override
	public String toString() {
		return "Admin_MemberDTO [id=" + id + ", passwd=" + passwd + ", mname=" + mname + ", email=" + email
				+ ", zipcode1=" + zipcode1 + ", zip_num1=" + zip_num1 + ", address1=" + address1 + ", zipcode2="
				+ zipcode2 + ", zip_num2=" + zip_num2 + ", address2=" + address2 + ", phone1=" + phone1 + ", phone2="
				+ phone2 + ", useryn=" + useryn + ", indate=" + indate + ", lv=" + lv + "]";
	}
	
}
