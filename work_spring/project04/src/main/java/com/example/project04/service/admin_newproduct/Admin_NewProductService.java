package com.example.project04.service.admin_newproduct;

import java.util.List;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

public interface Admin_NewProductService {
	public List<Admin_NewProductDTO> listNew(int start,int end);
	//public Admin_NewProductDTO viewNew(int nseq);
	public Admin_NewProductDTO detailProduct(int nseq);
	public void insertNew(Admin_NewProductDTO dto);
	public Admin_NewProductDTO orderProduct(int nseq);
	public int countArticle() throws Exception;
	public List<Admin_NewProductDTO> ordermoney();
	/*public void deleteOrder(Admin_ProductDTO dto);*/
}
