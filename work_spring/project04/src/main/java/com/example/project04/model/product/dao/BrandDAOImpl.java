package com.example.project04.model.product.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.model.product.dto.ProductDTO2;

@Repository
public class BrandDAOImpl implements BrandDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<ProductDTO> listBrand1() {
		return sqlSession.selectList("brand.nike");
	}

	@Override
	public List<ProductDTO> listBrand2() {
		return sqlSession.selectList("brand.fila");	}

	@Override
	public List<ProductDTO> listBrand3() {
		return sqlSession.selectList("brand.kolca");
	}

	@Override
	public List<ProductDTO> listBrand4() {
		return sqlSession.selectList("brand.adidas");
	}

	@Override
	public List<ProductDTO> listBrand5() {
		return sqlSession.selectList("brand.puma");
	}

	@Override
	public List<ProductDTO> listBrand6() {
		return sqlSession.selectList("brand.descente");
	}

	@Override
	public List<ProductDTO> listBrand7() {
		return sqlSession.selectList("brand.vans");
	}

	@Override
	public List<ProductDTO> listBrand8() {
		return sqlSession.selectList("brand.converse");
	}

	@Override
	public List<ProductDTO> listBrand9() {
		return sqlSession.selectList("brand.excelsior");
	}

	@Override
	public List<ProductDTO> listBrand10() {
		return sqlSession.selectList("brand.newbalance");
	}

	@Override
	public List<ProductDTO> listBrand11() {
		return sqlSession.selectList("brand.men");
	}

	@Override
	public List<ProductDTO> listBrand12() {
		return sqlSession.selectList("brand.women");
	}
	
	@Override
	public List<ProductDTO> listBrand13() {
		return sqlSession.selectList("brand.unisex");
	}

	@Override
	public List<ProductDTO2> listCatg1(int dseq) {
		return sqlSession.selectList("brand.catg1", dseq);
	}

	@Override
	public List<ProductDTO2> listCatg2(int dseq) {
		return sqlSession.selectList("brand.catg2", dseq);
	}
	
	@Override
	public List<ProductDTO2> listCatg3(int dseq) {
		return sqlSession.selectList("brand.catg3", dseq);
	}

	@Override
	public List<ProductDTO2> listDseq1(String sort) {
		return sqlSession.selectList("brand.dseq1", sort);
	}

	@Override
	public List<ProductDTO2> listDseq2(String sort) {
		return sqlSession.selectList("brand.dseq2", sort);
	}

	@Override
	public List<ProductDTO2> listDseq3(String sort) {
		return sqlSession.selectList("brand.dseq3", sort);
	}

	@Override
	public List<ProductDTO2> listDseq4(String sort) {
		return sqlSession.selectList("brand.dseq4", sort);
	}

	@Override
	public List<ProductDTO2> listDseq5(String sort) {
		return sqlSession.selectList("brand.dseq5", sort);
	}

	@Override
	public List<ProductDTO2> listDseq6(String sort) {
		return sqlSession.selectList("brand.dseq6", sort);
	}

	@Override
	public List<ProductDTO2> listDseq7(String sort) {
		return sqlSession.selectList("brand.dseq7", sort);
	}

	@Override
	public List<ProductDTO2> listDseq8(String sort) {
		return sqlSession.selectList("brand.dseq8", sort);
	}

	@Override
	public List<ProductDTO2> listDseq9(String sort) {
		return sqlSession.selectList("brand.dseq9", sort);
	}

	@Override
	public List<ProductDTO2> listDseq10(String sort) {
		return sqlSession.selectList("brand.dseq10", sort);
	}
	
}
