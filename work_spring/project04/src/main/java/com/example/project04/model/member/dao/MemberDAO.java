package com.example.project04.model.member.dao;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.project04.model.member.dto.MemberDTO;

public interface MemberDAO {

	public boolean loginCheck(MemberDTO dto);

	public MemberDTO viewMember(String id);
	
	public String idcheck(String id);

	public void join(MemberDTO dto);

	public void memberupdate1(MemberDTO dto, HttpSession session);
	
	public void memberupdate2(MemberDTO dto, HttpSession session);
	
	public void memberupdate3(MemberDTO dto, HttpSession session);

	public String findid(MemberDTO dto);

	public List<MemberDTO> joinview(MemberDTO dto);
	
	public String findpwd(MemberDTO dto);

	public void deleteEnd(String id);

	public boolean deletecheck(String id, String passwd);
	
	public void temporaryPW(MemberDTO dto);
	
	public List<MemberDTO> memberinfo(String id);

	public String findname(String id);
}
