<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>
<script type="text/javascript">
$(function(){
	
	$("#btnSave").click(function(){
		var subject=$("#subject").val();
		var content=$("#content").val();
		if(subject == ""){
			alert("제목을 작성해주세요.");
			$("#subject").focus();	
			return;
		}
		if(content == ""){
			alert("공지사항 내역을 작성해주세요.");
			$("#content").focus();
			return;
		}
		
		document.form1555.action="${path}/notice/insert.do";
		document.form1555.submit();
	});
	//문의하기 작성란으로 이동
	$("#btnList").click(function(){
		location.href="${path}/notice/list.do";
	});
});
	
</script>
<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-16">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table" align="center">
               <h2>공지하기</h2>			
	<form id="form1555" name="form1555" method="post">
	 <div align="left" style="margin-left:212px;"><Strong>공지제목 </Strong></div> 
	 <input name="subject" id="subject" size="81" placeholder="공지하실 내용의 제목을 입력해주세요."><br><br>
			
		<div align="left" style="margin-left:210px;"><Strong>공지사항 </Strong></div><textarea id="content" name="content" rows="15" cols="80" placeholder="내용을 입력하세요"></textarea><br><br>
	<button class="btn btn-primary" type="button" id="btnSave" ><Strong>작성완료</Strong></button>
	<button class="btn btn-primary" type="button" id="btnList" ><Strong>목록으로</Strong></button>
	</form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>



</body>
<%@ include file="../include/Footer.jsp"%>
</html>