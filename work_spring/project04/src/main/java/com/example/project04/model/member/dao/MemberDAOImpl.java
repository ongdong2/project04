package com.example.project04.model.member.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.member.dto.MemberDTO;

@Repository
public class MemberDAOImpl implements MemberDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public boolean loginCheck(MemberDTO dto) {
		System.out.println(dto);
		String name = sqlSession.selectOne("member.check", dto);
		System.out.println(name);
		return (name==null) ? false : true;
	}

	@Override
	public MemberDTO viewMember(String id) {
		return sqlSession.selectOne("member.viewMember", id);
	}

	@Override
	public String idcheck(String id) {
		return sqlSession.selectOne("member.idcheck", id);
	}

	@Override
	public void join(MemberDTO dto) {
		sqlSession.update("member.join", dto);
	}

	@Override
	public void memberupdate1(MemberDTO dto, HttpSession session) {
		sqlSession.update("member.memberupdate1", dto);
	}

	@Override
	public void memberupdate2(MemberDTO dto, HttpSession session) {
		sqlSession.update("member.memberupdate2", dto);
	}

	@Override
	public void memberupdate3(MemberDTO dto, HttpSession session) {
		sqlSession.update("member.memberupdate3", dto);
		
	}

	@Override
	public String findid(MemberDTO dto) {
		return sqlSession.selectOne("member.findid", dto);
	}

	@Override
	public List<MemberDTO> joinview(MemberDTO dto) {
		return sqlSession.selectList("member.joinview", dto);
	}

	@Override
	public String findpwd(MemberDTO dto) {
		return sqlSession.selectOne("member.findpwd", dto);
	}

	@Override
	public void deleteEnd(String id) {
		sqlSession.delete("member.deleteEnd",id);
	}

	@Override
	public boolean deletecheck(String id, String passwd) {
		boolean result=false;
		Map<String,String> map=new HashMap<>();
		map.put("id", id);
		map.put("passwd", passwd);
		int count=sqlSession.selectOne("member.deletecheck",map);
		//비번이 맞으면 1 => true, 틀리면 0 => false 리턴
		if(count==1) result=true;
		return result;
	}

	@Override
	public void temporaryPW(MemberDTO dto) {
		sqlSession.update("member.temporaryPW", dto);
	}

	@Override
	public List<MemberDTO> memberinfo(String id) {
		return sqlSession.selectList("member.memberinfo", id);
	}

	@Override
	public String findname(String id) {
		return sqlSession.selectOne("member.findname",id);
	}
	


}
