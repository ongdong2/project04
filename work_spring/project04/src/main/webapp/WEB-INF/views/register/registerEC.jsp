<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>	
    <%@ include file="../include/header.jsp" %>  
	<%@ include file="../include/menu.jsp" %>
	
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
	$(function(){
		$("#emailconfirm").click(function(){
			var email=$("#email").val();
			if(email == ""){
				alert("이메일을 양식에 맞게 작성해 주세요.")
				return "member/registerEC.do";
		}else{
			alert("입력한 주소로 인증을 위한 이메일이 발송됩니다.")
		}
			document.Eform.action = "${path}/email/send.do";
			document.Eform.submit();
		});
	});
	
	$(function() {
		$("#btncheck").click(function() {
			var num = $("#emailcheck").val();
			var emailnum="${number}";
			if(emailnum == ""){
				alert("이메일 인증을 받으세요.")
			}else if (num == "") {
				alert("인증번호를 입력하세요.")
			}else if(num != emailnum){
				alert("인증번호가 일치하지 않습니다.")
			}else if(num == emailnum){
					alert("이메일인증이 완료되었습니다..")
			document.Eform.action = "${path}/member/register.do";
			document.Eform.submit();
			}
		});
	});
</script>

<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">


</head>




<br><br><br><br>
<div></div>
<br><br><br><br>
<div class="container">
<form method="post" action="registerEC.do" id="Eform" name="Eform">
<br><br><br>
<table class="table table-bordered table-hover" style="width: 500px" align="center">
				
				<thead>
					<tr>
						<th style="width: 550px" colspan="3">
							<h4>회원 등록을 위한 이메일인증</h4>
					</tr>
				</thead>
				
<c:choose>
<c:when test="${receiveMail == null }">
	<tr>
		<td style="width: 150px" align = "center"><br><h5>이메일</h5></td>
		
		<td colspan="2">
			<input class="form-inline; form-control" type="text" placeholder="example@gmail.com"
				style="width: 350px" id="email" name="email" maxLength="20">
			<input class="btn btn-primary" type="button" id="emailconfirm" value="이메일인증">
		</td>
	</tr>
		</c:when>
	<c:otherwise>
	<tr>
		<td style="width: 150px;"><h5>이메일</h5></td>
		<td colspan="2">
			<input class="form-inline; form-control" type="email" value="${receiveMail}"
				style="width: 350px" id="email" name="email" maxLength="20">
			<input class="btn btn-primary" type="button" id="emailconfirm" value="이메일인증">
		</td>
	</tr>
		</c:otherwise>
	</c:choose>

	<tr>
		<td style="width: 150px" align = "center"><br><h5>이메일 인증번호</h5></td>
		<td colspan="2">
			<input class="form-inline; form-control" type="text" placeholder="인증번호 4자리 입력해주세요."
				style="width: 350px" id="emailcheck" name="emailcheck" maxLength="20">
			<input class="btn btn-primary" type="button" id="btncheck" value="인증하기">
		</td>
	</tr>
	</table>
	<br><br>
</form>
</div>
<%@ include file="../include/Footer.jsp" %>
</body>
</html>
