package com.example.project04.controller.admin_dealer;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.service.admin.Pager;
import com.example.project04.service.admin_dealer.Admin_DealerService;

@Controller
@RequestMapping("admin_dealer/*")
public class Admin_DealerController {
	@Inject
	Admin_DealerService dealerService;
	
	@RequestMapping("list.do")
	public ModelAndView dealerList(@RequestParam(defaultValue="1")int curPage) throws Exception {
		int count=dealerService.countArticle();
		
		Pager pager=new Pager(count,curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		List<Admin_DealerDTO> list=dealerService.listDealer(start,end);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", list.size());
		map.put("pager", pager);
		mav.setViewName("admin_dealer/dealer_list");
		mav.addObject("map", map);
		System.out.println(mav);
		return mav;
	}
	@RequestMapping("prolist.do/{dseq}")
	public ModelAndView prodealerList(ModelAndView mav,@PathVariable("dseq") int dseq) {
		mav.setViewName("admin_dealer/dealer_proview");
		mav.addObject("list", dealerService.prolistDealer(dseq));
		System.out.println(mav);
		return mav;
	}
	@RequestMapping("dealerView/{dseq}")
	public ModelAndView view(@PathVariable("dseq") int dseq,ModelAndView mav) {
		mav.setViewName("admin_dealer/dealer_view");
		mav.addObject("dto", dealerService.viewDealer(dseq));
		return mav;
	}
	@RequestMapping("productView/{dseq}")
	public ModelAndView proview(@PathVariable("dseq") int dseq,ModelAndView mav) {
		mav.setViewName("admin_dealer/dealer_proview");
		mav.addObject("dto", dealerService.viewproDealer(dseq));
		return mav;
	}
	@RequestMapping("dealer_delete.do")
	public String delete(Admin_DealerDTO dto) {
		dealerService.deleteDealer(dto);;
		return "redirect:/admin_dealer/list.do";
	}
	@RequestMapping("insert.do")
	public String insert(Admin_DealerDTO dto) {
		dealerService.insertDealer(dto);
		return "redirect:/admin_dealer/list.do";
	}
	@RequestMapping("dealer_write.do")
	public String write() {
		return "/admin_dealer/dealer_write";
	}
	@RequestMapping("dealer_edit/{dseq}")
	public ModelAndView edit(@PathVariable("dseq") int dseq, ModelAndView mav) {
	mav.setViewName("admin_dealer/dealer_edit");
	mav.addObject("dto", dealerService.detailDealer(dseq));
	return mav;
	}
	@RequestMapping("dealer_update.do")
	public String update(Admin_DealerDTO dto) {
		dealerService.updateDealer(dto);
		return "redirect:/admin_dealer/list.do";
	}
	@RequestMapping("dealer_add.do")
	public String add() {
		return "/admin_dealer/dealer_add";
	}
	@RequestMapping("dealer_add1.do")
	public String adddealer(Admin_DealerDTO dto) {
		dealerService.addDealer(dto);;
		return "redirect:/admin_dealer/list.do";
	}
	
}
