package com.example.project04.model.product.dto;

import java.util.Date;

public class ProductDTO {
	private int pseq;
	private String pname;
	private String sort;
	private int price1;
	private int price2;
	private int dseq;
	private int sz230mm;
	private int sz240mm;
	private int sz250mm;
	private int sz260mm;
	private int sz270mm;
	private int sz280mm;
	private String content;
	private String image_url;
	private String useryn;
	private String bestyn;
	private Date indate;
	
	public ProductDTO() {
	}

	public int getPseq() {
		return pseq;
	}

	public void setPseq(int pseq) {
		this.pseq = pseq;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public int getPrice1() {
		return price1;
	}

	public void setPrice1(int price1) {
		this.price1 = price1;
	}

	public int getPrice2() {
		return price2;
	}

	public void setPrice2(int price2) {
		this.price2 = price2;
	}

	public int getDseq() {
		return dseq;
	}

	public void setDseq(int dseq) {
		this.dseq = dseq;
	}

	public int getSz230mm() {
		return sz230mm;
	}

	public void setSz230mm(int sz230mm) {
		this.sz230mm = sz230mm;
	}

	public int getSz240mm() {
		return sz240mm;
	}

	public void setSz240mm(int sz240mm) {
		this.sz240mm = sz240mm;
	}

	public int getSz250mm() {
		return sz250mm;
	}

	public void setSz250mm(int sz250mm) {
		this.sz250mm = sz250mm;
	}

	public int getSz260mm() {
		return sz260mm;
	}

	public void setSz260mm(int sz260mm) {
		this.sz260mm = sz260mm;
	}

	public int getSz270mm() {
		return sz270mm;
	}

	public void setSz270mm(int sz270mm) {
		this.sz270mm = sz270mm;
	}

	public int getSz280mm() {
		return sz280mm;
	}

	public void setSz280mm(int sz280mm) {
		this.sz280mm = sz280mm;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUseryn() {
		return useryn;
	}

	public void setUseryn(String useryn) {
		this.useryn = useryn;
	}

	public String getBestyn() {
		return bestyn;
	}

	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	@Override
	public String toString() {
		return "ProductDTO [pseq=" + pseq + ", pname=" + pname + ", sort=" + sort + ", price1=" + price1 + ", price2="
				+ price2 + ", dseq=" + dseq + ", sz230mm=" + sz230mm + ", sz240mm=" + sz240mm + ", sz250mm=" + sz250mm
				+ ", sz260mm=" + sz260mm + ", sz270mm=" + sz270mm + ", sz280mm=" + sz280mm + ", content=" + content
				+ ", image_url=" + image_url + ", useryn=" + useryn + ", bestyn=" + bestyn + ", indate=" + indate + "]";
	}

}
