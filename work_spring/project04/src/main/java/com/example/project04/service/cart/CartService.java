package com.example.project04.service.cart;

import java.util.List;

import com.example.project04.model.cart.dto.CartDTO;

public interface CartService {
	public List<CartDTO> cartMoney();
	public void insert(CartDTO dto);
	public List<CartDTO> listCart(String id);
	public void show(String id);
	public void delete(int cseq);
	public void deleteSel(int cseq);
	public void pdelete(int pseq);
	public void iddelete(String id);
	public void deleteAll(String id);
	public void update(int cseq);
	public int sumMoney(String id);
	public int countCart(String id, int pseq);
	public void updateCart(CartDTO dto);
	public void modifyCart(CartDTO dto);
}
