package com.example.project04.controller.admin;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;
import com.example.project04.service.admin.AdminService;
import com.example.project04.service.admin_chart.Admin_ChartService;
@RestController
@Controller
@RequestMapping("admin/*")
public class AdminController {
	
	@Inject
	AdminService adminService;
	@Inject
	Admin_ChartService chartService;
	
	
	
	@RequestMapping("adminHome.do")
	public String admin_home() {
		System.out.println("어드민에염!!!!!");
		return "admin/admin";
	}
	@RequestMapping("adminAll.do")
	public ModelAndView listAll(ModelAndView mav) {
		mav.setViewName("admin/admin");
		mav.addObject("list1", adminService.listMember());
		mav.addObject("list2", adminService.listDealer());
		mav.addObject("list3", adminService.listProduct());
		mav.addObject("list4", adminService.listNewproduct());
		System.out.println(adminService.listMember());
		System.out.println(adminService.listDealer());
		System.out.println(adminService.listProduct());
		System.out.println(adminService.listNewproduct());
		return mav;
	}
	@RequestMapping("order_list.do")
	public ModelAndView newProductlist(ModelAndView mav){
		mav.setViewName("admin/order_list");
		/*mav.addObject("list",adminService.orderlist());*/
		mav.addObject("stocklist",adminService.orderstocklist());
		return mav;
	}
	@RequestMapping("admin_chart.do")
	public ModelAndView ordermoneylist() {
		return new ModelAndView("admin/admin_chart");
	}
	@RequestMapping("cart_money_list.do")
	public JSONObject cart_money_list() {
		return chartService.getChartData();
	}
	@RequestMapping("cart_money_list1.do")
	public JSONObject cart_money_list1() {
		return chartService.getChartData1();
	}
	@RequestMapping("cart_money_list2.do")
	public JSONObject cart_money_list2() {
		return chartService.getChartData2();
	}
	@RequestMapping("cart_money_list3.do")
	public JSONObject cart_money_list3() {
		return chartService.getChartData3();
	}
	/*@RequestMapping("pay_list.do")
	public ModelAndView paylist(ModelAndView mav) {
		mav.setViewName("admin/admin_pay");
		mav.addObject("list", adminService.paylist());
		return mav;
	}*/
	/*하이차트*/
	@RequestMapping(value="admin_highchart.do",method=RequestMethod.GET)
	public ModelAndView highchart(HttpServletRequest request,HttpServletResponse response)throws Exception{
		List<Admin_ProductDTO> list=adminService.orderstocklist();
		for(int i=0; i<list.size(); i++) {
			System.out.println(list.get(i).getPname());
		}
		ModelAndView mav=new ModelAndView();
		mav.addObject("list", list);
		mav.setViewName("admin/admin_highchart");
		return mav;
	}
	/*@RequestMapping("order_stocklist.do")
	public ModelAndView stocklist(ModelAndView mav){
		mav.setViewName("admin/order_list");
		mav.addObject("list",adminService.orderstocklist());
		return mav;
	}*/
	
	/*@RequestMapping("login.do")
	public String login() {
		return "admin/admin_login";
	}
	@RequestMapping("login_check.do")
	public ModelAndView login_check(AdminMemberDTO dto,HttpSession session,ModelAndView mav) {
		String name=adminService.loginCheck(dto);
		if(name!=null) {
			session.setAttribute("admin_id", dto.getAdmin_id());
			
			session.setAttribute("admin_lv", dto.getAdmin_lv());
			mav.setViewName("admin/admin_home");
		}else {
			
		mav.setViewName("admin/admin_login");
		mav.addObject("message", "error");
		}
		return mav;
	}
	@RequestMapping("logout.do")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/admin/login.do";
	}*/
	/*@RequestMapping("listall.do")
	public List<AdminListDTO dto>*/
	
	/*@RequestMapping("insert.do")
	public String insert(ProductDTO dto) {
		String filename="";
		if(!dto.getImage().isEmpty()){
            filename = dto.getImage().getOriginalFilename();
            
            String path="D:\\work\\.metadata\\.plugins\\org.eclipse.wst.server.core\\"
					+ "tmp0\\wtpwebapps\\spring02\\WEB-INF\\views\\images\\";
			new File(path).mkdir();//디렉토리가 없으면 생성
            try {
                new File(path).mkdirs(); // 디렉토리 생성
                // 임시디렉토리(서버)에 저장된 파일을 지정된 디렉토리로 전송
                dto.getImage().transferTo(new File(path+filename));
            } catch (Exception e) {
                e.printStackTrace();
            }
            dto.setImage_url(filename);
            productService.insertProduct(dto);
        }
        return "redirect:/admin/productList.do";
    }*/
	}
/*응 아니야
망했어
프로젝트 끝!
잘가
창섭이 짱짱!!
주석처리 안할거지롱
 :D
 :)
:(
웃음웃음
걸려버렸네...............
씨익씨익
안할거지롱
도우미
저장~
딱!
오타 딱!
아이고 예뻐라ㅎㅎㅎㅎ
*/



