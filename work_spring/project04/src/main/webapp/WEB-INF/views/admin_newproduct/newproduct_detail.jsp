<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
   
    $("#btnDelete").click(function(){
       
        if(confirm("선택 상품을 삭제하시겠습니까?")){
            document.form1.action = "${path}/project04/admin_product/product_delete.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_newproduct/list.do";
    });
    $("#btnNew").click(function(){
        location.href = "${path}/project04/admin_newproduct/newproduct_order/${dto.nseq}";
    });
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">신상품상세정보</div>
				<div class="pull-right">
					<span class="badge badge-info">${dto.nname}</span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
							
							<td align="center">
							<tr>
							<th>상품코드</th>
							<td>${dto.nseq }</td>
							</tr>
							<tr>
							<th>상품명</th>
							<td>${dto.nname}</td>
							</tr>
							<tr>
							<th>분류코드</th>
							<td>${dto.sort}</td>
							</tr>
							<tr>
							<th>원가</th>
							<td><fmt:formatNumber value="${dto.price1}"/>원</td>
							</tr>
							<tr>
							<th>판매가</th>
							<td><fmt:formatNumber value="${dto.price2 }"/>원</td>
							</tr>
							<tr>
							<th>거래처코드</th>
							<td>${dto.dseq }</td>
							</tr>
							<tr>
							<th>상품설명</th>
							<td>${dto.content }</td>
							</tr>
							
							
							<tr>
							<th>등록일자</th>
							<td><fmt:formatDate value="${dto.indate }" pattern="yyyy-MM-dd
							HH:mm:ss" /></td>
							</tr>
							<tr>
							<td><img src="${path}/resources/images/${dto.image_url}"
							width="200px" height="200px"></td>
							
							<td><table>
							<tr>
							<th align="justify">사이즈</th>
							<td>230mm</td>
							<td>240mm</td>
							<td>250mm</td>
							<td>260mm</td>
							<td>270mm</td>
							<td>280mm</td>
							</tr>
							<tr>
							<th align="justify">재고</th>
							<td>${dto.sz230mm }</td>
							<td>${dto.sz240mm }</td>
							<td>${dto.sz250mm }</td>
							<td>${dto.sz260mm }</td>
							<td>${dto.sz270mm }</td>
							<td>${dto.sz280mm }</td>
							
							</tr>
							</table> </td>
							</tr>
							
							
							
							
							<tr>
							<td colspan="2" align="center">
							<input type="hidden" value="${dto.nseq }" name="pseq">
								<input type="button" value="삭제" id="btnDelete">
								<input type="button" value="목록" id="btnList">
								<input type="button" value="신상품주문" id="btnNew">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>