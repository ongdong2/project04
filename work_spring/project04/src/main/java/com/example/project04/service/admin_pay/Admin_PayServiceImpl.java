package com.example.project04.service.admin_pay;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.admin_pay.dao.Admin_PayDAO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;

@Service
public class Admin_PayServiceImpl implements Admin_PayService {
	
	@Inject
	Admin_PayDAO adminpayDao;

	@Override
	public List<Admin_PayDTO> paylist() {
		// TODO Auto-generated method stub
		return adminpayDao.paylist();
	}

	@Override
	public Admin_PayDTO orderDetail(int odseq) {
		// TODO Auto-generated method stub
		return adminpayDao.orderDetail(odseq);
	}

	@Override
	public Admin_PayDTO confirmorder(int odseq) {
		// TODO Auto-generated method stub
		return adminpayDao.confirmorder(odseq);
	}
	@Override
	public Admin_PayDTO cancleorder(int odseq) {
		// TODO Auto-generated method stub
		return adminpayDao.cancleorder(odseq);
	}

	@Override
	public void dispatch(Admin_PayDTO dto) {
		// TODO Auto-generated method stub
		
		adminpayDao.dispatch(dto);
	}
	

	

	@Override
	public void insertresult(Admin_PayDTO dto) {
		// TODO Auto-generated method stub
		adminpayDao.insertresult(dto);
	}

	@Override
	public List<Admin_PayDTO> ordermoney1() {
		// TODO Auto-generated method stub
		return adminpayDao.ordermoney1();
	}
	@Override
	public List<Admin_PayDTO> ordermoney2() {
		// TODO Auto-generated method stub
		return adminpayDao.ordermoney2();
	}
	@Override
	public List<Admin_PayDTO> ordermoney3() {
		// TODO Auto-generated method stub
		return adminpayDao.ordermoney3();
	}

	@Override
	public void cutstock(Admin_PayDTO dto) {
		// TODO Auto-generated method stub
		adminpayDao.cutstock(dto);
	}

	@Override
	public List<Admin_PayDTO> dispatchlist() {
		// TODO Auto-generated method stub
		return adminpayDao.dispatchlist();
	}
	@Override
	public List<Admin_PayDTO> dispatchlist2() {
		// TODO Auto-generated method stub
		return adminpayDao.dispatchlist2();
	}
	@Override
	public List<Admin_PayDTO> dispatchlist3() {
		// TODO Auto-generated method stub
		return adminpayDao.dispatchlist3();
	}
	@Override
	public List<Admin_PayDTO> dispatchlist4() {
		// TODO Auto-generated method stub
		return adminpayDao.dispatchlist4();
	}

	@Override
	public void cancleconfirm(Admin_PayDTO dto) {
		adminpayDao.cancleconfirm(dto);
		adminpayDao.cancledone(dto);
	}

	@Override
	public void updateresult(Admin_PayDTO dto) {
		adminpayDao.updateresult(dto);
		
	}

}
