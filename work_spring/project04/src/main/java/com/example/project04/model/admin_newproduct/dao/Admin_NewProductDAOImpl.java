package com.example.project04.model.admin_newproduct.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Repository
public class Admin_NewProductDAOImpl implements Admin_NewProductDAO {
	@Inject
	SqlSession sqlSession;

	@Override
	public List<Admin_NewProductDTO> listNew(int start,int end) {
		Map<String,Object> map=new HashMap<>();
		map.put("start", start);
		map.put("end", end);
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_newproduct.listnew",map);
	}

	@Override
	public Admin_NewProductDTO detailProduct(int nseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_newproduct.detail_new", nseq);
	}
	@Override
	public Admin_NewProductDTO orderProduct(int nseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_newproduct.order_new", nseq);
	}

	@Override
	public void insertNew(Admin_NewProductDTO dto) {
		
		sqlSession.insert("admin_newproduct.insert_new", dto);
		
	}

	@Override
	public void insertNewSize(Admin_NewProductDTO dto) {
		sqlSession.insert("admin_newproduct.insert_size", dto);
		
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_newproduct.countArticle");
	}

	@Override
	public void update(Admin_NewProductDTO dto) {
		sqlSession.update("admin_newproduct.update_newproduct", dto);
		
	}

	@Override
	public List<Admin_NewProductDTO> ordermoney() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_newproduct.ordermoney");
	}

	/*@Override
	public void deleteOrder(Admin_ProductDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.update("admin_newproduct.deleteorder", dto);
	}*/

	@Override
	public void insertStock(Admin_NewProductDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.insert("admin_newproduct.insertstock", dto);
	}

}
