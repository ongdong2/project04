<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<%@ include file="../include/header.jsp" %>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
$(function(){
		$("#btndelete").click(function(){
			document.form8.action="${path}/member/delete.do";
			document.form8.submit();
		});
});
</script>
<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
</head>
<body>
<%@ include file="../include/menu.jsp" %>
<section id="aa-subscribe">
<div class="aa-subscribe-area">
<span class="container">
<form method="post" id="form8" name="form8">
	<table class="table table-bordered table-hover" style="width: 450px" align="center">
	<thead>
		<tr>
	<th style="width: 200px;" colspan="2">
	<h4>비밀번호 재확인</h4>
		</tr>
	</thead>
	<tbody>
		<tr>
	<td>
	<input class="form-control" type="text"	value="${sessionScope.id}" 
		style="width: 100%" id="id" name="id" readonly>
	</td>
		</tr>
		<tr>
	<td>
	<input class="form-control" type="password" placeholder="비밀번호"
	 	style="width: 100%" id="passwd" name="passwd">
	 	<div style="color: red;">${message}</div>
 	</td>
		</tr>
		<tr>
	<td>
	<input class="btn btn-primary pull-right" style="width:100%" 
		type="button" id="btndelete" value="확인">
		
		</tr>
	</tbody>
	</table>
</form>
</span>
</div>
</section>
<%@ include file="../include/Footer.jsp" %>
</body>
</html>