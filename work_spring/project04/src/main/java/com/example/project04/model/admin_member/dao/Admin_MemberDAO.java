package com.example.project04.model.admin_member.dao;

import java.util.List;

import com.example.project04.model.admin_member.dto.Admin_MemberDTO;

public interface Admin_MemberDAO {
	public List<Admin_MemberDTO> listMember(int start,int end);
	public void insertmember(Admin_MemberDTO dto);
	public Admin_MemberDTO viewMember(String id);
	public void deletemember(String id);
	public void updateMember(Admin_MemberDTO dto);
	public boolean checkPw(String userid, String passwd);
	public int countArticle() throws Exception;
}
