package com.example.project04.controller.detail;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project04.service.order.OrderService;

@Controller
@RequestMapping("detail/*")
public class DetailController {

	@Inject
	OrderService orderService;

	@RequestMapping("confirm.do")
	public String confirm(int odseq) {
		orderService.update3(odseq);
		return "redirect:/mypage.do";
	}

	@RequestMapping("cancel.do")
	public String cancel(int odseq) {
		orderService.update4(odseq);
		return "redirect:/mypage.do";
	}

}
