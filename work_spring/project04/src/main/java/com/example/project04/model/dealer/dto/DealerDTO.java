package com.example.project04.model.dealer.dto;

public class DealerDTO {
	private int dseq;
	private String dname;
	private String phone;
	private String address1;
	private String address2;
	private String zip_num;

	public DealerDTO() {
	}

	public int getDseq() {
		return dseq;
	}

	public void setDseq(int dseq) {
		this.dseq = dseq;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getZip_num() {
		return zip_num;
	}

	public void setZip_num(String zip_num) {
		this.zip_num = zip_num;
	}

	@Override
	public String toString() {
		return "DealerDTO [dseq=" + dseq + ", dname=" + dname + ", phone=" + phone + ", address1=" + address1
				+ ", address2=" + address2 + ", zip_num=" + zip_num + "]";
	}

}
