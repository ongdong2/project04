<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
   
    $("#btnOrder").click(function(){
        if(confirm("선택 상품을 주문하시겠습니까?")){
        	var nseq= $("#nseq").val();
    		var nname = $("#nname").val();
    		var sort = $("#sort").val();
    		var price1 = $("#price1").val();
    		var price2 = $("#price2").val();
    		var content = $("#content").val();
    		var dseq = $("#dseq").val();
    		var image_url = $("#image_url").val();
    		var sz230mm = $("#sz230mm").val();
    		var sz240mm = $("#sz240mm").val();
    		var sz250mm = $("#sz250mm").val();
    		var sz260mm = $("#sz260mm").val();
    		var sz270mm = $("#sz270mm").val();
    		var sz280mm = $("#sz280mm").val();
            document.form1.action = "${path}/project04/admin_newproduct/insertNew.do";
            document.form1.submit();
        }
    });
   
    $("#btnList").click(function(){
        location.href = "${path}/project04/admin_newproduct/list.do";
    });
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">신상품주문</div>
				<div class="pull-right">
					<span class="badge badge-info">${dto.nname}</span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
							
							<td align="center">
							
							<tr>
							<th>상품코드</th>
							<td>${dto.nseq }<input type="hidden" value="${dto.nseq }" name="nseq"
							id="nseq"></td>
							</tr>
							<tr>
							<th>상품명</th>
							<td>${dto.nname}<input type="hidden" value="${dto.nname }" name="nname"
							id="nname"></td>
							</tr>
							<tr>
							<th>분류코드</th>
							<td>${dto.sort}<input type="hidden" value="${dto.sort }" name="sort"
							id="sort"></td>
							</tr>
							<tr>
							<th>원가</th>
							<td>${dto.price1}<input type="hidden" value="${dto.price1 }" name="price1"
							id="price1"></td>
							</tr>
							<tr>
							<th>판매가</th>
							<td>${dto.price2 }<input type="hidden" value="${dto.price2 }" name="price2"
							id="price2"></td>
							</tr>
							<tr>
							<th>거래처코드</th>
							<td>${dto.dseq }<input type="hidden" value="${dto.dseq }" name="dseq"
							id="dseq"></td>
							</tr>
							<tr>
							<th>상품설명</th>
							<td>${dto.content }<input type="hidden" value="${dto.content }" name="content"
							id="content"></td>
							</tr>
							<tr>
							<td><img src="${path}/resources/images/${dto.image_url}" 
							width="200px" height="200px"><input type="hidden" id="image_url"
							name="image_url" value="${dto.image_url }"></td>
							
							<tr>
							<th>230mm</th>
							<td>
							<select name="sz230mm" id="sz230mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							<tr>
							<th>240mm</th>
							<td>
							<select name="sz240mm" id="sz240mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							<tr>
							<th>250mm</th>
							<td>
							<select name="sz250mm" id="sz250mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							<tr>
							<th>260mm</th>
							<td>
							<select name="sz260mm" id="sz260mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							<tr>
							<th>270mm</th>
							<td>
							<select name="sz270mm" id="sz270mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							<tr>
							<th>280mm</th>
							<td>
							<select name="sz280mm" id="sz280mm">
								<option value="0" selected>0</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
							</select>
							</td>
							</tr>
							
							<tr>
							<td colspan="2" align="center">
							<input type="hidden" value="${dto.nseq }" name="nseq" id="nseq">
								<input type="button" value="발주신청" id="btnOrder">
								<input type="button" value="목록" id="btnList">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>