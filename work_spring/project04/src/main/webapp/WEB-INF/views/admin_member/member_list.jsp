<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>Admin Home Page</title>

<script type="text/javascript">
	$(document).ready(function() {
		$("#btnAdd").click(function() {
			location.href = "${path}/project04/admin/adminAll.do";
		});
	}); 
	function list(page) {
		location.href = "${path}/project04/admin_member/list.do?curPage=" + page;
	}
</script>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>


<%-- <!--/span-->
<div class="span9" id="content">
	<div class="row-fluid">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4>
				<h2>상품목록</h2>
			</h4>
		</div>
		<div class="navbar">
			<div class="navbar-inner">
				<ul class="breadcrumb">
					<i class="icon-chevron-left hide-sidebar"><a href='#'
						title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
					<i class="icon-chevron-right show-sidebar" style="display: none;"><a
						href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>

				</ul>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">Statistics</div>
				<div class="center">
						<table>
							<tr>
								<th>상품코드</th>
								<th>상품명</th>
								<th>원가</th>
								<th>판매가</th>
							</tr>
							<c:forEach var="row" items="${list}">
								<tr>
									<td align="center">${row.pseq }</td>
									<td align="center">${row.name }</td>
									<td align="center">${row.price1 }</td>
									<td align="center">${row.price2 }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				
			
			</div>
			<div class="block-content collapse in"></div>
		</div>
	</div>
	<!-- /block -->
</div> --%>
<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">회원목록</div>
				<div class="pull-right">
					<span class="badge badge-info"></span>

				</div>
			</div>
			<div class="block-content collapse in">
				<%-- <c:if test="${sessionScope.adminId != null}"> --%>
				
				<br>
				<%-- </c:if> --%>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>아이디</th>
							<th>이름</th>
							<th>이메일<input type="hidden" name="indate" value="${row.indate }"></th>
							<%-- <th>가입일자<input type="hidden" name="indate" value="${row.indate }"></th> --%>

						</tr>
					</thead>
					<tbody>
						<c:forEach var="row" items="${map.list}">
							<tr>
								<td align="center"><a
									href="${path}/admin_member/memberView/${row.id}">
										${row.id }</a></td>
								<td align="center">${row.mname }</td>
								<td align="center">${row.email }</td>
								<%-- <td align="center"><fmt:formatDate value="${row.indate }" 
								pattern="yyyy-MM-dd HH:mm:ss" />
								<input type="hidden" name="indate" value="${row.indate }">
								</td> --%>
							</tr>
						</c:forEach>

					</tbody>
					<tr>
			<td colspan="5" align="center">
			<c:if test="${map.pager.curBlock >1 }">
				<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curBlock >1 }">
				<a href="#" onclick="list('${map.pager.prevPage }')">[이전]</a>
			</c:if>
			<c:forEach var="num" 
					begin="${map.pager.blockBegin }"
					end="${map.pager.blockEnd }">

					<c:choose>
						<c:when test="${num == map.pager.curPage }">
							<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
							<span style="color: red;">${num }</span>
						</c:when>
						<c:otherwise>
							<a href="#" onclick="list('${num}')">${num }</a>

						</c:otherwise>
					</c:choose>

				</c:forEach>
				<c:if test="${map.pager.curBlock<map.pager.totBlock }">
					<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
				</c:if>
				<c:if test="${map.pager.curPage<map.pager.totPage }">
					<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
				</c:if>
				</td>
		</tr>
				</table>
				
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>