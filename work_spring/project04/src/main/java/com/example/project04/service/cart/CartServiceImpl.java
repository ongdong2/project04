package com.example.project04.service.cart;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.cart.dao.CartDAO;
import com.example.project04.model.cart.dto.CartDTO;

@Service
public class CartServiceImpl implements CartService {

	@Inject
	CartDAO cartDao;
	
	@Override
	public List<CartDTO> cartMoney() {
		return cartDao.cartMoney();
	}

	@Override
	public void insert(CartDTO dto) {
		cartDao.insert(dto);
	}

	@Override
	public List<CartDTO> listCart(String id) {
		return cartDao.listCart(id);
	}

	@Override
	public void show(String id) {
		cartDao.show(id);
	}
	
	@Override
	public void delete(int cseq) {
		cartDao.delete(cseq);
	}
	
	@Override
	public void deleteSel(int cseq) {
		cartDao.deleteSel(cseq);
	}

	@Override
	public void pdelete(int pseq) {
		
	}

	@Override
	public void iddelete(String id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(String id) {
		cartDao.deleteAll(id);
	}

	@Override
	public void update(int cseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int sumMoney(String id) {
		return cartDao.sumMoney(id);
	}

	@Override
	public int countCart(String id, int pseq) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCart(CartDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyCart(CartDTO dto) {
		// TODO Auto-generated method stub
		
	}

}
