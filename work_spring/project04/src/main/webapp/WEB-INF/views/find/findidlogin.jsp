<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Find ID/PWD</title>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
	$(function(){
		$("#notfind").click(function(){
			document.form6.action="${path}/member/find.do";
			document.form6.submit();
		});
/* 		$("#findid").click(function(){
			document.form6.action="${path}/member/findidlogin.do";
			document.form6.submit();
		}); */
	});
	
</script>
<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<%@ include file="../include/menu.jsp"%>
	<form method="post" action="register.do" id="form6" name="form6">
		<table class="table table-bordered table-hover" style="width: 500px"
			align="center">
			<thead>
				<tr>
					<th style="width: 500px" colspan="3">
						<h4>아이디 찾기</h4>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="width: 150px;"><h5>
							<c:if test="${message == 'notfind'}">
								검색된 아이디가 없습니다.
							<input class="btn btn-primary pull-right" type="button" id="notfind" value="돌아가기">
							</c:if>
							<c:if test="${message == 'findid'}">
								검색된 아이디는 ${result} 입니다.
								<input class="btn btn-primary pull-right" data-toggle="modal" data-target="#login-modal"
                      type="button" id="findid" value="검색된 아이디로 로그인">
							<!-- <input class="btn btn-primary pull-right" type="button" id="findid" value="검색된 아이디로 로그인"> -->
							</c:if>
							</h5>
					</td>
			</tbody>
		</table>
	</form>

	<%@ include file="../include/Footer.jsp"%>
</body>
</html>