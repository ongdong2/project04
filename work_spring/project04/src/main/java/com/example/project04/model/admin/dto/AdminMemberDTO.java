package com.example.project04.model.admin.dto;

public class AdminMemberDTO {
	private String admin_id;
	private int admin_lv;
	private String passwd;
	private String mname;
	private String phone;
	public AdminMemberDTO() {
		// TODO Auto-generated constructor stub
	}
	public String getAdmin_id() {
		return admin_id;
	}
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	public int getAdmin_lv() {
		return admin_lv;
	}
	public void setAdmin_lv(int admin_lv) {
		this.admin_lv = admin_lv;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "AdminMemberDTO [admin_id=" + admin_id + ", admin_lv=" + admin_lv + ", passwd=" + passwd + ", mname="
				+ mname + ", phone=" + phone + "]";
	}
	
	
}
