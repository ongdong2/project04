package com.example.project04;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.service.dealer.DealerService;
import com.example.project04.service.product.ProductService;

/**
 * Handles requests for the application home page.
 */
@Controller

public class HomeController {
	
	@Inject
	DealerService dealerService;
	
	@Inject
	ProductService productService;
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	
	
	/*-------------productDAO------------
	---------------productService--------			이곳에서 작업했음
	---------------productMapper---------			꿀빨았당 ㅎㅎ*/
	
	
	
	
	@RequestMapping(value = "/")
	public String home( Model model) {
		
		List<ProductDTO> list = productService.home_list();
		model.addAttribute("list",list); 
		System.out.println("꾸꾸는 귀엽다!!!!!"+list);
		
		List<ProductDTO> newlist = productService.home_newlist();
		model.addAttribute("newlist",newlist); 
		System.out.println("꾸꾸는 귀엽다!!!!!"+newlist);
		
		List<ProductDTO> men_list = productService.man_list();
		model.addAttribute("mlist",men_list);
		System.out.println("섭로로는 남자다!!!!!"+men_list);
		
		List<ProductDTO> woman_list = productService.woman_list();
		model.addAttribute("wlist",woman_list);
		System.out.println("섭로로는 여자다!!!!"+woman_list);
		
		List<ProductDTO> public_list = productService.public_list();
		model.addAttribute("plist",public_list);
		System.out.println("섭로로는 공룡이다!!!!"+woman_list);
		
		return "home";
	}
	
	
	
}
