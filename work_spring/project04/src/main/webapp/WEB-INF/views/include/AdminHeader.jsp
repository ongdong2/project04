<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
                <!-- Bootstrap -->
        <link href="${path}/resources/Bootstrap-Admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="${path}/resources/Bootstrap-Admin/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="${path}/resources/Bootstrap-Admin/assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="${path}/admin/adminAll.do">관리자 페이지</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> ${sessionScope.mname} <i class="caret"></i>

                           	     </a>
                                <ul class="dropdown-menu">
                                    <!-- <li>
                                        <a tabindex="-1" href="#">Profile</a>
                                    </li>
                                    <li class="divider"></li> -->
                                    <li><a tabindex="-1" href="${path}/member/logout.do">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li>
                                <a href="${path}/">홈페이지</a>
                            </li>
                           <!--  <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Settings <b class="caret"></b>

                                </a>
                                <ul class="dropdown-menu" id="menu1">
                                    <li>
                                        <a href="#">Tools <i class="icon-arrow-right"></i>

                                        </a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li>
                                                <a href="#">Reports</a>
                                            </li>
                                            <li>
                                                <a href="#">Logs</a>
                                            </li>
                                            <li>
                                                <a href="#">Errors</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">SEO Settings</a>
                                    </li>
                                    <li>
                                        <a href="#">Other Link</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#">Other Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Other Link</a>
                                    </li>
                                </ul>
                            </li> -->
    
                            <!-- <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="#">User List</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="#">Search</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="#">Permissions</a>
                                    </li> -->
                                    
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="${path}/admin/adminAll.do"><i class="icon-chevron-right"></i>관리자 홈</a>
                        </li>
                        <li>
                            <a href="${path}/admin/admin_chart.do"><i class="icon-chevron-right"></i> 차트</a>
                        </li>
                        <li >
                            <a href="${path}/admin_newproduct/list.do"><i class="icon-chevron-right"></i> 신상품</a>
                        </li>
                        <li>
                            <a href="${path}/admin_member/list.do"><i class="icon-chevron-right"></i> 회원관리</a>
                        </li>
                        <li>
                            <a href="${path}/admin_dealer/list.do"><i class="icon-chevron-right"></i> 거래처관리</a>
                        </li>
                        <li>
                            <a href="${path}/admin/order_list.do"><i class="icon-chevron-right"></i> 발주현황</a>
                        </li>
                        <li>
                            <a href="${path}/admin_pay/pay_list.do"><i class="icon-chevron-right"></i> 배송/취소/환불</a>
                        </li>
                        <li>
                            <a href="${path}/admin_product/productList.do"><i class="icon-chevron-right"></i> 상품관리</a>
                        </li>
                        <li>
                            <a href="${path}/admin_product/needlist.do"><i class="icon-chevron-right"></i> 재고부족상품</a>
                        </li>
                        <li>
                            <a href="${path}/notice/list.do"><i class="icon-chevron-right"></i> 공지사항</a>
                        </li>
                        <li>
                            <a href="${path}/inquiry/list.do"><i class="icon-chevron-right"></i> 문의사항</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-success pull-right">731</span> Orders</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-success pull-right">812</span> Invoices</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-info pull-right">27</span> Clients</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-info pull-right">1,234</span> Users</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-info pull-right">2,221</span> Messages</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-info pull-right">11</span> Reports</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-important pull-right">83</span> Errors</a>
                        </li>
                        <li>
                            <a href="#"><span class="badge badge-warning pull-right">4,231</span> Logs</a>
                        </li>
                    </ul>
                </div>

<c:if test="${sessionScope.id ==''}">
  <c:redirect url="{path}/admin/admin_login.do" />
</c:if> 

 