package com.example.project04.service.dealer;

import java.util.List;

import com.example.project04.model.dealer.dto.DealerDTO;

public interface DealerService {
	public List<DealerDTO> listDealer();
}
