package com.example.project04.model.inquiry.dto;

import java.util.Date;

public class AnswerDTO {
 private int ano;
 private int qseq;
 private String answertext;
 private String adminid;
 private Date indate;
public int getAno() {
	return ano;
}
public void setAno(int ano) {
	this.ano = ano;
}
public int getQseq() {
	return qseq;
}
public void setQseq(int qseq) {
	this.qseq = qseq;
}
public String getAnswertext() {
	return answertext;
}
public void setAnswertext(String answertext) {
	this.answertext = answertext;
}
public String getAdminid() {
	return adminid;
}
public void setAdminid(String adminid) {
	this.adminid = adminid;
}
public Date getIndate() {
	return indate;
}
public void setIndate(Date indate) {
	this.indate = indate;
}
@Override
public String toString() {
	return "AnswerDTO [ano=" + ano + ", qseq=" + qseq + ", answertext=" + answertext + ", adminid=" + adminid
			+ ", indate=" + indate + "]";
}
 
 
}
