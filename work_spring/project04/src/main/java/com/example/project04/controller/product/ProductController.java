package com.example.project04.controller.product;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.service.product.ProductService;

@Controller
@RequestMapping("product/*")
public class ProductController {

	@Inject
	ProductService productService;

	@RequestMapping("list.do")
	public ModelAndView list(
		@RequestParam(defaultValue="") String keyword,
		@RequestParam(defaultValue="1") int curPage)
			throws Exception {
		//레코드 갯수 계산
		int count=productService.countArticle(keyword);
		//페이지 관련 설정
		Pager pager=new Pager(count, curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		
		List<ProductDTO> list=
productService.listProduct(keyword,start,end);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", count);
		map.put("pager",pager);
		map.put("keyword",keyword);
		mav.setViewName("/product/product_list");
		mav.addObject("map",map);
		return mav;
	}
	
	@RequestMapping("detail.do")
	public ModelAndView detail(ModelAndView mav,int pseq) throws Exception {
		mav.setViewName("product/product_detail");
		mav.addObject("product", productService.detailProduct(pseq));
		return mav;
	}
	
	@RequestMapping("brand.do")
	public ModelAndView brand(int dseq) {
		ModelAndView mav=new ModelAndView();
		mav.setViewName("/brand/brand_list");
		mav.addObject("brand", productService.brandProduct(dseq));
		return mav;
	}
	
}
