package com.example.project04.service.order;

import java.util.List;

import com.example.project04.model.order.dto.OrderDTO;

public interface OrderService {
	public void cinsert(OrderDTO dto);
	public void oinsert(OrderDTO dto);
	public List<OrderDTO> clistOrder(String id);
	public List<OrderDTO> olistOrder(String id);
	public List<OrderDTO> listOrder(String id);
	public int sumMoney(String id);
	public void update3(int odseq);
	public void update4(int odseq);
	/*public void cutstock(int pseq);
	public List<OrderDTO> listcut(OrderDTO dto);*/
}
