<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	// 상품 등록 유효성검사
	$("#addBtn").click(function() {
		var name = $("#pname").val();
		var sort = $("#sort").val();
		var price1 = $("#price1").val();
		var price2 = $("#price2").val();
		var dseq = $("#dseq").val();
		var content = $("#content").val();
		var image = $("#image").val();

		if (name == "") {
			alert("상품명을 입력해주세요");
			name.foucs();
		} else if (price2 == "") {
			alert("상품 판매가를 입력해주세요");
			price2.focus();
		} else if (price1 == "") {
			alert("상품 원가를 입력해주세요");
			price1.focus();
		} else if (sort == "") {
			alert("상품 분류코드를 입력해주세요");
			sort.focus();
		} else if (dseq == "") {
			alert("상품 판매자코드를 입력해주세요");
			dseq.focus();
		} else if (content == "") {
			alert("상품 설명을 입력해주세요");
			content.focus();
		}
		// 상품 정보 전송
		document.form1.action = "${path}/project04/admin_product/product_edit.do";
		document.form1.submit();
	});
	// 상품 목록이동
	$("#listBtn").click(function() {
		location.href = '${path}/project04/admin_product/productList.do';
	});
});
</script>
<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">상품수정</div>
				<div class="pull-right">
					<span class="badge badge-info">165</span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
							
							<td align="center">
							
							<tr>
							<th>상품명</th>
							<td><input type="text" name="pname" id="pname" readonly></td>
							</tr>
							<tr>
							<th>분류코드</th>
							<td>
							<select name="sort" id="sort">
								<c:forEach begin="1" end="3" var="i">
									<option value="${i}">${i}</option>
								</c:forEach>
							</select>
							</td>
							</tr>
							<tr>
							<th>원가</th>
							<td><input type="text" name="price1" id="price1" >원</td>
							</tr>
							<tr>
							<th>판매가</th>
							<td><input type="text" name="price2" id="price2">원</td>
							</tr>
							<tr>
							<th>거래처코드</th>
							<td><input type="text" name="dealer" id="dealer"></td>
							</tr>
							<tr>
							<th>상품설명</th>
							<td><input type="text" name="content" id="content"></td>
							</tr>
							<tr>
				            <th>상품이미지</th>
				            <td><input type="file" name="image" id="image"></td>
				        	</tr>
							
							
							<tr>
							<td colspan="2" align="center">
								<input type="hidden" value="${dto.pseq }" name="pseq">
								<input type="button" value="등록" id="addBtn">
                				<input type="button" value="목록" id="listBtn">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>