<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
	$(function() {
		$("#btncheckout").click(function() {
			if (confirm("결제하시겠습니까?")) {
				document.form101.action = "${path}/checkend2.do";
				document.form101.submit();
			}
		});
	});
</script>
<script>
	$(function() {
		$("#wish1").change(function() {
			var wishselect = $("#wish1").val();
			$("#wishresult1").val(wishselect);
		});
		$("#wish2").change(function() {
			var wishselect = $("#wish2").val();
			$("#wishresult2").val(wishselect);
		});
		$("#wish3").change(function() {
			var wishselect = $("#wish3").val();
			$("#wishresult3").val(wishselect);
		});
	});
</script>
<script>
	$(function() {
		$("#card1").keyup(function() {
			var card1 = $("#card1").val();
			if (card1.length == 4) {
				$("#card2").focus();
			}
		});
		$("#card2").keyup(function() {
			var card2 = $("#card2").val();
			if (card2.length == 4) {
				$("#card3").focus();
			}
		});
		$("#card3").keyup(function() {
			var card3 = $("#card3").val();
			if (card3.length == 4) {
				$("#card4").focus();
			}
		});
		$("#card4").keyup(function() {
			var card4 = $("#card4").val();
			if (card4.length == 4) {
				$("#cvcnumber").focus();
			}
		});
	});
</script>

<script>
	function searchaddress() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('a1').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('a2').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('a3').focus();
					}
				}).open();
	}
</script>

<!-- catg header banner section -->
<section id="aa-catg-head-banner">
	<img src="${path}/resources/img/fashion/fashion-header-bg-8.jpg"
		alt="fashion img">
	<div class="aa-catg-head-banner-area">
		<div class="container">
			<div class="aa-catg-head-banner-content">
				<h2>Checkout Page</h2>
				<ol class="breadcrumb">
					<li><a href="index.html">Home</a></li>
					<li class="active">Checkout</li>
				</ol>
			</div>
		</div>
	</div>
</section>
<!-- / catg header banner section -->

<!-- Cart view section -->
<section id="checkout">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="checkout-area">
					<form name="form101" id="form101"><h3>
						<div class="row">
							<div class="col-md-8">
								<div class="checkout-left">
									<div class="panel-group" id="accordion">
										<!-- Coupon section -->
										<!-- <div class="panel panel-default aa-checkout-coupon">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            Have a Coupon?
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <input type="text" placeholder="Coupon Code" class="aa-coupon-code">
                          <input type="submit" value="Apply Coupon" class="aa-browse-btn">
                        </div>
                      </div>
                    </div> -->
										<!-- Login section -->
										<!-- <div class="panel panel-default aa-checkout-login">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Client Login 
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat voluptatibus modi pariatur qui reprehenderit asperiores fugiat deleniti praesentium enim incidunt.</p>
                          <input type="text" placeholder="Username or email">
                          <input type="password" placeholder="Password">
                          <button type="submit" class="aa-browse-btn">Login</button>
                          <label for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                          <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                        </div>
                      </div>
                    </div> -->
										<!-- Billing Details -->
										<div class="panel panel-default aa-checkout-billaddress">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
														href="#collapseThree"> 저장된 배송주소록1 </a>
												</h4>
											</div>
											<div id="collapseThree" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${mname}" placeholder="이름*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${email}" placeholder="이메일주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="email" value="${phone1}"
																	placeholder="휴대폰번호*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="tel" value="${zipcode1}" readonly
																	placeholder="우편번호*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${zip_num1}" readonly
																	placeholder="주소*">
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${address1}"
																	placeholder="상세주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<select id="wish1">
																	<option value="">요청사항 선택</option>
																	<option value="경비실에 맡겨주세요.">경비실에 맡겨주세요.</option>
																	<option value="배송전 전화부탁드립니다.">배송전 전화부탁드립니다.</option>

																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<textarea cols="8" rows="3" id="wishresult1"
																	placeholder="요청사항을 입력 해 주세요."></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- 배송주소록2 -->
										<div class="panel panel-default aa-checkout-billaddress">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
														href="#collapseV"> 저장된 배송주소록2 </a>
												</h4>
											</div>
											<div id="collapseV" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${mname}" placeholder="이름*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${email}" placeholder="이메일주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="email" value="${phone2}"
																	placeholder="휴대폰번호*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="tel" value="${zipcode2}" readonly
																	placeholder="우편번호*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${zip_num2}" readonly
																	placeholder="주소*">
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${address2}"
																	placeholder="상세주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<select id="wish2">
																	<option value="">요청사항 선택</option>
																	<option value="경비실에 맡겨주세요.">경비실에 맡겨주세요.</option>
																	<option value="배송전 전화부탁드립니다.">배송전 전화부탁드립니다.</option>

																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<textarea cols="8" rows="3" id="wishresult2"
																	placeholder="요청사항을 입력 해 주세요."></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<!-- 배송주소록 직접입력 -->
										<div class="panel panel-default aa-checkout-billaddress">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordion"
														href="#collapseFive"> 배송주소 직접입력 </a>
												</h4>
											</div>
											<div id="collapseFive" class="panel-collapse collapse">
												<div class="panel-body">
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${mname}" placeholder="이름*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" value="${email}" placeholder="이메일주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="email" value="${phone2}"
																	placeholder="휴대폰번호*">
															</div>
														</div>
														<div class="col-md-6">
															<div class="aa-checkout-single-bill">
																<input type="text" onclick="searchaddress()" id="a1"
																	placeholder="우편번호검색*">




															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" id="a2" value="" readonly
																	placeholder="주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<input type="text" id="a3" value="" placeholder="상세주소*">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<select id="wish3">
																	<option value="">요청사항 선택</option>
																	<option value="경비실에 맡겨주세요.">경비실에 맡겨주세요.</option>
																	<option value="배송전 전화부탁드립니다.">배송전 전화부탁드립니다.</option>
																</select>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="aa-checkout-single-bill">
																<textarea cols="8" rows="3" id="wishresult3"
																	placeholder="요청사항을 입력 해 주세요."></textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="checkout-right">
									<h4>Order Summary</h4>
									<div class="aa-order-summary-area">
							<h5>
										<table class="table table-responsive">
											<thead>
												<tr>
													<th>제품명</th>
													<th>가격</th>
												</tr>
											</thead>
											<c:forEach var="row" items="${map.dlist}">
												<input type="hidden" name="oseq" value="${row.oseq}">
												<input type="hidden" name="pseq" value="${row.pseq}">
												<input type="hidden" name="sz230mm" value="${row.sz230mm}">
												<input type="hidden" name="sz240mm" value="${row.sz240mm}">
												<input type="hidden" name="sz250mm" value="${row.sz250mm}">
												<input type="hidden" name="sz260mm" value="${row.sz260mm}">
												<input type="hidden" name="sz270mm" value="${row.sz270mm}">
												<input type="hidden" name="sz280mm" value="${row.sz280mm}">
												<input type="hidden" name="price2" value="${row.price2}">
												<input type="hidden" name="image_url" value="${row.image_url}">
											<tbody>
												<tr>
													<td>${row.pname}<strong> x ${row.amount}</strong></td>
													<td>${row.total}</td>
												</tr>
												<!--  <tr>
                          <td>Polo T-Shirt <strong> x  1</strong></td>
                          <td>$250</td>
                        </tr>
                        <tr>
                          <td>Shoes <strong> x  1</strong></td>
                          <td>$350</td>
                        </tr> -->
											</tbody>
											</c:forEach>
											<tfoot>
												<tr>
													<th>Total</th>

													<td>${map.dsumMoney}</td>
												</tr>
											</tfoot>
										</table>
									</h5>
									</div>
									<h4>Payment Method</h4>
									<div class="aa-payment-method">
										<label for="cashdelivery"><input type="radio"
											id="cashdelivery" name="optionsRadios"> Cash on
											Delivery </label> <label for="paypal"><input type="radio"
											id="paypal" name="optionsRadios" checked> Via Paypal
										</label> <img
											src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg"
											border="0" alt="PayPal Acceptance Mark"> <input
											type="button" data-toggle="modal" data-target="#check-out"
											value="Place Order" class="aa-browse-btn">
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- / Cart view section -->

<%@ include file="../include/Footer.jsp"%>


<!-- Login Modal -->
<div class="modal fade" id="check-out" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">




				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>

				<h4 style="font-weight: bold;">Product Payment</h4>




				<form id="form13" name="form13" method="post" class="aa-login-form">

					<label for="">Price<span>*</span></label> <input
						style="width: 100px" name="price" id="price" type="text"
						value="${map.dsumMoney}원" readonly> <br>

					<p>
						<select style="width: 110px; height: 25px" id="selectoption">
							<option value="">Select Card</option>
							<option value="">신한은행</option>
							<option value="">우리은행</option>
							<option value="">기업은행</option>
							<option value="">미래은행</option>
						</select>
					</p>

					<label for="">Card Number<span>*</span></label> <br> <input
						style="width: 100px" name="card1" id="card1" type="text">
					- <input style="width: 100px" name="card2" id="card2"
						type="password"> - <input style="width: 100px"
						name="card3" id="card3" type="password"> - <input
						style="width: 100px" name="card4" id="card4" type="text"><br>

					<label for="">CVC Number<span>*</span></label> <br> <input
						style="width: 100px" name="cvcnumber" id="cvcnumber"
						type="password"> A three-digit number at the end of a card<br>

					<button type="button" class="aa-browse-btn pull-right"
						id="btncheckout">결제하기</button>


					<label> </label>
					<div class="aa-register-now">
						<a href="${path}/member/register.do"> </a>
					</div>
				</form>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
</body>
</html>