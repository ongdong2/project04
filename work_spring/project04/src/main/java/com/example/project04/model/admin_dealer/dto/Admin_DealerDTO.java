package com.example.project04.model.admin_dealer.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class Admin_DealerDTO {
	private int dseq;
	private String dname;
	private String phone;
	private String address1;
	private String address2;
	private String zip_num;
	private int pseq;
	private String pname;
	private String sort;
	private int price1;
	private int price2;
	
	private String content;
	private String image_url;
	private MultipartFile image;
	private String useyn;
	private String bestyn;
	private Date indate;
	public Admin_DealerDTO() {
		// TODO Auto-generated constructor stub
	}
	public int getDseq() {
		return dseq;
	}
	public void setDseq(int dseq) {
		this.dseq = dseq;
	}
	public String getDname() {
		return dname;
	}
	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getZip_num() {
		return zip_num;
	}
	public void setZip_num(String zip_num) {
		this.zip_num = zip_num;
	}
	public int getPseq() {
		return pseq;
	}
	public void setPseq(int pseq) {
		this.pseq = pseq;
	}
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getPrice1() {
		return price1;
	}
	public void setPrice1(int price1) {
		this.price1 = price1;
	}
	public int getPrice2() {
		return price2;
	}
	public void setPrice2(int price2) {
		this.price2 = price2;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public MultipartFile getImage() {
		return image;
	}
	public void setImage(MultipartFile image) {
		this.image = image;
	}
	public String getUseyn() {
		return useyn;
	}
	public void setUseyn(String useyn) {
		this.useyn = useyn;
	}
	public String getBestyn() {
		return bestyn;
	}
	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}
	public Date getIndate() {
		return indate;
	}
	public void setIndate(Date indate) {
		this.indate = indate;
	}
	@Override
	public String toString() {
		return "Admin_DealerDTO [dseq=" + dseq + ", dname=" + dname + ", phone=" + phone + ", address1=" + address1
				+ ", address2=" + address2 + ", zip_num=" + zip_num + ", pseq=" + pseq + ", pname=" + pname + ", sort="
				+ sort + ", price1=" + price1 + ", price2=" + price2 + ", content=" + content + ", image_url="
				+ image_url + ", image=" + image + ", useyn=" + useyn + ", bestyn=" + bestyn + ", indate=" + indate
				+ "]";
	}
	
}
