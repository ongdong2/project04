package com.example.project04.model.order.dto;

import java.util.Date;

public class OrderDTO {
	private int odseq;
	
	private String mname;
	private String id;
	private String passwd;
	private String email;
	private String phone1;
	private String phone2;
	private String zipcode1;
	private String zipcode2;
	private String zip_num1;
	private String zip_num2;
	private String address1;
	private String address2;
	private int lv;

	private int pseq;
	private String pname;
	private String sort;
	private int price1;
	private int price2;
	private String content;
	private String image_url;
	private String useyn;
	private String bestyn;
	private int sz230mm;
	private int sz240mm;
	private int sz250mm;
	private int sz260mm;
	private int sz270mm;
	private int sz280mm;

	private int dseq;
	private String dname;
	private String phone; //판매처전화

	private int cseq;
	private int amount;
	private int total;
	
	private int oseq;
	
	private String result;
	private Date indate; //주문테이블 indate
	
	public OrderDTO() {
	}

	public int getOdseq() {
		return odseq;
	}

	public void setOdseq(int odseq) {
		this.odseq = odseq;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getZipcode1() {
		return zipcode1;
	}

	public void setZipcode1(String zipcode1) {
		this.zipcode1 = zipcode1;
	}

	public String getZipcode2() {
		return zipcode2;
	}

	public void setZipcode2(String zipcode2) {
		this.zipcode2 = zipcode2;
	}

	public String getZip_num1() {
		return zip_num1;
	}

	public void setZip_num1(String zip_num1) {
		this.zip_num1 = zip_num1;
	}

	public String getZip_num2() {
		return zip_num2;
	}

	public void setZip_num2(String zip_num2) {
		this.zip_num2 = zip_num2;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public int getLv() {
		return lv;
	}

	public void setLv(int lv) {
		this.lv = lv;
	}

	public int getPseq() {
		return pseq;
	}

	public void setPseq(int pseq) {
		this.pseq = pseq;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public int getPrice1() {
		return price1;
	}

	public void setPrice1(int price1) {
		this.price1 = price1;
	}

	public int getPrice2() {
		return price2;
	}

	public void setPrice2(int price2) {
		this.price2 = price2;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public String getUseyn() {
		return useyn;
	}

	public void setUseyn(String useyn) {
		this.useyn = useyn;
	}

	public String getBestyn() {
		return bestyn;
	}

	public void setBestyn(String bestyn) {
		this.bestyn = bestyn;
	}

	public int getSz230mm() {
		return sz230mm;
	}

	public void setSz230mm(int sz230mm) {
		this.sz230mm = sz230mm;
	}

	public int getSz240mm() {
		return sz240mm;
	}

	public void setSz240mm(int sz240mm) {
		this.sz240mm = sz240mm;
	}

	public int getSz250mm() {
		return sz250mm;
	}

	public void setSz250mm(int sz250mm) {
		this.sz250mm = sz250mm;
	}

	public int getSz260mm() {
		return sz260mm;
	}

	public void setSz260mm(int sz260mm) {
		this.sz260mm = sz260mm;
	}

	public int getSz270mm() {
		return sz270mm;
	}

	public void setSz270mm(int sz270mm) {
		this.sz270mm = sz270mm;
	}

	public int getSz280mm() {
		return sz280mm;
	}

	public void setSz280mm(int sz280mm) {
		this.sz280mm = sz280mm;
	}

	public int getDseq() {
		return dseq;
	}

	public void setDseq(int dseq) {
		this.dseq = dseq;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getCseq() {
		return cseq;
	}

	public void setCseq(int cseq) {
		this.cseq = cseq;
	}

	public int getAmount() {
		amount = sz230mm+sz240mm+sz250mm+sz260mm+sz270mm+sz280mm;
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getTotal() {
		total = (sz230mm+sz240mm+sz250mm+sz260mm+sz270mm+sz280mm) * price2;
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getOseq() {
		return oseq;
	}

	public void setOseq(int oseq) {
		this.oseq = oseq;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Date getIndate() {
		return indate;
	}

	public void setIndate(Date indate) {
		this.indate = indate;
	}

	@Override
	public String toString() {
		return "OrderDTO [odseq=" + odseq + ", mname=" + mname + ", id=" + id + ", passwd=" + passwd + ", email="
				+ email + ", phone1=" + phone1 + ", phone2=" + phone2 + ", zipcode1=" + zipcode1 + ", zipcode2="
				+ zipcode2 + ", zip_num1=" + zip_num1 + ", zip_num2=" + zip_num2 + ", address1=" + address1
				+ ", address2=" + address2 + ", lv=" + lv + ", pseq=" + pseq + ", pname=" + pname + ", sort=" + sort
				+ ", price1=" + price1 + ", price2=" + price2 + ", content=" + content + ", image_url=" + image_url
				+ ", useyn=" + useyn + ", bestyn=" + bestyn + ", sz230mm=" + sz230mm + ", sz240mm=" + sz240mm
				+ ", sz250mm=" + sz250mm + ", sz260mm=" + sz260mm + ", sz270mm=" + sz270mm + ", sz280mm=" + sz280mm
				+ ", dseq=" + dseq + ", dname=" + dname + ", phone=" + phone + ", cseq=" + cseq + ", amount=" + amount
				+ ", total=" + total + ", oseq=" + oseq + ", result=" + result + ", indate=" + indate + "]";
	}

}
