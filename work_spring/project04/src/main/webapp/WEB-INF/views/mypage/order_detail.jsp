<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<!-- <script>
	$(function() {
		$("#cancel").click(function() {
			if(confirm("취소하시겠습니까?")) {
				document.form13.action = "${path}/detail/cancel.do?odseq="+odseq;
				document.form13.submit();
			}
		});
	});
</script> -->
<script>
	function show(odseq) {
		hiddenandshow = $("#hidden_" + odseq).css("display");
		if (hiddenandshow == "none") {
			$("#hidden_" + odseq).css("display", "");
		} else {
			$("#hidden_" + odseq).css("display", "none");
		}
	}
</script>
<!-- catg header banner section -->

<!-- seop /Cart PAGE 백그라운드 -->

<img src="${path}/resources/img/banner/mypage_banner.jpg" alt="banner"
	height="300"
	style="margin-left: auto; margin-right: auto; display: block;">
<div class="aa-catg-head-banner-area">
	<div class="container">
		<div class="aa-catg-head-banner-content"></div>
	</div>
</div>
<!-- / catg header banner section -->
<!-- Cart view section -->
<section id="cart-view">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="cart-view-area">
					<div class="cart-view-table">
						<!-- <form id="form1" name="form1"> -->
						<div class="table-responsive">
							<table class="table">
								<form name="form1" method="post">
									<!-- <table border="1" width="400px"> -->
									<thead>
										<tr>
											<th style="width: 140px; height: 30px;">주문일자</th>
											<th style="width: 140px; height: 30px;">&nbsp;</th>
											<th style="width: 350px; height: 30px;">상 품 명</th>
											<th style="width: 100px; height: 30px;">가 격</th>
											<th style="width: 70px; height: 30px;">수 량</th>
											<th style="width: 100px; height: 30px;">주문금액</th>
											<th style="width: 150px; height: 30px;">&nbsp;</th>
											<th style="width: 150px; height: 30px;">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="order" items="${map.clist}">
											<tr>
												<td style="width: 117px;"><fmt:formatDate
														value="${order.indate}" pattern="yy-MM-dd" /> <br> <fmt:formatDate
														value="${order.indate}" pattern="HH:dd:ss" /></td>
												<td><a
													href="${path}/product/detail.do?pseq=${order.pseq}"><img
														src="${path}/resources/img/shoes/${order.image_url}"
														alt="img"></a></td>
												<td><a href="#" class="viewhidden"
													onclick="show('${order.odseq}')" class="button">${order.pname}</a></td>
												<td>￦<fmt:formatNumber value="${order.price2}" /></td>
												<td>${order.amount}</td>
												<td>￦<fmt:formatNumber value="${order.total}" /></td>
												<td><c:choose>
														<c:when test="${order.result=='1'}">
														배송대기중
													</c:when>
														<c:when test="${order.result=='2'}">
														배송완료<br>
															<input type="button" data-toggle="modal"
																data-target="#cancel222" value="배송추적">
														</c:when>
														<c:when test="${order.result=='3'}">
														구매완료
													</c:when>
														<c:when test="${order.result=='4'}">
														취소대기
													</c:when>
														<c:when test="${order.result=='5'}">
														취소완료
													</c:when>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${order.result=='1' || order.result=='2'}">
															<a
																href="${page}/project04/detail/confirm.do?odseq=${order.odseq}">구매확정</a>
															<br>
															<a
																href="${page}/project04/detail/cancel.do?odseq=${order.odseq}">취소/교환</a>
														</c:when>
														<c:when
															test="${order.result=='3' || order.result=='4' || order.result=='5'}">
														-
													</c:when>
													</c:choose> <!-- <input type="button" data-toggle="modal" data-target="#cancel" value="취소/교환"> -->
													<%-- <a href="${page}/project04/detail/cancel.do?odseq=${order.odseq}">취소/교환</a> --%>
												</td>
											<tr id="hidden_${order.odseq}" style="display: none">
												<td colspan="13" align="left""><br>
													<p align="left" style="margin-left: 120px;">주문번호 :
														${order.odseq}</p>
													<p align="left" style="margin-left: 120px;">
														사 이 즈 :
														<c:choose>
															<c:when test="${order.sz230mm!=0}">
																<v>230(${order.sz230mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz240mm!=0}">
																<v>240(${order.sz240mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz250mm!=0}">
																<v>250(${order.sz250mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz260mm!=0}">
																<v>260(${order.sz260mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz270mm!=0}">
																<v>270(${order.sz270mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz280mm!=0}">
																<v>280(${order.sz280mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
													</p> <c:choose>
														<c:when test="${order.address2 == null}">
															<p align="left" style="margin-left: 120px;">받는사람 :
																${sessionScope.mname}</p>
															<p align="left" style="margin-left: 120px;">전화번호 :
																${sessionScope.phone1}</p>
															<p align="left" style="margin-left: 120px;">배송주소 :
																${order.zip_num1}${order.address1}</p>
															<p align="left" style="margin-left: 120px;">요청사항 :
																${order.phone2}</p></td>
												</c:when>
												<c:when test="${order.address2 != null}">
													<p align="left" style="margin-left: 120px;">받는사람 :
														${sessionScope.mname}</p>
													<p align="left" style="margin-left: 120px;">전화번호 :
														${sessionScope.phone1}</p>
													<p align="left" style="margin-left: 120px;">배송주소 :
														${order.zip_num2}${order.address2}</p>
													<p align="left" style="margin-left: 120px;">요청사항 :
														${order.phone2}</p>
													</td>
												</c:when>
												</c:choose>
											</tr>

										</c:forEach>

										<c:forEach var="order" items="${map.olist}">
											<tr>
												<td style="width: 117px;"><fmt:formatDate
														value="${order.indate}" pattern="yy-MM-dd" /> <br> <fmt:formatDate
														value="${order.indate}" pattern="HH:dd:ss" /></td>
												<td><a
													href="${path}/product/detail.do?pseq=${order.pseq}"><img
														src="${path}/resources/img/shoes/${order.image_url}"
														alt="img"></a></td>
												<td><a href="#" class="viewhidden"
													onclick="show('${order.odseq}')" class="button">${order.pname}</a></td>
												<td>￦<fmt:formatNumber value="${order.price2}" /></td>
												<td>${order.amount}</td>
												<td>￦<fmt:formatNumber value="${order.total}" /></td>
												<td><c:choose>
														<c:when test="${order.result=='1'}">
														배송대기중
													</c:when>
														<c:when test="${order.result=='2'}">
														배송완료<br>
															<input type="button" data-toggle="modal"
																data-target="#cancel222" value="배송추적">
														</c:when>
														<c:when test="${order.result=='3'}">
														구매완료
													</c:when>
														<c:when test="${order.result=='4'}">
														취소대기
													</c:when>
														<c:when test="${order.result=='5'}">
														취소완료
													</c:when>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${order.result=='1' || order.result=='2'}">
															<a
																href="${page}/project04/detail/confirm.do?odseq=${order.odseq}">구매확정</a>
															<br>
															<a
																href="${page}/project04/detail/cancel.do?odseq=${order.odseq}">취소/교환</a>
														</c:when>
														<c:when
															test="${order.result=='3' || order.result=='4' || order.result=='5'}">
														-
													</c:when>
													</c:choose> <!-- <input type="button" data-toggle="modal" data-target="#cancel" value="취소/교환"> -->
													<%-- <a href="${page}/project04/detail/cancel.do?odseq=${order.odseq}">취소/교환</a> --%>
												</td>
											<tr id="hidden_${order.odseq}" style="display: none">
												<td colspan="13" align="left"><br>
													<p align="left" style="margin-left: 120px;">주문번호 :
														${order.odseq}</p>
													<p align="left" style="margin-left: 120px;">
														사 이 즈 :
														<c:choose>
															<c:when test="${order.sz230mm!=0}">
																<v>230(${order.sz230mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz240mm!=0}">
																<v>240(${order.sz240mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz250mm!=0}">
																<v>250(${order.sz250mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz260mm!=0}">
																<v>260(${order.sz260mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz270mm!=0}">
																<v>270(${order.sz270mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
														<c:choose>
															<c:when test="${order.sz280mm!=0}">
																<v>280(${order.sz280mm})&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</v>
															</c:when>
														</c:choose>
													</p>
												<c:choose>
													<c:when test="${order.address2 == null}">
													<p align="left" style="margin-left: 120px;">받는사람 : ${sessionScope.mname}</p>
													<p align="left" style="margin-left: 120px;">전화번호 : ${sessionScope.phone1}</p>
													<p align="left" style="margin-left: 120px;">배송주소 : ${order.zip_num1}${order.address1}</p>
													<p align="left" style="margin-left: 120px;">요청사항 : ${order.phone2}</p></td>
													</c:when>
													<c:when test="${order.address2 != null}">
													<p align="left" style="margin-left: 120px;">받는사람 : ${sessionScope.mname}</p>
													<p align="left" style="margin-left: 120px;">전화번호 : ${sessionScope.phone1}</p>
													<p align="left" style="margin-left: 120px;">배송주소 : ${order.zip_num2}${order.address2}</p>
													<p align="left" style="margin-left: 120px;">요청사항 : ${order.phone2}</p></td>													
													</c:when>
													</c:choose>
											</tr>
											
										</c:forEach>
									</tbody>
							</table>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
</section>
<!-- / Cart view section -->


<%@ include file="../include/Footer.jsp"%>

<!-- Login Modal -->
<div class="modal fade" id="cancel222" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">&times;</button>

				<h3 style="font-weight: bold; margin-left: 40px;">배 송 조 회</h3>
				<hr>
				<img src="${path}/resources/images/package.jpg" alt="package.jpg"
					height="220"
					style="margin-left: auto; margin-right: auto; display: block;"><br>
				<table border="1" align="center" width="500px">
					<thead align="center">
						<tr>
							<td>날 짜</td>
							<td>현재위치</td>
							<td>배송상태</td>
							<td>상세정보</td>
						</tr>
					</thead>
					<tbody align="center">
						<tr>
							<td>2018-03-15<br>20:03
							</td>
							<td>광진</td>
							<td>화물입고</td>
							<td>-</td>
						</tr>
						<tr>
							<td>2018-03-15<br>20:03
							</td>
							<td>광진</td>
							<td>C/T적임</td>
							<td>-</td>
						</tr>
						<tr>
							<td>2018-03-15<br>23:32
							</td>
							<td>광진</td>
							<td>화물출발</td>
							<td>-</td>
						</tr>
						<tr>
							<td>2018-03-16<br>02:43
							</td>
							<td>남서울</td>
							<td>화물도착</td>
							<td>070-4849-0540</td>
						</tr>
						<tr>
							<td>2018-03-16<br>07:13
							</td>
							<td>남서울</td>
							<td>C/T해체</td>
							<td>070-4849-0540</td>
						</tr>
						<tr>
							<td>2018-03-16<br>15:08
							</td>
							<td>신노량진</td>
							<td>배송출발</td>
							<td>070-4064-1187</td>
						</tr>
						<tr>
							<td>2018-03-16<br>16:43
							</td>
							<td>신노량진</td>
							<td>배송완료</td>
							<td>070-4064-1187</td>
						</tr>
					</tbody>
				</table>
				<!-- <form id="form13" name="form13" method="post" class="aa-login-form">

					<label for="">취소 / 교환 사유<span>*</span></label>
						<select style="width: 500px; height: 40px" id="selectoption">
							<option value="">선택하세요</option>
							<option value="">구매하고 싶지 않거나 색상 사이즈 잘못 선택</option>
							<option value="">배송된 상품 파손/하자/불량, 상품 미도착</option>
							<option value="">배송지연 및 상품품질 불만족</option>
							<option value="">기타</option>
						</select><br><br>

					<label for="">기타 사유를 적어주세요</label> <br> <input
						style="width: 500px" name="card1" id="card1" type="text">

				<button type="button" class="aa-browse-btn pull-right" id="cancel">확인</button> -->

				<label> </label>
				<div class="aa-register-now">
					<a href="${path}/member/register.do"> </a>
				</div>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

</body>
</html>