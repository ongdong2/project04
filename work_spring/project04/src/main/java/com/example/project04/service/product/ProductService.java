package com.example.project04.service.product;

import java.util.List;

import com.example.project04.model.dealer.dto.DealerDTO;
import com.example.project04.model.product.dto.ProductDTO;

public interface ProductService {
	List<ProductDTO> listProduct(String keyword,int start, int end)throws Exception;
	ProductDTO detailProduct(int pseq);
	void updateProduct(ProductDTO dto);
	void deleteProduct(int pseq);
	void insertProduct(ProductDTO dto);
	String fileInfo(int pseq);
	List<ProductDTO> home_list();		//Home
	List<ProductDTO> home_newlist();		//Home
	List<ProductDTO> man_list();		//Home
	List<ProductDTO> woman_list();		//Home
	List<ProductDTO> public_list();		//Home
	DealerDTO brandProduct(int dseq);
	public int countArticle(String keyword) throws Exception;
}
