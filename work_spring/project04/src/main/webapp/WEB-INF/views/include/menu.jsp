<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop</title>


<link rel="shortcut icon" href="${path}/resources/img/title.ico">
<!-- Font awesome -->
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
	$(function() {

		$("#btnLogin").click(function() {
			var id = $("#userid").val();
			var passwd = $("#passwd").val();
			if (id == "") {
				alert("아이디를 입력하세요.")
				$("#userid").focus();
				return;
			}
			if (passwd == "") {
				alert("비밀번호를 입력하세요.")
				$("#passwd").focus();
				return;
			}

			document.form00	.action = "${path}/member/login.do";
			document.form00.submit();

		});
	});
</script>

</head>
<body>
	<%-- 	<c:if test="${message == 'error'}">
		<script>
			alert("아이디또는 비밀번호가 틀렸습니다.")
		</script>
	</c:if> --%>

	<!-- wpf loader Two -->
	<div id="wpf-loader-two">
		<div class="wpf-loader-two-inner">
			<span>Loading</span>
		</div>
	</div>
	<!-- / wpf loader Two -->
	<!-- SCROLL TOP BUTTON -->
	<a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
	<!-- END SCROLL TOP BUTTON -->


	<!-- Start header section -->
	<header id="aa-header">
		<!-- start header top  -->
		<div class="aa-header-top">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="aa-header-top-area">
							<!-- start header top left -->
							<div class="aa-header-top-left">
								<!-- start language -->
								<div class="aa-language">
									<div class="dropdown">
										<a class="btn dropdown-toggle" href="#" type="button"
											id="dropdownMenu1" data-toggle="dropdown"
											aria-haspopup="true" aria-expanded="true"> <img
											src="${path}/resources/img/flag/koreanflag.png"
											alt="english flag">대한민국 <span class="caret"></span>
										</a>
									</div>
								</div>
								<!-- / language -->

								<!-- start currency -->
								<div class="aa-currency">
									<div class="dropdown">
										<a class="btn dropdown-toggle" href="#" type="button"
											id="dropdownMenu1" data-toggle="dropdown"
											aria-haspopup="true" aria-expanded="true"> <i
											class="fa fa-krw"></i>원 <span class="caret"></span>
										</a>
									</div>
								</div>
								<!-- / currency -->
								<!-- start cellphone -->
								<div class="cellphone hidden-xs">
									<p>
										<span class="fa fa-phone"></span>010-2032-3341
									</p>
								</div>
								<!-- / cellphone -->
							</div>
							<!-- / header top left -->
							<div class="aa-header-top-right">
								<ul class="aa-head-top-nav-right">

									<li class="hidden-xs"><a href="${path}/cart.do">장바구니</a></li>
									<c:choose>
										<c:when test="${sessionScope.id == null }">
											<li><a href="#" data-toggle="modal"
												data-target="#login-modal">로그인</a></li>
											<li class="hidden-xs"><a href="${path}/notice/list.do">공지사항</a></li>
											<li class="hidden-xs"><a href="${path}/member/registerEC.do">회원가입</a></li>
											<li class="hidden-xs"><a href="${path}/member/find.do">아이디/비밀번호찾기</a></li>
										</c:when>
										<c:when test="${sessionScope.lv == 1 }">
											<li class="hidden-xs"><a href=#>${sessionScope.mname}님 환영합니다♥</a></li>
											<li class="hidden-xs"><a href="${path}/admin/adminAll.do">관리자페이지</a></li>
											<li class="hidden-xs"><a href="${path}/notice/list.do">공지사항</a></li>
											<li class="hidden-xs"><a href="${path}/inquiry/list.do">상품문의</a></li>
											<li class="hidden-xs"><a href="${path}/member/logout.do">로그아웃</a></li>
										</c:when>
										<c:otherwise>
											<li class="hidden-xs"><a href=#>${sessionScope.mname}님 환영합니다♥</a></li>
											<li class="hidden-xs"><a href="${path}/member/mypage.do">내정보</a></li>
											<li class="hidden-xs"><a href="${path}/mypage.do">마이페이지</a></li>
											<li class="hidden-xs"><a href="${path}/notice/list.do">공지사항</a></li>
											<li class="hidden-xs"><a href="${path}/inquiry/list.do">상품문의</a></li>
											<li class="hidden-xs"><a href="${path}/member/logout.do">로그아웃</a></li>
										</c:otherwise>
									</c:choose>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / header top  -->

		<!-- start header bottom  -->
		<div class="aa-header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="aa-header-bottom-area">
							<!-- logo  -->
							<div class="aa-logo">
								<!-- Text based logo -->
								<a href="${path}/"> <span class="fa fa-shopping-cart"></span>
									<p>
										<strong>KkuKkuShop</strong> <span>안녕하세요'^'♥</span>
									</p>
								</a>
								<!-- img based logo -->
								<!-- <a href="index.html"><img src="${path}/resources/img/logo.jpg" alt="logo img"></a> -->
							</div>
							<!-- / logo  -->

							<!-- 쇼핑카트 세션id확인 -->




							<!-- cart box -->
							<%-- <c:choose>
								<c:when test="${sessionScope.id != null }">
									<div class="aa-cartbox">
										<a class="aa-cart-link" href="#"> <span
											class="fa fa-shopping-basket"></span> <span
											class="aa-cart-title">쇼핑카트</span> <span
											class="aa-cart-notify">2</span>
										</a>
										<div class="aa-cartbox-summary">
											<ul>
												<li><a class="aa-cartbox-img" href="#"><img
														src="${path}/resources/img/woman-small-2.jpg" alt="img"></a>
													<div class="aa-cartbox-info">
														<h4>
															<a href="#">Product Name</a>
														</h4>
														<p>1 x $250</p>
													</div> <a class="aa-remove-product" href="#"><span
														class="fa fa-times"></span></a></li>
												<li><a class="aa-cartbox-img" href="#"><img
														src="${path}/resources/img/woman-small-1.jpg" alt="img"></a>
													<div class="aa-cartbox-info">
														<h4>
															<a href="#">Product Name</a>
														</h4>
														<p>1 x $250</p>
													</div> <a class="aa-remove-product" href="#"><span
														class="fa fa-times"></span></a></li>
												<li><span class="aa-cartbox-total-title"> Total
												</span> <span class="aa-cartbox-total-price"> $500 </span></li>
											</ul>
											<a class="aa-cartbox-checkout aa-primary-btn"
												href="checkout.do">쇼핑카트[결제]</a>
										</div>
									</div>



								</c:when>
							</c:choose>
							<!-- / cart box --> --%>


							<!-- search box -->
							<div class="aa-search-box">
								<form name="Sform" method="post" 
								action="${path}/product/list.do">
									<input type="text" name="keyword"
									placeholder="ex)나이키">
									<button type="submit">
										<span class="fa fa-search"></span>
									</button>
								</form>
							</div>
							<!-- / search box -->
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / header bottom  -->
	</header>
	<!-- / header section -->
	<!-- menu -->
	<section id="menu">
		<div class="container">
			<div class="menu-area">
				<!-- Navbar -->
				<div class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
					</div>
					<div class="navbar-collapse collapse">
						<!-- Left nav -->
						<ul class="nav navbar-nav">
							<li><a href="#">BRANDS<span class="caret"></span></a>
								<ul class="dropdown-menu">
									 <li><a href="${path}/brand/nike.do">나이키</a></li>
					                 <li><a href="${path}/brand/fila.do">필라</a></li>
					                 <li><a href="${path}/brand/kolca.do">콜카</a></li>
					                 <li><a href="${path}/brand/adidas.do">아디다스</a></li>                                                
					                 <li><a href="${path}/brand/puma.do">퓨마</a></li>
					                 <li><a href="${path}/brand/descente.do">데상트</a></li>
					                 <li><a href="${path}/brand/vans.do">반스</a></li>
					                 <li><a href="${path}/brand/converse.do">컨버스</a></li>
						             <li><a href="${path}/brand/excelsior.do">엑셀시오르</a></li>
					                 <li><a href="${path}/brand/newbalance.do">뉴발란스</a></li>
								</ul></li>
							<li><a href="${path}/brand/men.do">Men</a></li>
							<li><a href="${path}/brand/women.do">Women</a></li>
							<li><a href="${path}/brand/unisex.do">Unisex</a></li>
							<%-- <li><a href="#">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="${path}/product/list.do">Shop Page</a></li>
									<li><a href="product-detail.html">Shop Single</a></li>
									<li><a href="404.html">404 Page</a></li>
								</ul></li> --%>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
	</section>
	<!-- / menu -->

	<!-- Login Modal -->
	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4>로그인</h4>
					<form id="form00" name="form00" method="post" class="aa-login-form">
						<label for="">아 이 디<span>*</span></label> <input
							name="id" id="userid" type="text" placeholder="아이디를 입력해주세요">
						<label for="">비밀번호<span>*</span></label> <input name="passwd"
							id="passwd" type="password" placeholder="비밀번호를 입력해주세요">
						<button type="button" class="aa-browse-btn" id="btnLogin">로그인</button>

						<%-- 									<c:if test="${param.message == 'error' }">
										<div class="aa-browse-btn" style="color:red;">
											아이디 또는 비밀번호가 일치하지 않습니다.
										</div>
									</c:if>
 --%>
						<label for="rememberme" class="rememberme"> <input
							type="checkbox" id="rememberme"> 정보저장
						</label>
						<p class="aa-lost-password">
							<a href="${path}/member/find.do">아이디/비밀번호 찾기</a>
						</p>
						<div class="aa-register-now">
							아직 회원이 아니신가요?<a href="${path}/member/registerEC.do">회원가입하기!
								</a>
						</div>
					</form>

				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>