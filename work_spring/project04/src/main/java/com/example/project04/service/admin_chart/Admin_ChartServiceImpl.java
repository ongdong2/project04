package com.example.project04.service.admin_chart;

import java.util.List;

import javax.inject.Inject;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;
import com.example.project04.service.admin_newproduct.Admin_NewProductService;
import com.example.project04.service.admin_pay.Admin_PayService;
import com.example.project04.service.pro_admin.Admin_ProductService;


@Service
public class Admin_ChartServiceImpl implements Admin_ChartService {
	
	@Inject
	Admin_NewProductService newproductService;
	@Inject
	Admin_ProductService productService;
	@Inject
	Admin_PayService payService;

	@Override
	public JSONObject getChartData() {
		List<Admin_ProductDTO> items=productService.ordermoney(); 
		JSONObject data=new JSONObject();
		//컬럼을 정의할 json 객체 
		JSONObject col1=new JSONObject();
		JSONObject col2=new JSONObject();
		JSONArray title=new JSONArray();
		col1.put("lable", "상품명");
		col1.put("type", "string");
		col2.put("lable", "금액"); 
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		JSONArray body=new JSONArray();
		for(Admin_ProductDTO dto: items) {
			JSONObject name=new JSONObject();
			name.put("v", dto.getPname());
			JSONObject money=new JSONObject();
			money.put("v", dto.getTotal());
			JSONArray row=new JSONArray();
			row.add(name);
			row.add(money);
			JSONObject cell=new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		
		return data;
	}

	@Override
	public JSONObject getChartData1() {
		List<Admin_PayDTO> items=payService.ordermoney1(); 
		JSONObject data=new JSONObject();
		//컬럼을 정의할 json 객체 
		JSONObject col1=new JSONObject();
		JSONObject col2=new JSONObject();
		JSONArray title=new JSONArray();
		col1.put("lable", "회원이름");
		col1.put("type", "string");
		col2.put("lable", "구매금액"); 
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		JSONArray body=new JSONArray();
		for(Admin_PayDTO dto: items) {
			JSONObject name=new JSONObject();
			name.put("v", dto.getMname());
			JSONObject money=new JSONObject();
			money.put("v", dto.getTotal());
			JSONArray row=new JSONArray();
			row.add(name);
			row.add(money);
			JSONObject cell=new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		
		return data;
	}
	@Override
	public JSONObject getChartData2() {
		List<Admin_PayDTO> items=payService.ordermoney2(); 
		JSONObject data=new JSONObject();
		//컬럼을 정의할 json 객체 
		JSONObject col1=new JSONObject();
		JSONObject col2=new JSONObject();
		JSONArray title=new JSONArray();
		col1.put("lable", "상품명");
		col1.put("type", "string");
		col2.put("lable", "판매수량"); 
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		JSONArray body=new JSONArray();
		for(Admin_PayDTO dto: items) {
			JSONObject name=new JSONObject();
			name.put("v", dto.getPname());
			JSONObject money=new JSONObject();
			money.put("v", dto.getTotal());
			JSONArray row=new JSONArray();
			row.add(name);
			row.add(money);
			JSONObject cell=new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		
		return data;
	}
	@Override
	public JSONObject getChartData3() {
		List<Admin_PayDTO> items=payService.ordermoney3(); 
		JSONObject data=new JSONObject();
		//컬럼을 정의할 json 객체 
		JSONObject col1=new JSONObject();
		JSONObject col2=new JSONObject();
		JSONArray title=new JSONArray();
		col1.put("lable", "상품명");
		col1.put("type", "string");
		col2.put("lable", "판매금액"); 
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);
		JSONArray body=new JSONArray();
		for(Admin_PayDTO dto: items) {
			JSONObject name=new JSONObject();
			name.put("v", dto.getPname());
			JSONObject money=new JSONObject();
			money.put("v", dto.getTotal());
			JSONArray row=new JSONArray();
			row.add(name);
			row.add(money);
			JSONObject cell=new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		
		return data;
	}

}
