package com.example.project04.service.admin_dealer;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;

import com.example.project04.model.admin_dealer.dao.Admin_DealerDAO;
import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Repository
public class Admin_DealerServiceImpl implements Admin_DealerService {
	
	@Inject
	Admin_DealerDAO dealerDao;

	@Override
	public List<Admin_DealerDTO> listDealer(int start,int end) {
		// TODO Auto-generated method stub
		return dealerDao.listDealer(start,end);
	}

	@Override
	public void insertDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Admin_DealerDTO viewDealer(int dseq) {
		// TODO Auto-generated method stub
		return dealerDao.viewDealer(dseq);
	}

	@Override
	public void deleteDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub
		dealerDao.deleteDealer(dto);
	}

	@Override
	public void updateDealer(Admin_DealerDTO dto) {
		// TODO Auto-generated method stub
		dealerDao.updateDealer(dto);
	}

	@Override
	public Admin_ProductDTO viewproDealer(int dseq) {
		// TODO Auto-generated method stub
		return dealerDao.viewproDealer(dseq);
	}

	@Override
	public List<Admin_ProductDTO> prolistDealer(int dseq) {
		// TODO Auto-generated method stub
		return dealerDao.prolistDealer(dseq);
	}

	@Override
	public Admin_DealerDTO detailDealer(int dseq) {
		// TODO Auto-generated method stub
		return dealerDao.detailDealer(dseq);
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return dealerDao.countArticle();
	}

	@Override
	public void addDealer(Admin_DealerDTO dto) {
		dealerDao.addDealer(dto);
		
	}

	
}
