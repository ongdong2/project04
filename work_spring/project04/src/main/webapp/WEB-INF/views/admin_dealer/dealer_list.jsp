<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<title>Admin Home Page</title>
<style>
#map {
	height: 400px;
	width: 700px;
}
</style>
</head>
<body>
	
	<script>
		var map;
		function initMap() {
			var decente = {
				lat : 37.497587,
				lng : 127.046732
			};
			var nike = {
				lat : 37.502864,
				lng : 127.037131
			};
			var adidas = {
				lat : 37.4976898,
				lng : 127.025740
			};
			var puma = {
				lat : 37.495309,
				lng : 127.924645
			};
			var vans = {
				lat : 37.554578,
				lng : 126.922260
			};
			var newbalance = {
				lat : 37.564773,
				lng : 126.985088
			};
			var excelsior = {
				lat : 37.552120,
				lng : 127.936881
			};
			var converse = {
				lat : 37.564115,
				lng : 127.984436
			};
			var fila = {
				lat : 37.536543,
				lng : 127.989402
			};
			var kolka = {
				lat : 37.487311,
				lng : 127.991039
			};
			var map = new google.maps.Map(document.getElementById('map'), {
				center : decente,
				zoom : 9
			});
			var marker = new google.maps.Marker({
				position : nike,
				map : map
			});
			var marker = new google.maps.Marker({
				position : adidas,
				map : map
			});
			var marker = new google.maps.Marker({
				position : vans,
				map : map
			});
			var marker = new google.maps.Marker({
				position : puma,
				map : map
			});
			var marker = new google.maps.Marker({
				position : newbalance,
				map : map
			});
			var marker = new google.maps.Marker({
				position : excelsior,
				map : map
			});
			var marker = new google.maps.Marker({
				position : converse,
				map : map
			});
			var marker = new google.maps.Marker({
				position : fila,
				map : map
			});
			var marker = new google.maps.Marker({
				position : kolka,
				map : map
			});
			var marker = new google.maps.Marker({
				position : fila,
				map : map
			});
		}
	</script>
	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBxJHXO5wsTm5AiCLqR2QUHUu-fbeOWjLk&callback=initMap"
		async defer></script>
</body>
</html>


<!-- <script src="http://dmaps.daum.net/map_js_init/v3.js" type="text/javascript"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=dc3079bae651e7ef68ed8ffaec29fc27&libraries=services,clusterer,drawing"></script>
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=dc3079bae651e7ef68ed8ffaec29fc27"></script>
<script type="text/javascript">
var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
mapOption = { 
    center: new daum.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
    level: 3 // 지도의 확대 레벨
};

var map = new daum.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

//마커를 표시할 위치와 title 객체 배열입니다 
var positions = [
{
    title: '카카오', 
    latlng: new daum.maps.LatLng(33.450705, 126.570677)
},
{
    title: '생태연못', 
    latlng: new daum.maps.LatLng(33.450936, 126.569477)
},
{
    title: '텃밭', 
    latlng: new daum.maps.LatLng(33.450879, 126.569940)
},
{
    title: '근린공원',
    latlng: new daum.maps.LatLng(33.451393, 126.570738)
}
];

//마커 이미지의 이미지 주소입니다
var imageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png"; 

for (var i = 0; i < positions.length; i ++) {

// 마커 이미지의 이미지 크기 입니다
var imageSize = new daum.maps.Size(24, 35); 

// 마커 이미지를 생성합니다    
var markerImage = new daum.maps.MarkerImage(imageSrc, imageSize); 

// 마커를 생성합니다
var marker = new daum.maps.Marker({
    map: map, // 마커를 표시할 지도
    position: positions[i].latlng, // 마커를 표시할 위치
    title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
    image : markerImage // 마커 이미지 
});
}

</script> -->
<script type="text/javascript">
	$(document).ready(function() {
		$("#btnAdd").click(function() {
			location.href = "${path}/admin_dealer/dealer_write.do";
		});
	});
	function list(page) {
		location.href = "${path}/project04/admin_dealer/list.do?curPage="
				+ page;
	}
</script>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>


<%-- <!--/span-->
<div class="span9" id="content">
	<div class="row-fluid">
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4>
				<h2>상품목록</h2>
			</h4>
		</div>
		<div class="navbar">
			<div class="navbar-inner">
				<ul class="breadcrumb">
					<i class="icon-chevron-left hide-sidebar"><a href='#'
						title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
					<i class="icon-chevron-right show-sidebar" style="display: none;"><a
						href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>

				</ul>
			</div>
		</div>
	</div>
	<div class="row-fluid">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">Statistics</div>
				<div class="center">
						<table>
							<tr>
								<th>상품코드</th>
								<th>상품명</th>
								<th>원가</th>
								<th>판매가</th>
							</tr>
							<c:forEach var="row" items="${list}">
								<tr>
									<td align="center">${row.pseq }</td>
									<td align="center">${row.name }</td>
									<td align="center">${row.price1 }</td>
									<td align="center">${row.price2 }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				
			
			</div>
			<div class="block-content collapse in"></div>
		</div>
	</div>
	<!-- /block -->
</div> --%>
<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">거래처목록</div>
				<div class="pull-right">
					<span class="badge badge-info"></span>

				</div>
			</div>
			<div class="block-content collapse in">
				<%-- <c:if test="${sessionScope.adminId != null}"> --%>
				<a href="${path}/admin_dealer/dealer_add.do">[거래처등록]</a>
				<br>
				<%-- </c:if> --%>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>거래처코드</th>
							<th>거래처명</th>
							<th>전화번호</th>


						</tr>
					</thead>
					<tbody>
						<c:forEach var="row" items="${map.list}">
							<tr>
								<td align="center">${row.dseq }<a
									href="${path}/admin_dealer/dealer_edit/${row.dseq}">[거래처편집]</a></td>
								<td align="center"><a
									href="${path}/admin_dealer/dealerView/${row.dseq}">
										${row.dname }</a></td>
								<td align="center">${row.phone }</td>

							</tr>
						</c:forEach>
					</tbody>
					<tr>
						<td colspan="5" align="center"><c:if
								test="${map.pager.curBlock >1 }">
								<a href="#" onclick="list('1')">[처음]</a>
							</c:if> <c:if test="${map.pager.curBlock >1 }">
								<a href="#" onclick="list('${map.pager.prevPage }')">[이전]</a>
							</c:if> <c:forEach var="num" begin="${map.pager.blockBegin }"
								end="${map.pager.blockEnd }">

								<c:choose>
									<c:when test="${num == map.pager.curPage }">
										<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
										<span style="color: red;">${num }</span>
									</c:when>
									<c:otherwise>
										<a href="#" onclick="list('${num}')">${num }</a>

									</c:otherwise>
								</c:choose>

							</c:forEach> <c:if test="${map.pager.curBlock<map.pager.totBlock }">
								<a href="#" onclick="list('${map.pager.nextPage}')">[다음]</a>
							</c:if> <c:if test="${map.pager.curPage<map.pager.totPage }">
								<a href="#" onclick="list('${map.pager.totPage}')">[끝]</a>
							</c:if></td>
					</tr>

				</table>
				<div id="map"></div>
			</div>
		</div>
		<!-- /block -->
	</div>

	<hr>
	<footer> </footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>