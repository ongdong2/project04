package com.example.project04.service.member;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.project04.model.member.dto.MemberDTO;




public interface MemberService {

	
	public boolean loginCheck(MemberDTO dto, HttpSession session);
	
	public MemberDTO viewMember(String id);
	
	public void logout(HttpSession session);
	
	public String idcheck(String id);

	public void join(MemberDTO dto);
	
	public void memberupdate1(MemberDTO dto, HttpSession session);

	public void memberupdate2(MemberDTO dto, HttpSession session);

	public void memberupdate3(MemberDTO dto, HttpSession session);

	public String memberfindid(MemberDTO dto);

	public String memberfindpwd(MemberDTO dto);
	
	public boolean deletecheck(String id, String passwd);

	public void deleteEnd(String id);
	
	public void temporaryPW(MemberDTO dto);
	
	public List<MemberDTO> memberinfo(String id);

	public String findname(String id);
}
