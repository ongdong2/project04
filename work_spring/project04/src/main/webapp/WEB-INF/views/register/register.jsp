<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
	<%@ include file="../include/menu.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop</title>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
	$(function() {
		//아이디중복확인
		$("#id").keyup(function() {
			var id = $("#id").val();
			if (id.length < 4) {
				$("#div1").css("visibility", "hidden");
			} else {
				$("#div1").css("visibility", "visible");
				$.ajax({
					type : "post",
					url : "${path}/member/idcheck.do",
					data : "id=" + id,
					success : function(result) {
						console.log(result);
						$("#div1").html(result);
					}
				});
			}
		});
	});
</script>

<script>
	$(function() {
		$("#sign").click(function() {
			
			var num=$("#emailcheck").val();
			
			var id = $("#id").val();
			var name = $("#name").val();
			var email = $("#email").val();
			var phone1 = $("#phone1").val();
			var passwd1 = $("#passwd1").val();
			var zipcode1 = $("#zipcode1").val();
			var passwdcheck = $("#passwdcheck").val();

			var address1 = $("#address1").val();
			var exp1 = /^[A-Za-z0-9]{4,10}$/;
			var exp2 = /^[A-Za-z0-9]{8,15}$/;
			var exp3 = /^[a-z0-9]{2,}@[a-z0-9]{2,}\.[a-z]{2,}$/;
			var exp4 = /^[0-9]+$/;
			var exp5 = /^[0-9]{3,}\-[0-9]{4,}\-[0-9]{4,}$/;
			if (!exp3.test(email)) {
				alert("이메일 형식이 잘못되었습니다.");
				$("#email").focus();
				return;
			}
			if (id == "") {
				alert("아이디는 필수 입력입니다.");
				$("#id").focus();
				return;
			}
			if (!exp1.test(id)) {
				alert("아이디는 영문자,숫자 4~10자리로 입력하세요.");
				$("#id").focus();
				return;
			}
			if (passwd1 == "") {
				alert("비밀번호를 입력해주세요");
				$("#passwd1").focus();
				passwd1 == "";
				passwdcheck == "";
				return;
			}
			if (!exp2.test(passwd1)) {
				alert("비밀번호는 소문자,숫자 8자리 이상 입력하세요.");
				$("#passwd1").focus();
				return;
			}
			if (passwd1 != passwdcheck) {
				alert("입력하신 비밀번호가 서로 맞지 않습니다.");
				$("#passwd1").focus();
				return;
			}
			if (name == "") {
				alert("이름은 필수 입력입니다.");
				$("#name").focus();
				return;
			}

			if (phone1 == "") {
				alert("전화번호를 입력 해 주세요.)");
				$("#phone1").focus();
				return;
			}
			if (!exp5.test(phone1)) {
				alert("전화번호는 양식에 맞춰 입력하세요.(ex)010-1234-5678))");
				$("#phone1").focus();
				return;
			}
			if (zipcode1 == "") {
				alert("우편번호와 주소를 검색해주세요.");
				$("#zipcode").focus();
				return;
			}
			if (address1 == "") {
				alert("상세주소를 입력해 주세요.");
				$("#address1").focus();
				return;
			}

			document.form2.action = "${path}/member/join.do";
			document.form2.submit();
		});
	});
</script>

<script>
	function daumZipCode() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('zipcode1').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('zip_num1').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('address1').focus();
					}
				}).open();
	}
</script>


<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body>

	<div class="container">
		<form method="post" action="register.do" id="form2" name="form2">
<table class="table table-bordered table-hover" style="width: 650px" align="center">
	<thead>
		<tr>
			<th style="width: 550px" colspan="3">
				<h4>회원 등록 양식</h4>
		</tr>
	</thead>
<tbody>
	<tr>
		<td style="width: 150px;"><h5>이메일</h5></td>
		<td colspan="2"><input class="form-inline; form-control"
			type="email" value="${receiveMail}" style="width: 350px"
			id="email" name="email" maxLength="20" readonly>
	</tr>
	<tr>
		<td style="width: 150px;"><h5>아이디</h5></td>
		<td>
			<input class="form-control" type="text" placeholder="영문, 숫자 4~10자리"
				style="width: 350px" id="id" name="id" maxLength="20">
		</td>
		<td style="width: 100px; text-align: right">
			<div id="div1" name="div1" style="font-size: 12px; color: red; width: 110px"></div>
		</td>
	</tr>
	<tr>
		<td style="width: 150px;"><h5>비밀번호</h5></td>
		<td colspan="2">
			<input class="form-control" type="password" placeholder="영문, 숫자 8자리 이상"
				style="width: 350px" id="passwd1" name="passwd" maxLength="20">
		</td>
	</tr>
	<tr>
		<td style="width: 150px;"><h5>비밀번호확인</h5></td>
		<td colspan="2">
			<input class="form-control" type="password" placeholder="영문, 숫자 8자리 이상"
				style="width: 350px" id="passwdcheck" name="passwdcheck" maxLength="20">
			<div id="div2" name="div2"
				style="font-size: 12px; color: red; width: 110px">
			</div>
		</td>
	</tr>
	<tr>
		<td style="width: 150px;"><h5>이름</h5></td>
		<td colspan="2">
			<input class="form-control" type="text" placeholder="ex) 홍길동 "
				style="width: 350px" id="mname" name="mname" maxLength="20">
		</td>
	</tr>
	
	<!-- <tr>
		<td style="width: 150px;"><h5>성별</h5></td>
		<td colspan="2">
			<div class="form-group">
				<div class="btn-group" data-toggle="buttons" style="width:350px" >
				</div>
					<label class="btn btn-primary active">
						<input type="radio" name="gender" autocomplete="off" value="남자" >남자
					</label>
					<label class="btn btn-primary">
						<input type="radio" name="gender" autocomplete="off" value="여자"> 여자
					</label>
				</div>
		</td>
		</tr> -->
	
	<tr>
		<td style="width: 150px;"><h5>전화번호</h5></td>
		<td colspan="2">
			<input class="form-control" type="text" placeholder="010-1234-5678"
			style="width: 350px" id="phone1" name="phone1" maxLength="20">
		</td>
	</tr>
	<tr>
		<td style="width: 75px;"><h5>우편번호</h5></td>
		<td colspan="2">
			<input class="form-inline; form-control" type="text" style="width: 350px"
				id="zipcode1" name="zipcode1" readonly maxLength="60">
			<input class="btn btn-primary  pull-right" type="button"
				onclick="daumZipCode()" value="우편번호찾기">
		</td>
	</tr>
	<tr>
		<td style="width: 75px;"><h5>주소</h5></td>
		<td colspan="2">
			<input class="form-control" type="text" style="width: 350px" 
				id="zip_num1" name="zip_num1" readonly maxLength="60">
		</td>
	</tr>
	<tr>
		<td style="width: 150px;"><h5>상세주소</h5></td>
		<td colspan="2">
			<input class="form-control" type="text" style="width: 350px"
				id="address1" name="address1" maxLength="60">
		</td>
	</tr>
	<tr>
		<td style="text-align: left" colspan="3">
			<input class="btn btn-primary pull-right" type="button" id="sign" value="회원가입">
		</td>
	</tr>
</tbody>
</table>
</form>
</div>
	<%@ include file="../include/Footer.jsp"%>

</body>
</html>