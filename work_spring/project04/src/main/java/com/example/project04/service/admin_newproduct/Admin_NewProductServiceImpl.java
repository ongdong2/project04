package com.example.project04.service.admin_newproduct;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.project04.model.admin_newproduct.dao.Admin_NewProductDAO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Service
public class Admin_NewProductServiceImpl implements Admin_NewProductService {
	
	@Inject
	Admin_NewProductDAO newproductDao;

	@Override
	public List<Admin_NewProductDTO> listNew(int start,int end) {
		
		return newproductDao.listNew(start,end);
	}

	@Override
	public Admin_NewProductDTO detailProduct(int nseq) {
		// TODO Auto-generated method stub
		return newproductDao.detailProduct(nseq);
	}
	@Override
	public Admin_NewProductDTO orderProduct(int nseq) {
		// TODO Auto-generated method stub
		return newproductDao.orderProduct(nseq);
	}
	@Transactional
	@Override
	public void insertNew(Admin_NewProductDTO dto) {
		newproductDao.insertNew(dto);
		newproductDao.insertNewSize(dto);
		newproductDao.insertStock(dto);
		newproductDao.update(dto);
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return newproductDao.countArticle();
	}

	@Override
	public List<Admin_NewProductDTO> ordermoney() {
		// TODO Auto-generated method stub
		return newproductDao.ordermoney();
	}

	/*@Override
	public void deleteOrder(Admin_ProductDTO dto) {
		productDao.deleteOrder(dto);
		
	}*/

}
