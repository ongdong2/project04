<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<%@ include file="include/header.jsp"%>
<%@ include file="include/menu.jsp"%>
<head>
</head>
<body>

	<%-- <c:if test="${message == 'ok'}">
		<script>
			alert("로그인성공")
		</script>
	</c:if> --%>
	<c:if test="${message == 'error'}">
		<script>
			alert("아이디 또는 비밀번호가 틀렸습니다.")
		</script>

	</c:if>
	<!-- Start slider -->
	<section id="aa-slider">
		<div class="aa-slider-area">
			<div id="sequence" class="seq">
				<div class="seq-screen">
					<ul class="seq-canvas">


						<!-- single slide item -->
						<li>
							<div class="seq-model">
								<img data-seq src="${path}/resources/img/banner/slider3.jpg"
									alt="Women Jeans slide img" />
							</div>
							<div class="seq-title">
								<a data-seq href="${path}/product/detail.do?pseq=13"
									class="aa-shop-now-btn aa-secondary-btn"
									style="margin-top: 350px;"> 구매하기</a>
							</div>

						</li>

						<!-- single slide item -->
						<li>
							<div class="seq-model">
								<img data-seq src="${path}/resources/img/banner/slider2.jpg"
									alt="Women Jeans slide img" />
							</div>
							<div class="seq-title">
								<a data-seq href="${path}/brand/converse.do"
									class="aa-shop-now-btn aa-secondary-btn"
									style="margin-top: 350px;"> 쇼핑하기</a>
							</div>

						</li>

						<!-- single slide item -->

						<li>
							<div class="seq-model">
								<img data-seq src="${path}/resources/img/slider/5.jpg"
									alt="Male Female slide img" />
							</div>
							<div class="seq-title">
								<h2 data-seq>커플상품!!</h2>
								<p data-seq>따뜻한 봄! 사랑하는 연인과 커플아이템</p>

								<%-- <p data-seq><%=application.getRealPath("/WEB-INF/views/images/") %></p> --%>

								<a data-seq href="${path}/brand/unisex.do"
									class="aa-shop-now-btn aa-secondary-btn style="
									style="margin-top: 225px;"> 쇼핑하기</a>
							</div>
						</li>
					</ul>
				</div>
				<!-- slider navigation btn -->
				<fieldset class="seq-nav" aria-controls="sequence"
					aria-label="Slider buttons">
					<a type="button" class="seq-prev" aria-label="Previous"><span
						class="fa fa-angle-left"></span></a> <a type="button" class="seq-next"
						aria-label="Next"><span class="fa fa-angle-right"></span></a>
				</fieldset>
			</div>
		</div>
	</section>
	</div>
	<!-- quick view modal -->
	<div class="modal fade" id="quick-view-modal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<div class="row">
						<!-- Modal view slider -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="aa-product-view-slider">
								<div class="simpleLens-gallery-container" id="demo-1">
									<div class="simpleLens-container">
										<div class="simpleLens-big-image-container">
											<a class="simpleLens-lens-image"
												data-lens-image="${path}/resources/img/view-slider/large/polo-shirt-1.png">
												<img
												src="${path}/resources/img/view-slider/medium/polo-shirt-1.png"
												class="simpleLens-big-image">
											</a>
										</div>
									</div>
									<div class="simpleLens-thumbnails-container">
										<a href="#" class="simpleLens-thumbnail-wrapper"
											data-lens-image="${path}/resources/img/view-slider/large/polo-shirt-1.png"
											data-big-image="${path}/resources/img/view-slider/medium/polo-shirt-1.png">
											<img
											src="${path}/resources/img/view-slider/thumbnail/polo-shirt-1.png">
										</a> <a href="#" class="simpleLens-thumbnail-wrapper"
											data-lens-image="${path}/resources/img/view-slider/large/polo-shirt-3.png"
											data-big-image="${path}/resources/img/view-slider/medium/polo-shirt-3.png">
											<img
											src="${path}/resources/img/view-slider/thumbnail/polo-shirt-3.png">
										</a> <a href="#" class="simpleLens-thumbnail-wrapper"
											data-lens-image="${path}/resources/img/view-slider/large/polo-shirt-4.png"
											data-big-image="${path}/resources/img/view-slider/medium/polo-shirt-4.png">
											<img
											src="${path}/resources/img/view-slider/thumbnail/polo-shirt-4.png">
										</a>
									</div>
								</div>
							</div>
						</div>
						<!-- Modal view content -->
						<div class="col-md-6 col-sm-6 col-xs-12">
							<div class="aa-product-view-content">
								<h3>T-Shirt</h3>
								<div class="aa-price-block">
									<span class="aa-product-view-price">$34.99</span>
									<p class="aa-product-avilability">
										Avilability: <span>In stock</span>
									</p>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									Officiis animi, veritatis quae repudiandae quod nulla porro
									quidem, itaque quis quaerat!</p>
								<h4>Size</h4>
								<div class="aa-prod-view-size">
									<a href="#">S</a> <a href="#">M</a> <a href="#">L</a> <a
										href="#">XL</a>
								</div>
								<div class="aa-prod-quantity">
									<form action="">
										<select name="" id="">
											<option value="0" selected="1">1</option>
											<option value="1">2</option>
											<option value="2">3</option>
											<option value="3">4</option>
											<option value="4">5</option>
											<option value="5">6</option>
										</select>
									</form>
									<p class="aa-prod-category">
										Category: <a href="#">Polo T-Shirt</a>
									</p>
								</div>
								<div class="aa-prod-view-bottom">
									<a href="#" class="aa-add-to-cart-btn"><span
										class="fa fa-shopping-cart"></span>Add To Cart</a> <a href="#"
										class="aa-add-to-cart-btn">View Details</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- / quick view modal -->
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</section>
	<!-- / Products section -->
	<!-- banner section -->
	<section id="aa-banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="aa-banner-area">
							<a href="#"><img src="${path}/resources/img/banner/Kku_banner.jpg"
								alt="fashion banner img"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>






	<!-- popular section -->
	<section id="aa-popular-category">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="aa-popular-category-area">

							<!-- start prduct navigation -->
							<ul class="nav nav-tabs aa-products-tab">
								<li class="active"><a href="#unisex" data-toggle="tab">남여공용</a></li>
								<li><a href="#men" data-toggle="tab">남성용</a></li>
								<li><a href="#woman" data-toggle="tab">여성용</a></li>
							</ul>

							<!-- Tab panes -->
							<div class="tab-content">
								<!-- Start men popular category -->

								<!-- Start men popular category -->
								<div class="tab-pane fade in active" id="unisex">
									<ul class="aa-product-catg aa-popular-slider">


										<!-- --start 상품-- -->
										<!-- ------------------------------------------------------------------------------ -->
										<!-- -------------------------------남여공룡----------------------------------------- -->
										<!-- -------------------------------250x300---사진 사이즈----------------------------- -->
										<!-- ------------------------------------------------------------------------------ -->
										<c:forEach var="dto" items="${plist}">
											<!-- start single product item -->
											<li>
												<figure>
													<a class="aa-product-img"
														href="${path}/product/detail.do?pseq=${dto.pseq}"> <img
														src="${path}/resources/img/shoes/${dto.image_url}"
														style="width: 250px; height: 300px;" alt="polo shirt img">
													</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href="#">${dto.pname}</a>
														</h4>
														<span class="aa-product-price">￦<fmt:formatNumber
																value="${dto.price2}" /></span>
													</figcaption>
												</figure>
										</c:forEach>

									</ul>
									<div align="center">
										<a class="aa-browse-btn" href="${path}/brand/unisex.do">남여공용
											상품페이지 <span class="fa fa-long-arrow-right"></span>
										</a>
									</div>
								</div>
								<!-- / popular product category -->


								<!-----------------------------------------------------------------------------------------------------------------  -->



								<!-- start 틀 -->
								<div class="tab-pane fade" id="men">
									<ul class="aa-product-catg aa-featured-slider">
										<!-- start 상품 -->
										<!-- ------------------------------------------------------------------------------ -->
										<!-- -------------------------------남 성 용 상 품 ------------------------------------- -->
										<!-- ------------------------------------------------------------------------------ -->
										<c:forEach var="dto" items="${mlist}">
											<li>
												<figure>
													<a class="aa-product-img"
														href="${path}/product/detail.do?pseq=${dto.pseq}"> <img
														src="${path}/resources/img/shoes/${dto.image_url}"
														style="width: 250px; height: 300px;" alt="polo shirt img">
													</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href="#">${dto.pname}</a>
														</h4>
														<span class="aa-product-price">￦<fmt:formatNumber
																value="${dto.price2}" /></span>
													</figcaption>
												</figure>
										</c:forEach>
										<!-- (/ul) 대리고 가야지 버튼나옴 -->
									</ul>
									<div align="center">
										<a class="aa-browse-btn" href="${path}/brand/men.do">남성용
											상품페이지<span class="fa fa-long-arrow-right"></span>
										</a>
									</div>
								</div>





								<!-- ------------------------------여성용 상품---------------------------------- -->
								<div class="tab-pane fade" id="woman">
									<ul class="aa-product-catg aa-latest-slider">
										<!-- start single product item -->
										<c:forEach var="dto" items="${wlist}">
											<li>
												<figure>
													<a class="aa-product-img"
														href="${path}/product/detail.do?pseq=${dto.pseq}"> <img
														src="${path}/resources/img/shoes/${dto.image_url}"
														style="width: 250px; height: 300px;" alt="polo shirt img">
													</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href="#">${dto.pname}</a>
														</h4>
														<span class="aa-product-price">￦<fmt:formatNumber
																value="${dto.price2}" /></span>
													</figcaption>
												</figure>
										</c:forEach>
										<!-- (/ul) 대리고 가야지 버튼나옴 -->
									</ul>
									<div align="center">
										<a class="aa-browse-btn" href="${path}/brand/women.do">여성용
											상품페이지<span class="fa fa-long-arrow-right"></span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- / popular section -->


	<!-- Support section -->
	<section id="aa-support">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="aa-support-area">
						<!-- single support -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="aa-support-single">
								<span class="fa fa-truck"></span>
								<h4>무료 배송</h4>
								<P>
								</P>
							</div>
						</div>


						<!-- single support -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="aa-support-single">
								<span class="fa fa-clock-o"></span>
								<h4>30일 환불 서비스</h4>
								<P>
								</P>
							</div>
						</div>
						<!-- single support -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<div class="aa-support-single">
								<span class="fa fa-phone"></span>
								<h4>24시간 상담</h4>
								<P>
								</P>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- / Support section -->


	<section id="aa-popular-category">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="aa-popular-category-area">
							<!-- start prduct navigation -->
							<ul class="nav nav-tabs aa-products-tab">
								<li class="active"><a href="#popular" data-toggle="tab">베스트상품</a></li>
								<li><a href="#newlist" data-toggle="tab">최신상품</a></li>
							</ul>


							<!-- -----------------------------------베스트 상품----------------------------------- -->
							<!-- Tab panes -->
							<div class="tab-content">
								<!-- Start men popular category -->
								<!-- Start men popular category -->
								<div class="tab-pane fade in active" id="popular">
									<ul class="aa-product-catg aa-popular-slider">
										<c:forEach var="dto" items="${list}">
											<!-- start single product item -->
											<li>
												<figure>
													<a class="aa-product-img"
														href="${path}/product/detail.do?pseq=${dto.pseq}"> <!-- ------------------------------------------------------------------------------ -->
														<!-- -------------------------------250x300---사진 사이즈----------------------------- -->
														<!-- ------------------------------------------------------------------------------ -->
														<img src="${path}/resources/img/shoes/${dto.image_url}"
														style="width: 250px; height: 300px;" alt="polo shirt img">
													</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href="#">${dto.pname}</a>
														</h4>
														<span class="aa-product-price">￦<fmt:formatNumber
																value="${dto.price2}" /></span>
													</figcaption>
												</figure>
										</c:forEach>
										
									</ul>
									<a class="aa-browse-btn" href="#">베스트 상품페이지 <span
										class="fa fa-long-arrow-right"></span></a><br>
								</div>
								<!-- 신상품 -->
								<div class="tab-pane fade" id="newlist">
									<ul class="aa-product-catg aa-latest-slider">
										<c:forEach var="dto" items="${newlist}">
											<!-- start single product item -->
											<li>
												<figure>
													<a class="aa-product-img"
														href="${path}/product/detail.do?pseq=${dto.pseq}"> <!-- ------------------------------------------------------------------------------ -->
														<!-- -------------------------------250x300---사진 사이즈----------------------------- -->
														<!-- ------------------------------------------------------------------------------ -->
														<img src="${path}/resources/img/shoes/${dto.image_url}"
														style="width: 250px; height: 300px;" alt="polo shirt img">
													</a>
													<figcaption>
														<h4 class="aa-product-title">
															<a href="#">${dto.pname}</a>
														</h4>
														<span class="aa-product-price">￦<fmt:formatNumber
																value="${dto.price2}" /></span>
													</figcaption>
												</figure>
										</c:forEach>
									</ul>
									<div align="center">
									<a class="aa-browse-btn" href="#">신상품 상품페이지 <span
										class="fa fa-long-arrow-right"></span></a><br>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>




	<%@ include file="include/Footer.jsp"%>
</body>
</html>
+
