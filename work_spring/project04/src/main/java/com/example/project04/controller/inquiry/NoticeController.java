package com.example.project04.controller.inquiry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.notice.dto.NoticeDTO;
import com.example.project04.service.notice.NoticeService;
import com.example.project04.service.notice.Pager;

@Controller
@RequestMapping("notice/*")
public class NoticeController {
	@Inject
	NoticeService noticeService;
	

	
	@RequestMapping("list.do")
	public ModelAndView list(@RequestParam(defaultValue="1")int curPage) throws Exception {
		int count=noticeService.countPage();
		Pager pager=new Pager(count, curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		List<NoticeDTO> list=noticeService.list(start, end);
		ModelAndView mav=new ModelAndView();
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("pager", pager);
		map.put("count", count);
		map.put("list", list);
		mav.setViewName("notice/list");
		mav.addObject("map",map);
		return mav;
		
	}
	@RequestMapping("update.do")
	public String update(NoticeDTO dto,@RequestParam(defaultValue="1") int curPage,Model model) throws Exception {
		int count=0;
		Pager pager=new Pager(count, curPage);
		model.addAttribute("curPage",curPage);
		noticeService.update(dto);
		Map<String,Object> map=new HashMap<>();
		map.put("curPage", curPage);
		return "redirect:/notice/list.do";
	}
	@RequestMapping("view.do")
	public ModelAndView view(int nseq,HttpSession session,int curPage,String adminid) throws Exception {
		noticeService.viewCnt(nseq, session);
		int count=0;
		Pager pager = new Pager(count, curPage);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("notice/view1");
		Map<String,Object> map=new HashMap<>();
		map.put("pager", pager);
		map.put("adminid", adminid);
		map.put("dto", noticeService.view(nseq));
		mav.addObject("map",map);
		
		return mav;
	}
	@RequestMapping("delete.do")
	public String delete(int nseq) throws Exception{
		noticeService.delete(nseq);
		return "redirect:/notice/list.do";
	}
	@RequestMapping("write.do")
	public String write() {
		return "notice/write";
	}
	@RequestMapping("insert.do")
	public String insert(@ModelAttribute NoticeDTO dto,HttpSession session) {
		String adminid= (String) session.getAttribute("id");
		dto.setAdminid(adminid);
		
		noticeService.create(dto);
		
		return "redirect:/notice/list.do";
	}
	

}
