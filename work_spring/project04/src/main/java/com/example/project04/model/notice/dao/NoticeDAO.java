package com.example.project04.model.notice.dao;

import java.util.List;

import com.example.project04.model.notice.dto.NoticeDTO;

public interface NoticeDAO {
	List<NoticeDTO> list(int start,int end) throws Exception;
	public int countPage();//페이지나누기를위한 메소드
	public void viewCnt(int nseq) throws Exception;//조회수 처리를위한 메소드
	public void create(NoticeDTO dto);//문의글 추가 메소드
	public void delete(int nseq);//삭제메소드
	public void update(NoticeDTO dto) throws Exception;//문의글 수정 메소드
	public NoticeDTO view(int nseq); //뷰를 띄우는 메소드
	
}
