<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>
	<%@ include file="../include/header.jsp" %>  
	<%@ include file="../include/menu.jsp" %>
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="${path}/resources/img/banner/men_banner.jpg" alt="fashion img" height="300" style="margin-left: auto;  margin-right: auto; display: block;">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-category">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-8 col-md-push-3">
          <div class="aa-product-catg-content">
            
             <c:if test="${dseq == 1}">
				<img src="${path}/resources/img/banner/nike_banner.gif" width="845px" >
			 </c:if>
             <c:if test="${dseq == 2}">
				<img src="${path}/resources/img/banner/fila_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 3}">
				<img src="${path}/resources/img/banner/kolca_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 4}">
				<img src="${path}/resources/img/banner/adidas_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 5}">
				<img src="${path}/resources/img/banner/puma_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 6}">
				<img src="${path}/resources/img/banner/descente_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 7}">
				<img src="${path}/resources/img/banner/vans_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 8}">
				<img src="${path}/resources/img/banner/converse_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 9}">
				<img src="${path}/resources/img/banner/excelsior_banner.gif" width="845px" >
			 </c:if>
			 <c:if test="${dseq == 10}">
				<img src="${path}/resources/img/banner/newbalance_banner.gif" width="845px" >
			 </c:if>

			 
            <div class="aa-product-catg-head">
              
              
				
				
              <div class="aa-product-catg-head-right">
                <a id="grid-catg" href="#"><span class="fa fa-th"></span></a>
                <a id="list-catg" href="#"><span class="fa fa-list"></span></a>
              </div>
            </div>
            <div class="aa-product-catg-body">
              <ul class="aa-product-catg">
              
                <!-- start single product item -->
                <c:forEach var="catg" items="${catg}">
                <c:if test=""></c:if>
                <li>
                  <figure>
                    <a class="aa-product-img" href="${path}/product/detail.do?pseq=${catg.pseq}"><img src="${path}/resources/img/shoes/${catg.image_url}" width="200px" height="300px" alt="이언경"></a>
                    <figcaption>
                      <h4 class="aa-product-title"><a href="${catg.pname}"></a></h4>
                      <span class="aa-product-price">￦<fmt:formatNumber value="${catg.price2}"/></span>
                      <p class="aa-product-descrip">${catg.content}</p>
                    </figcaption>
                  </figure>
				                        
                  <!-- product badge -->
                </li>
                </c:forEach>

              </ul>

              
            </div>
            <div class="aa-product-catg-pagination">
              <nav>
                <ul class="pagination">
                  <li>
                    <a href="#" aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                    </a>
                  </li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li>
                    <a href="#" aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-md-pull-9">
          <aside class="aa-sidebar">
            <!-- single sidebar -->
            <div class="aa-sidebar-widget">
              <h3>Category</h3>
              <ul class="aa-catg-nav">
              <c:forEach var="dealer" items="${dealer}">
               	<li><a href="${path}/brand/catg1.do?dseq=${dealer.dseq}">${dealer.dname}</a></li>
              </c:forEach>
              </ul>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->







	<%@ include file="../include/Footer.jsp" %>  
	
  </body>
</html>