package com.example.project04.service.admin_chart;

import org.json.simple.JSONObject;

public interface Admin_ChartService {
	public JSONObject getChartData();
	public JSONObject getChartData1();
	public JSONObject getChartData2();
	public JSONObject getChartData3();
}
