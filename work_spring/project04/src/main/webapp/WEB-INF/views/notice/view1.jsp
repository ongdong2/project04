<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script>
$(function(){
	$("#btnList").click(function(){
		location.href="${path}/notice/list.do?curPage="+${map.pager.curPage};
	
		
	});
	$("#btnUpdate").click(function(){
		alert("수정하시겠습니까?");
		document.form3.action="${path}/notice/update.do";
		document.form3.submit();
	});
	$("#btnDelete").click(function(){
		alert("삭제합니다");
		document.form3.action="${path}/notice/delete.do?nseq="+nseq+"&curPage="+curPage;
		document.form3.submit();
	});
});
</script>
<body>

<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table" align="center">
   <h2>공지사항</h2>
	<form id="form3" name="form3" method="post">
	<div><h4><strong>작성자&nbsp;&nbsp;:&nbsp;&nbsp;${map.dto.adminid}</strong></h4></div><br><br>
	<div><Strong>조회수 : <span style=color:red>${map.dto.viewcnt}</span> </Strong></div>
	<div align="left" style="margin-left:212px;"><strong>공지제목</strong></div>
	<input name="subject" id="subject" size="80" value="${map.dto.subject}" <c:if test="${sessionScope.id != map.adminid }">readonly</c:if>>
	<br><br>
	<div align="left" style="margin-left:212px;"><strong>공지사항</strong></div>
	<textarea id="content" name="content" rows="15" cols="80" <c:if test="${sessionScope.id != map.adminid}">readonly</c:if>>
	${map.dto.content}
	</textarea>
	<input type="hidden" name="nseq" value="${map.dto.nseq}"><br>
	<c:if test="${sessionScope.id == map.adminid}">
	<button type="button" id="btnUpdate" class="btn btn-primary" type="button">수정</button>
	<button type="button" id="btnDelete" class="btn btn-primary" type="button">삭제</button><br><br>
	</c:if>
	<button type="button" id="btnList" class="btn btn-primary" type="button">공지목록</button>
	</form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
</body>
<%@ include file="../include/Footer.jsp"%>
</html>