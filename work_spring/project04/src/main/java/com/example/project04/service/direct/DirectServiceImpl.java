package com.example.project04.service.direct;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.direct.dao.DirectDAO;
import com.example.project04.model.direct.dto.DirectDTO;

@Service
public class DirectServiceImpl implements DirectService {

	@Inject
	DirectDAO directDao;
	
	@Override
	public List<DirectDTO> directMoney() {
		return directDao.directMoney();
	}

	@Override
	public void insert(DirectDTO dto) {
		directDao.insert(dto);
	}

	@Override
	public List<DirectDTO> listDirect(String id) {
		return directDao.listDirect(id);
	}

	@Override
	public void show(String id) {
		directDao.show(id);
	}
	
	@Override
	public int sumMoney(String id) {
		return directDao.sumMoney(id);
	}

	@Override
	public void delete(String id) {
		directDao.delete(id);
	}

}
