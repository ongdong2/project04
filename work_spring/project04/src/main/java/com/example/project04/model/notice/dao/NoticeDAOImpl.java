package com.example.project04.model.notice.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.notice.dto.NoticeDTO;
@Repository
public class NoticeDAOImpl implements NoticeDAO {
@Inject
SqlSession sqlSession;
	@Override
	public List<NoticeDTO> list(int start,int end) throws Exception {
		Map<String,Object> map=new HashMap<String, Object>();
		map.put("start", start);
		map.put("end", end);
		return sqlSession.selectList("Notice.noticelist",map);
	
	}

	@Override
	public int countPage() {
		return sqlSession.selectOne("Notice.countPage");
	}

	@Override
	public void create(NoticeDTO dto) {
			sqlSession.insert("Notice.insert",dto);
		
	}

	@Override//삭제하기
	public void delete(int nseq) {
		sqlSession.delete("Notice.delete",nseq);
	}
	
	@Override//수정하기
	public void update(NoticeDTO dto) throws Exception{
		sqlSession.update("Notice.update",dto);
		
	}

	@Override
	public NoticeDTO view(int nseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Notice.view",nseq);
	}

	@Override
	public void viewCnt(int nseq) throws Exception {
		sqlSession.update("Notice.viewcnt",nseq);
		
	}

}
