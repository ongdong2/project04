package com.example.project04.controller.admin_newproduct;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;
import com.example.project04.service.admin.Pager;
import com.example.project04.service.admin_newproduct.Admin_NewProductService;

@Controller
@RequestMapping("admin_newproduct/*")
public class Admin_NewProductController {

	@Inject
	Admin_NewProductService newproductService;
	
	@RequestMapping("list.do")
	public ModelAndView newProductlist(@RequestParam(defaultValue="1")int curPage) throws Exception{
		int count=newproductService.countArticle();
		
		Pager pager=new Pager(count,curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		List<Admin_NewProductDTO> list=newproductService.listNew(start,end);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", list.size());
		map.put("pager", pager);
		mav.setViewName("admin_newproduct/newproduct_list");
		mav.addObject("map", map);
		return mav;
	}
	@RequestMapping("newproduct_detail/{nseq}")
	public ModelAndView view(@PathVariable("nseq") int nseq,ModelAndView mav) {
		mav.setViewName("admin_newproduct/newproduct_detail");
		mav.addObject("dto", newproductService.detailProduct(nseq));
		return mav;
	}
	@RequestMapping("insertNew.do")
	public String insert(Admin_NewProductDTO dto) {
		//int num = dto.getSz230mm();
		System.out.println(dto);
		newproductService.insertNew(dto);
		
		return "redirect:/admin/order_list.do";
	}
	@RequestMapping("newproduct_order/{nseq}")
	public ModelAndView order(@PathVariable("nseq") int nseq,ModelAndView mav) {
		mav.setViewName("admin_newproduct/newproduct_order");
		mav.addObject("dto", newproductService.orderProduct(nseq));
		return mav;
	}
	
	
}
