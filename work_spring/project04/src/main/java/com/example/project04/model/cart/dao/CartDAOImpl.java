package com.example.project04.model.cart.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.cart.dto.CartDTO;

@Repository
public class CartDAOImpl implements CartDAO {

	@Inject
	SqlSession sqlSession;
	
	@Override
	public List<CartDTO> cartMoney() {
		return sqlSession.selectList("cart.cart_money");
	}

	@Override
	public void insert(CartDTO dto) {
		sqlSession.insert("cart.insert", dto);
	}

	@Override
	public List<CartDTO> listCart(String id) {
		return sqlSession.selectList("cart.cart_list", id);
	}

	@Override
	public void show(String id) {
		sqlSession.delete("cart.show", id);
	}
	
	@Override
	public void delete(int cseq) {
		sqlSession.delete("cart.delete", cseq);
	}
	
	@Override
	public void deleteSel(int cseq) {
		sqlSession.delete("cart.delete", cseq);
	}



	@Override
	public void deleteAll(String id) {
		sqlSession.delete("cart.deleteAll", id);
	}

	@Override
	public void update(int cseq) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int sumMoney(String id) {
		return sqlSession.selectOne("cart.sumMoney", id);
	}

	@Override
	public int countCart(String id, int pseq) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void updateCart(CartDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyCart(CartDTO dto) {
		// TODO Auto-generated method stub
		
	}

}
