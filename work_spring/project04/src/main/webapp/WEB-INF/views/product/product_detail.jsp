<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<script>
   $(function() {
      $("#btnCart").click(function() {
         var sz230mm=$("#sz230mm").val();
         var sz240mm=$("#sz240mm").val();
         var sz250mm=$("#sz250mm").val();
         var sz260mm=$("#sz260mm").val();
         var sz270mm=$("#sz270mm").val();
         var sz280mm=$("#sz280mm").val();
         if (sz230mm == 0 && sz240mm == 0 && sz250mm == 0 && sz260mm == 0 && sz270mm == 0 && sz280mm == 0) {
            alert("수량을 선택하세요.");
         }else {
            document.form1.action = "${path}/cart/insert.do";
            document.form1.submit();
         }
      });
      $("#btnDirect").click(function() {
         var sz230mm=$("#sz230mm").val();
         var sz240mm=$("#sz240mm").val();
         var sz250mm=$("#sz250mm").val();
         var sz260mm=$("#sz260mm").val();
         var sz270mm=$("#sz270mm").val();
         var sz280mm=$("#sz280mm").val();
         if (sz230mm == "0" && sz240mm == "0" && sz250mm == "0" && sz260mm == "0" && sz270mm == "0" && sz280mm == "0") {
            alert("수량을 선택하세요.");
         }else {
            document.form1.action = "${path}/direct/insert.do";
            document.form1.submit();
         }
      });
   });
</script>
<!-- product category -->
<section id="aa-product-details">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="aa-product-details-area">
					<div class="aa-product-details-content">
						<div class="row">
							<!-- Modal view slider -->
							<div class="col-md-5 col-sm-5 col-xs-12">
								<div class="aa-product-view-slider">
									<div id="demo-1" class="simpleLens-gallery-container">
										<div class="simpleLens-container">
											<div class="simpleLens-big-image-container">
												<a
													data-lens-image="${path}/resources/img/shoes/${product.image_url}"
													class="simpleLens-lens-image"> <img
													src="${path}/resources/img/shoes/${product.image_url}"
													class="simpleLens-big-image"></a>
											</div>
										</div>
										<div class="simpleLens-thumbnails-container">
											<a
												data-big-image="${path}/resources/img/shoes/${product.image_url}"
												data-lens-image="${path}/resources/img/shoes/${product.image_url}"
												class="simpleLens-thumbnail-wrapper" href="#"> <img
												src="${path}/resources/img/shoes/${product.image_url}"
												width="45px" height="55px">
											</a> <a
												data-big-image="${path}/resources/img/shoes/${product.image_url}"
												data-lens-image="${path}/resources/img/shoes/${product.image_url}"
												class="simpleLens-thumbnail-wrapper" href="#"> <img
												src="${path}/resources/img/shoes/${product.image_url}"
												width="45px" height="55px">
											</a> <a
												data-big-image="${path}/resources/img/shoes/${product.image_url}"
												data-lens-image="${path}/resources/img/shoes/${product.image_url}"
												class="simpleLens-thumbnail-wrapper" href="#"> <img
												src="${path}/resources/img/shoes/${product.image_url}"
												width="45px" height="55px">
											</a>
										</div>
									</div>
								</div>
							</div>
							<!-- Modal view content -->
							Category: <a href="#">${product.sort}</a>
							<div class="col-md-7 col-sm-7 col-xs-12">
								<div class="aa-product-view-content">
									<form name="form1" method="post">
										<input type="hidden" name="pseq" value="${product.pseq}">
										<h3>${product.pname}</h3>
										<div class="aa-price-block">
											<span class="aa-product-view-price">￦<fmt:formatNumber
													value="${product.price2}" /></span>
											<p class="aa-product-avilability">
												재고 현황: <span>재고있음</span>
											</p>
										</div>
										<p>${product.content}</p>

										<div class="aa-prod-view-size">
											<table>
												<tr>
													<th>사이즈&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
													<th>230mm&nbsp;&nbsp;&nbsp;</th>
													<th>240mm&nbsp;&nbsp;&nbsp;</th>
													<th>250mm&nbsp;&nbsp;&nbsp;</th>
													<th>260mm&nbsp;&nbsp;&nbsp;</th>
													<th>270mm&nbsp;&nbsp;&nbsp;</th>
													<th>280mm&nbsp;&nbsp;&nbsp;</th>
												</tr>
												<tr>
													<th>수량&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
													<td><select name="sz230mm" id="sz230mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
													<td><select name="sz240mm" id="sz240mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
													<td><select name="sz250mm" id="sz250mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
													<td><select name="sz260mm" id="sz260mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
													<td><select name="sz270mm" id="sz270mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
													<td><select name="sz280mm" id="sz280mm">
															<option value="0" selected>0</option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
															<option value="7">7</option>
															<option value="8">8</option>
															<option value="9">9</option>
															<option value="10">10</option>
													</select></td>
												</tr>
											</table>

										</div>

										<div></div>

										<div class="aa-prod-view-bottom">
											<input class="aa-add-to-cart-btn" type="button" id="btnCart"
												value="장 바 구 니"> <input class="aa-add-to-cart-btn"
												type="button" id="btnDirect" value="구 매 하 기">
										</div>
									</form>
								</div>
							</div>
						</div><br><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
</head>

<!-- / Subscribe section -->

<%@ include file="../include/Footer.jsp"%>

</body>
</html>