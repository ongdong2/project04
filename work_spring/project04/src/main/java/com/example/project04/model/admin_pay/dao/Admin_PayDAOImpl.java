package com.example.project04.model.admin_pay.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.admin_pay.dto.Admin_PayDTO;

@Repository
public class Admin_PayDAOImpl implements Admin_PayDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public List<Admin_PayDTO> paylist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.pay_list");
	}

	@Override
	public Admin_PayDTO orderDetail(int odseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_pay.order_detail", odseq);
	}

	@Override
	public Admin_PayDTO confirmorder(int odseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_pay.confirmorder", odseq);
	}
	@Override
	public Admin_PayDTO cancleorder(int odseq) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_pay.cancleorder", odseq);
	}

	@Override
	public void dispatch(Admin_PayDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.update("admin_pay.dispatch", dto);
	}

	@Override
	public void insertresult(Admin_PayDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.insert("admin_pay.insertresult", dto);
	}

	@Override
	public List<Admin_PayDTO> ordermoney1() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.ordermoney1");
	}
	@Override
	public List<Admin_PayDTO> ordermoney2() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.ordermoney2");
	}
	@Override
	public List<Admin_PayDTO> ordermoney3() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.ordermoney3");
	}

	@Override
	public void cutstock(Admin_PayDTO dto) {
		sqlSession.update("admin_pay.cutstock", dto);
		
	}

	@Override
	public List<Admin_PayDTO> dispatchlist() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.dispatchlist");
	}
	@Override
	public List<Admin_PayDTO> dispatchlist2() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.dispatchlist2");
	}
	@Override
	public List<Admin_PayDTO> dispatchlist3() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.dispatchlist3");
	}
	@Override
	public List<Admin_PayDTO> dispatchlist4() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_pay.dispatchlist4");
	}

	@Override
	public void cancleconfirm(Admin_PayDTO dto) {
		sqlSession.update("admin_pay.cancleconfirm", dto);
		
	}

	@Override
	public void cancledone(Admin_PayDTO dto) {
		sqlSession.update("admin_pay.cancledone", dto);
		
	}

	@Override
	public void updateresult(Admin_PayDTO dto) {
		sqlSession.update("admin_pay.updateresult", dto);
		
	}

}
