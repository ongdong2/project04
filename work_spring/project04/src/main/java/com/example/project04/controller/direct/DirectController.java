package com.example.project04.controller.direct;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.project04.model.direct.dto.DirectDTO;
import com.example.project04.service.direct.DirectService;

@Controller
@RequestMapping("direct/*")
public class DirectController {

	@Inject
	DirectService directService;
	
	@RequestMapping("insert.do")
	public String insert(HttpSession session, @ModelAttribute DirectDTO dto) {
		String id=(String)session.getAttribute("id");
		dto.setId(id);
		directService.show(id);
		directService.insert(dto);
		return "redirect:/direct.do";
	}

}
