<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
</head>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#btnList").click(function(){
		document.form123.action="${path}/inquiry/list.do";
		document.form123.submit();
	});
	//문의하기 저장
	$("#btnSave").click(function(){
		var subject=$("#subject").val();
		var content=$("#content").val();
		if(subject == ""){
			alert("제목을 작성해주세요.");
			$("#subject").focus();	
			return;
		}
		if(content == ""){
			alert("문의 내역을 작성해주세요.");
			$("#content").focus();
			return;
		}
		document.form123.action="${path}/inquiry/insert.do";
		document.form123.submit();
	});
});
</script>
<title>Insert title here</title>
<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table">
               <div class="table-responsive"  align="center">
                  <h1 align="center"><Strong>상품문의 작성</Strong></h1>
							<form id="form123" name="form123" method="post" action="${path}/inquiry/insert.do">
							<div align="left" style="margin-left:174px;"><Strong>문의제목</Strong></div><input name="subject" id="subject" size="90" placeholder="문의하실 내역의 제목을 입력해주세요">
							<div style="width:900px;"><br>
							
							<div align="left" style="margin-left:85px;"><Strong>문의 내역</Strong></div><textarea id="content" name="content" rows="15" cols="89.5" 
							placeholder="문의하실 내용을 입력해주세요"></textarea>
							</div>
							<div style="width=800px; text-align:center;">
								<button class="btn btn-primary" type="button" id="btnSave"><Strong>등록</Strong></button><br><br>
								<button class="btn btn-primary" type="button" id="btnList"><Strong>목록으로</Strong></button><br><br>
							</div>
							<br>
							</form>
							</div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>


<%@ include file="../include/Footer.jsp"%>

</body>
</html>