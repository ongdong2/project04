package com.example.project04.service.admin;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.admin.dao.AdminDAO;
import com.example.project04.model.admin.dto.AdminMemberDTO;
import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_member.dto.Admin_MemberDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

@Service
public class AdminServiceImpl implements AdminService {
	
	@Inject
	AdminDAO adminDao;

	@Override
	public String loginCheck(AdminMemberDTO dto) {
		// TODO Auto-generated method stub
		return adminDao.loginCheck(dto);
	}

	@Override
	public List<Admin_MemberDTO> listMember() {
		// TODO Auto-generated method stub
		return adminDao.listMember();
	}

	@Override
	public List<Admin_DealerDTO> listDealer() {
		// TODO Auto-generated method stub
		return adminDao.listDealer();
	}

	@Override
	public List<Admin_ProductDTO> listProduct() {
		// TODO Auto-generated method stub
		return adminDao.listProduct();
	}

	@Override
	public List<Admin_NewProductDTO> listNewproduct() {
		// TODO Auto-generated method stub
		return adminDao.listNewproduct();
	}

	@Override
	public List<Admin_ProductDTO> orderlist() {
		// TODO Auto-generated method stub
		return adminDao.orderlist();
		
	}

	@Override
	public List<Admin_ProductDTO> orderstocklist() {
		// TODO Auto-generated method stub
		return adminDao.orderstocklist();
	}

	

}
