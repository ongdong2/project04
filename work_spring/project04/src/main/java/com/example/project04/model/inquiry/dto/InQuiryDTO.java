package com.example.project04.model.inquiry.dto;

import java.util.Arrays;
import java.util.Date;

public class InQuiryDTO {
private int qseq; //글번호
private String subject; //문의 제목
private String content; //문의 내용
private int viewcnt; //조회수
private String answershow; //삭제 여부,답변처리
private String id; //회원 아이디
private String writer;
private Date indate; //작성일자
private String ansertext;
public String getAnsertext() {
	return ansertext;
}
public void setAnsertext(String ansertext) {
	this.ansertext = ansertext;
}
private String[] files; //첨부파일
public int getQseq() {
	return qseq;
}
public void setQseq(int qseq) {
	this.qseq = qseq;
}
public String getSubject() {
	return subject;
}
public void setSubject(String subject) {
	this.subject = subject;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public int getViewcnt() {
	return viewcnt;
}
public void setViewcnt(int viewcnt) {
	this.viewcnt = viewcnt;
}
public String getAnswershow() {
	return answershow;
}
public void setAnswershow(String answershow) {
	this.answershow = answershow;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getWriter() {
	return writer;
}
public void setWriter(String writer) {
	this.writer = writer;
}
public Date getIndate() {
	return indate;
}
public void setIndate(Date indate) {
	this.indate = indate;
}
public String[] getFiles() {
	return files;
}
public void setFiles(String[] files) {
	this.files = files;
}
@Override
public String toString() {
	return "InQuiryDTO [qseq=" + qseq + ", subject=" + subject + ", content=" + content + ", viewcnt=" + viewcnt
			+ ", answershow=" + answershow + ", id=" + id + ", writer=" + writer + ", indate=" + indate + ", ansertext="
			+ ansertext + ", files=" + Arrays.toString(files) + "]";
}

}
