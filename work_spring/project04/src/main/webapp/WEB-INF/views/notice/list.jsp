<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="../include/header.jsp"%>
<%@ include file="../include/menu.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script>
 $(function(){
		$("#btnWrite").click(function(){
			location.href="${path}/notice/write.do";
		});
 });

 function list(page){
	 location.href="${path}/notice/list.do?curPage="+page;
 }
</script>
<body>
<section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-20">
         <div class="cart-view-area">
           <div class="cart-view-table aa-wishlist-table" style="height:890px;">
                   	<form id="form15" name="form15" method="post" >
               <div class="table-responsive" align="center">

			<h1><Strong>공지사항</Strong></h1><br></div>

			 <p align="left" style="margin-left: 60px;"><Strong><span style=color:red>${map.count}</span></Strong>개의 공지글이 있습니다.</p>
			
			<div class="table-responsive" align="center">
			<table border="1" align="center" width="1000px" >
			<tr>
					<td align="center" style="width: 50px;"><Strong>번호</Strong></td>
					<td align="center"><Strong>공지</Strong></td>
					<td align="center" style="width: 100px;"><Strong>작성자</Strong></td>
					<td align="center" style="width: 100px;"><Strong>공지일자</Strong></td>
					<td align="center" style="width: 50px;"><Strong>조회수</Strong></td>
			</tr>
		<c:forEach var="dto" items="${map.list}">
						<tr>
						<td align="center">${dto.nseq}</td>
						<td><c:if test="${dto.viewcnt < 10}"><span style=color:red>new</span> </c:if><a  style=color:black href="${path}/notice/view.do?nseq=${dto.nseq}&adminid=${dto.adminid}&curPage=${map.pager.curPage}"> ${dto.subject}</a></textarea></td>
						<td align="center">${dto.adminid}</td>
						<td align="center"><fmt:formatDate value="${dto.indate}" pattern="yyyy-MM-dd a HH:mm:ss"/></td>
						<td align="center">${dto.viewcnt}</td>
						</tr>
		</c:forEach>
				 <!-- 페이지 나누기  -->
			<tr>
		<td colspan="5" align="center">
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('1')">[처음]</a>
			</c:if>
			<c:if test="${map.pager.curBlock > 1}">
				<a href="#" onclick="list('${map.pager.prevPage}')">
				[이전]</a>
			</c:if>
			<c:forEach var="num" begin="${map.pager.blockBegin}" end="${map.pager.blockEnd}">
				<c:choose>
					<c:when test="${num == map.pager.curPage}">
					<!-- 현재 페이지인 경우 하이퍼링크 제거 -->
						<span style="color:red;">${num}</span>
					</c:when>
					<c:otherwise>
						<a href="#" onclick="list('${num}')">${num}</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
			<c:if test="${map.pager.curBlock < map.pager.totBlock}">
				<a href="#" 
				onclick="list('${map.pager.nextPage}')">[다음]</a>
			</c:if>
			<c:if test="${map.pager.curPage < map.pager.totPage}">
				<a href="#" 
				onclick="list('${map.pager.totPage}')">[끝]</a>
			</c:if>
		</td>
	</tr>
	</table><br>
	
	<c:if test="${sessionScope.id == 'admin1'}">
		<div align="left" style="margin-left: 950px;"><button class="btn btn-primary pull-left" id="btnWrite" type="button" ><Strong>공지글작성</Strong></button></div>
	</c:if>
            
            </form>
            </div>
             </div>
             </div>
             </div>      
             </div>
           </div>
 </section>
</body>
<%@ include file="../include/Footer.jsp"%>
</html>