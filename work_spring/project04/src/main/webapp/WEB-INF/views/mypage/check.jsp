<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
  <head>
	<%@ include file="../include/header.jsp" %>  
	<%@ include file="../include/menu.jsp" %>
 
<%--   <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="${path}/resources/img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>구매해 주셔서 감사합니다.</h2>
        <ol class="breadcrumb">        
          <li class="active"><a href="${path}/mypage.do">주문내역 확인</a></li>
          
        </ol>
      </div>
     </div>
   </div>
  </section> --%>
  
  

  <section id="aa-error">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-error-area" style="padding-bottom: 50px;">
          <h2>구매해주셔서 감사합니다</h2>
        <img src="${path}/resources/img/banner/thx_banner.jpg" alt="cart_banner.jpg"><br>
            <a href="${path}/"> 계속 쇼핑하기</a>
            <a href="${path}/mypage.do"> 주문목록 확인</a>
          </div>
        </div>
      </div>
      <br><br>		<!-- 밑공간 여백 -->
    </div>
  </section>
  
  
  
  
  
  
  
  
  
  
    <%@ include file="../include/Footer.jsp" %>

  </body>
</html>