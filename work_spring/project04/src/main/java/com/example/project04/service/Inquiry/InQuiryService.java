package com.example.project04.service.Inquiry;

import java.util.List;


import com.example.project04.model.inquiry.dto.InQuiryDTO;

public interface InQuiryService {
	public List<InQuiryDTO> list(String search_option,String keyword,int start,int end) throws Exception;
	public int countPage(String search_option,String keyword) throws Exception;
	public void create(InQuiryDTO dto) throws Exception;//문의글 작성
	public void delete(int qseq) throws Exception;//문의 내역 삭제
	public void update(InQuiryDTO dto) throws Exception;//문의 내역 수정 
	public InQuiryDTO view(int qseq) throws Exception;//문의 상세보기
	public void answer(int qseq) throws Exception;

}
