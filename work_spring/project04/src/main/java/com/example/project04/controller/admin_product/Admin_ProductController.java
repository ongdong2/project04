package com.example.project04.controller.admin_product;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;
import com.example.project04.service.admin.Pager;
import com.example.project04.service.pro_admin.Admin_ProductService;

@Controller
@RequestMapping("admin_product/*")
public class Admin_ProductController {
	
	@Inject
	Admin_ProductService productService;
	
	@RequestMapping("productList.do")
	public ModelAndView list(@RequestParam(defaultValue="1")int curPage) throws Exception {
		int count=productService.countArticle();
		
		Pager pager=new Pager(count,curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		List<Admin_ProductDTO> list=productService.listProduct(start,end);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", list.size());
		map.put("pager", pager);
		mav.setViewName("admin_product/product_view");
		mav.addObject("map", map);
		return mav;
	}
	//상품재고부족
	@RequestMapping("needlist.do")
	public ModelAndView needlist(ModelAndView mav) {
		mav.setViewName("admin_product/product_need");
		mav.addObject("list", productService.needlist());
		return mav;
	}
	@RequestMapping("product_write.do")
	public String write() {
		return "/admin_product/product_write";
	}
	@RequestMapping("product_detail/{pseq}")
	public ModelAndView view(@PathVariable("pseq") int pseq,ModelAndView mav) {
		mav.setViewName("admin_product/product_detail");
		mav.addObject("dto", productService.viewProduct(pseq));
		return mav;
	}
	@RequestMapping("product_delete.do")
	public String delete(@RequestParam int pseq) {
		
		productService.deleteProduct(pseq);;
		return "redirect:/admin_product/productList.do";
	}
	@RequestMapping("insert.do")
	public String insert(Admin_ProductDTO dto) {
		String filename="";
		if(!dto.getImage().isEmpty()) {
			filename=dto.getImage().getOriginalFilename();
			try {
				
				String path="D:\\work\\.metadata\\.plugins\\org.eclipse.wst.server.core\\"
						+ "tmp0\\wtpwebapps\\spring02\\WEB-INF\\views\\images\\";
				new File(path).mkdir();
				dto.getImage().transferTo(new File(path+filename));
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		}
		dto.setImage_url(filename);
		productService.insertProduct(dto);
		return "redirect:/admin_product/productList.do";
		
	}
	@RequestMapping("product_edit/{pseq}")
	public ModelAndView edit(@PathVariable("pseq") int pseq, ModelAndView mav) {
	mav.setViewName("admin_product/product_edit");
	mav.addObject("dto", productService.detailProduct(pseq));
	return mav;
	}
	@RequestMapping("product_update.do")
	public String update(Admin_ProductDTO dto) {
		productService.updateProduct(dto);
		return "redirect:/admin_product/productList.do";
	}
	@RequestMapping("product_order/{pseq}")
	public ModelAndView order(@PathVariable("pseq") int pseq,ModelAndView mav) {
		mav.setViewName("admin_product/product_order");
		mav.addObject("dto", productService.orderProduct(pseq));
		return mav;
	}
	@RequestMapping("insertproduct.do")
	public String insertproduct(Admin_ProductDTO dto) {
		//int num = dto.getSz230mm();
		System.out.println(dto);
		productService.insertProduct(dto);
		
		return "redirect:/admin/order_list.do";
	}
	@RequestMapping("deleteorder.do")
	public String deleteorder(@RequestParam int pseq) {
		productService.deleteOrder(pseq);
		
		return "redirect:/admin/adminAll.do";
	}
	

/*꺵판을 시작하지
ㅋㅋㅋㅋㅋㅋㅋㅋㅋ
다 망해라
갈배 존맛
ㅋㅋㅋㅋㅋㅋ
idh
먹고싶지~~~?
안줄거야
빨간줄 보소~~~~
기가 막히죠ㅎㅎㅎ
오류~~~~
응 ㅇ난돼*/
	
}
