package com.example.project04.service.admin_pay;

import java.util.List;

import com.example.project04.model.admin_member.dto.Admin_MemberDTO;
import com.example.project04.model.admin_pay.dto.Admin_PayDTO;

public interface Admin_PayService {
	public List<Admin_PayDTO> paylist();
	public Admin_PayDTO orderDetail(int odseq);
	public Admin_PayDTO confirmorder(int odseq);
	public Admin_PayDTO cancleorder(int odseq);
	public void dispatch(Admin_PayDTO dto);
	public void updateresult(Admin_PayDTO dto);
	public void cancleconfirm(Admin_PayDTO dto);
	public void insertresult(Admin_PayDTO dto);
	public List<Admin_PayDTO> ordermoney1();
	public List<Admin_PayDTO> ordermoney2();
	public List<Admin_PayDTO> ordermoney3();
	public void cutstock(Admin_PayDTO dto);
	public List<Admin_PayDTO> dispatchlist();
	public List<Admin_PayDTO> dispatchlist2();
	public List<Admin_PayDTO> dispatchlist3();
	public List<Admin_PayDTO> dispatchlist4();
}
