package com.example.project04.service.admin_dealer;

import java.util.List;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

public interface Admin_DealerService {
	public List<Admin_DealerDTO> listDealer(int start,int end);
	public List<Admin_ProductDTO> prolistDealer(int dseq);
	public void insertDealer(Admin_DealerDTO dto);
	public Admin_DealerDTO viewDealer(int dseq);
	public Admin_ProductDTO viewproDealer(int dseq);
	public void deleteDealer(Admin_DealerDTO dto);
	public void updateDealer(Admin_DealerDTO dto);
	public Admin_DealerDTO detailDealer(int dseq);
	public int countArticle() throws Exception;
	public void addDealer(Admin_DealerDTO dto);
}
