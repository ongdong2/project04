package com.example.project04.service.notice;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.example.project04.model.notice.dto.NoticeDTO;

public interface NoticeService {
	List<NoticeDTO> list(int start,int end) throws Exception;
	public int countPage();
	public void create(NoticeDTO dto);
	public void delete(int nseq);
	public void update(NoticeDTO dto) throws Exception;
	public NoticeDTO view(int nseq);
	public void viewCnt(int nseq,HttpSession session) throws Exception;
	
}
