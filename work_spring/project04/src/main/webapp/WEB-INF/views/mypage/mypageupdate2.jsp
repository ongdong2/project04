<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Mypage</title>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
	$(function(){
		$("#memberupdate2").click(function(){
			var phone1 = $("#phone1").val();
			var address1 = $("#address1").val();
			var exp4 = /^[0-9]{3,}\-[0-9]{4,}\-[0-9]{4,}$/;
			
			if (!exp4.test(phone1)) {
				alert("전화번호는 숫자만 입력하세요.");
				$("#phone1").focus();
				return;
			}
			if (address1.length < 3) {
				alert("상세주소를 정확히 입력하세요.")
				$("#address1").focus();
				return;
			}
			alert("1번 연락처/배송지정보가 변경되었습니다.")
			document.form3.action="${path}/member/memberupdate22.do";
			document.form3.submit();
		});
		$("#memberupdate3").click(function(){
			var phone2 = $("#phone2").val();
			var address2 = $("#address2").val();
			var exp4 = /^[0-9]+$/;
			
			if (!exp4.test(phone2)) {
				alert("전화번호는 숫자만 입력하세요.");
				$("#phone2").focus();
				return;
			}
			if (address2.length < 3) {
				alert("상세주소를 정확히 입력하세요.")
				$("#address2").focus();
				return;
			}
			alert("2번 연락처/배송지정보가 변경되었습니다.")
			document.form4.action="${path}/member/memberupdate33.do";
			document.form4.submit();
		});
	});
	
</script>


<script>
	function daumZipCode1() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('zipcode1').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('zip_num1').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('address1').focus();
					}
				}).open();
	}
</script>
<script>
	function daumZipCode2() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('zipcode2').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('zip_num2').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('address2').focus();
					}
				}).open();
	}
</script>


<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%@ include file="../include/menu.jsp"%>


<div class="container">
		<form method="post" action="register.do" id="form3" name="form3">
			<table class="table table-bordered table-hover"
				style="width: 480px; text-align: cetner; border: 1px solid #dddddd">
				<thead>
					<tr>
						<th style="width: 550px" colspan="4">
							<h4>연락처, 배송지관리1</h4>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 150px;"><h5>전화번호</h5></td>
						<td colspan="2"><input class="form-control" type="text"
							value="${sessionScope.phone1}" style="width: 350px"
							id="phone1" name="phone1" maxLength="20"></td>
					</tr>
						<tr>
						<td style="width: 75px;"><h5>우편번호</h5></td>
						<td colspan="2"><input class="form-inline; form-control" type="text" value="${sessionScope.zipcode1}"
							style="width: 350px" id="zipcode1" name="zipcode1" readonly
							maxLength="60"> <input class="btn btn-primary  pull-right" type="button" onclick="daumZipCode1()" value="우편번호찾기"></td>
					</tr>
					<tr>
						<td style="width: 75px;"><h5>주소</h5></td>
						<td colspan="2"><input class="form-control" type="text" value="${sessionScope.zip_num1}"
							style="width: 350px" id="zip_num1" name="zip_num1" readonly
							maxLength="60"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>상세주소</h5></td>
						<td colspan="2"><input class="form-control" type="text" value="${sessionScope.address1}"
							style="width: 350px" id="address1" name="address1"
							maxLength="60"></td>
					</tr>
					<tr>
						<td style="text-align: left" colspan="3"><input
							class="btn btn-primary pull-right" type="button"
							id="memberupdate2" value="회원 정보 수정">
					</tr>
				</tbody>
			</table>
		</form>
		<!-------------------------------------------------------------------------------------------------------->
		<form method="post" action="register.do" id="form4" name="form4">
			<table class="table table-bordered table-hover"
				style="width: 480px; text-align: cetner; border: 1px solid #dddddd">
				<thead>
					<tr>
						<th style="width: 550px" colspan="4">
							<h4>연락처, 배송지관리2</h4>
					</tr>
				</thead>
				<tbody>
		 			<tr>
						<td style="width: 150px;"><h5>전화번호</h5></td>
						<td colspan="2"><input class="form-control" type="text"
							value="${sessionScope.phone2}" style="width: 350px"
							id="phone2" name="phone2" maxLength="20"></td>
					</tr> 
						<tr>
						<td style="width: 75px;"><h5>우편번호</h5></td>
						<td colspan="2"><input class="form-inline; form-control" type="text" value="${sessionScope.zipcode2}"
							style="width: 350px" id="zipcode2" name="zipcode2" readonly
							maxLength="60"> <input class="btn btn-primary  pull-right" type="button" onclick="daumZipCode2()" value="우편번호찾기"></td>
					</tr>
					<tr>
						<td style="width: 75px;"><h5>주소</h5></td>
						<td colspan="2"><input class="form-control" type="text" value="${sessionScope.zip_num2}"
							style="width: 350px" id="zip_num2" name="zip_num2" readonly
							maxLength="60"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>상세주소</h5></td>
						<td colspan="2"><input class="form-control" type="text" value="${sessionScope.address2}"
							style="width: 350px" id="address2" name="address2"
							maxLength="60"></td>
					</tr>
					<tr>
						<td style="text-align: left" colspan="3"><input
							class="btn btn-primary pull-right" type="button"
							id="memberupdate3" value="회원 정보 수정">
					</tr>
				</tbody>
			</table>
		</form>



<%@ include file="../include/Footer.jsp"%>
</body>
</html>