package com.example.project04.model.admin_member.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.example.project04.model.admin_member.dto.Admin_MemberDTO;

@Repository
public class Admin_MemberDAOImpl implements Admin_MemberDAO {
	
	@Inject
	SqlSession sqlSession;

	@Override
	public List<Admin_MemberDTO> listMember(int start,int end) {
		Map<String,Object> map=new HashMap<>();
		map.put("start", start);
		map.put("end", end);
		// TODO Auto-generated method stub
		return sqlSession.selectList("admin_member.member_list",map);
	}

	@Override
	public void insertmember(Admin_MemberDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Admin_MemberDTO viewMember(String id) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_member.member_view",id);
	}

	@Override
	public void deletemember(String id) {
		sqlSession.delete("admin_member.member_delete", id);
		
	}

	@Override
	public void updateMember(Admin_MemberDTO dto) {
		sqlSession.update("admin_member.member_update", dto);
		
	}

	@Override
	public boolean checkPw(String id, String passwd) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("admin_member.countArticle");
	}

}
