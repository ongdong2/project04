package com.example.project04.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.cart.dto.CartDTO;
import com.example.project04.model.checkout.dto.CheckoutDTO;
import com.example.project04.model.direct.dto.DirectDTO;
import com.example.project04.model.member.dto.MemberDTO;
import com.example.project04.model.order.dto.OrderDTO;
import com.example.project04.service.cart.CartService;
import com.example.project04.service.checkout.CheckoutService;
import com.example.project04.service.dealer.DealerService;
import com.example.project04.service.direct.DirectService;
import com.example.project04.service.member.MemberService;
import com.example.project04.service.order.OrderService;
import com.example.project04.service.product.ProductService;

@Controller
public class MenuController {

	@Inject
	CartService cartService;
	@Inject
	ProductService productService;
	@Inject
	DealerService dealerService;
	@Inject
	CheckoutService checkoutService;
	@Inject
	DirectService directService;
	@Inject
	OrderService orderService;
	@Inject
	MemberService memberService;
	
	@RequestMapping("cart.do")
	public ModelAndView list(HttpSession session, ModelAndView mav) {
		Map<String, Object> map=new HashMap<>();
		String id=(String)session.getAttribute("id");
		if(id!=null) {
			List<CartDTO> list=cartService.listCart(id);
			/*if (show == 'y') {
				int sumMoney=cartService.sumMoney(id);
				map.put("sumMoney", sumMoney);
			} else {
				int sumMoney=0;
				map.put("sumMoney", sumMoney);
			}*/
			int sumMoney=cartService.sumMoney(id);
			map.put("sumMoney", sumMoney);
			map.put("list", list);
			mav.setViewName("cart/cart_list");
			mav.addObject("map", map);
			return mav;
		}else {
			return new ModelAndView("admin/admin_login","",null);
		}
	}

	@RequestMapping("checkout.do")
	public ModelAndView list2(HttpSession session, ModelAndView mav, @ModelAttribute CheckoutDTO dto,  MemberDTO dto2) {
		Map<String, Object> map=new HashMap<>();
		String id=(String)session.getAttribute("id");
		if(id!=null) {
//			checkoutService.insert(dto);
			List<CheckoutDTO> list=checkoutService.listCheckout(id);
			List<MemberDTO> mlist=memberService.memberinfo(id);
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$체크아웃$$$$$$$$$$$$$$$$$$$$$$$$$$$$44"+list);
			int sumMoney=checkoutService.sumMoney(id);
			map.put("sumMoney", sumMoney);
			map.put("mlist", mlist);
			map.put("clist", list);
			mav.setViewName("checkout/checkout_list");
			mav.addObject("map", map);
			return mav;
		}else {
			return new ModelAndView("admin/admin_login","",null);
		}

	}
	
	@RequestMapping("direct.do")
	public ModelAndView list3(HttpSession session, ModelAndView mav, @ModelAttribute DirectDTO dto,  MemberDTO dto2) {
		Map<String, Object> map=new HashMap<>();
		String id=(String)session.getAttribute("id");
		if(id!=null) {
//			directService.insert(dto);
			List<DirectDTO> list=directService.listDirect(id);
			List<MemberDTO> mlist=memberService.memberinfo(id);
			System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$다이렉트$$$$$$$$$$$$$$$$$$$$$$$$$$$$44"+list);
			int dsumMoney=directService.sumMoney(id);
			map.put("dsumMoney", dsumMoney);
			map.put("mlist", mlist);
			map.put("dlist", list);
			mav.setViewName("direct/direct_list");
			mav.addObject("map", map);
			return mav;
		}else {
			return new ModelAndView("admin/admin_login","",null);
		}

	}
	
	@RequestMapping("checkend1.do")
	public String cinsert(HttpSession session, int[] cseq, int[] pseq, 
						int[] sz230mm, int[] sz240mm, int[] sz250mm, int[] sz260mm, int[] sz270mm, int[] sz280mm, 
						int[] price2, String[] image_url, String[] pname, int[] amount, int[] total, String addradio, Model model,
						MemberDTO dto2) {
		String id=(String)session.getAttribute("id");
		
		//System.out.println("직접입력주소록:"+dto2.toString());
		
		for(int i=0; i<cseq.length; i++) {
			OrderDTO dto=new OrderDTO();
			dto.setId(id);
			dto.setCseq(cseq[i]);
			dto.setPseq(pseq[i]);
			dto.setSz230mm(sz230mm[i]);
			dto.setSz240mm(sz240mm[i]);
			dto.setSz250mm(sz250mm[i]);
			dto.setSz260mm(sz260mm[i]);
			dto.setSz270mm(sz270mm[i]);
			dto.setSz280mm(sz280mm[i]);
			dto.setPrice2(price2[i]);
			dto.setImage_url(image_url[i]);
			dto.setPname(pname[i]);
			dto.setAmount(amount[i]);
			dto.setTotal(total[i]);
			
			String address1 = dto2.getAddress1();
			String address2 = dto2.getAddress2();
			String phone1 = dto2.getPhone1();
			String phone2 = dto2.getPhone2();
			String zipcode1 = dto2.getZipcode1();
			String zipcode2 = dto2.getZipcode2();
			String zip_num1 = dto2.getZip_num1();
			String zip_num2 = dto2.getZip_num2();
			
			dto.setAddress1(address1);
			dto.setAddress2(address2);
			dto.setPhone1(phone1);
			dto.setPhone2(phone2);
			dto.setZip_num1(zip_num1);
			dto.setZip_num2(zip_num2);
			dto.setZipcode1(zipcode1);
			dto.setZipcode2(zipcode2);
			orderService.cinsert(dto);
			String mname = memberService.findname(id);
			dto.setMname(mname);
			System.out.println("가져온이름:"+mname);
			model.addAttribute("mname", mname);
			model.addAttribute("codto",dto);
			model.addAttribute("add",addradio);
			System.out.println("codto:"+dto);
		}
		cartService.show(id);
		return "redirect:/check.do";
	}
	
	@RequestMapping("checkend2.do")
	public String oinsert(HttpSession session, @ModelAttribute OrderDTO dto, String addradio, Model model) {
		String id=(String)session.getAttribute("id");
		dto.setId(id);
		System.out.println("##################################디티오나오나#######################################"+dto);
		orderService.oinsert(dto);
		return "redirect:/check.do";
	}
	

	@RequestMapping("check.do")
	public String check(@ModelAttribute OrderDTO dto, String add, Model model, @ModelAttribute MemberDTO dto2) {
		model.addAttribute("add",add);
		return "mypage/check";
	}    
	/*@RequestMapping("check.do")
	public String cutstock(int pseq,OrderDTO dto) {
		orderService.cutstock(pseq);
		List<OrderDTO> list=orderService.listcut(dto);
		return "redirect:/mypage/check";
	}*/

	
	@RequestMapping("mypage.do")
	public ModelAndView orderlist(HttpSession session, ModelAndView mav, @ModelAttribute OrderDTO dto, String add) {
		Map<String, Object> map=new HashMap<>();
		String id=(String)session.getAttribute("id");
		if(id!=null) {
			List<OrderDTO> clist=orderService.clistOrder(id);
			List<OrderDTO> olist=orderService.olistOrder(id);
			map.put("add", add);
			map.put("clist", clist);
			map.put("olist", olist);
			mav.setViewName("mypage/order_detail");
			mav.addObject("map", map);
			return mav;
		}else {
			return new ModelAndView("admin/admin_login","",null);
		}
	}

	
	@RequestMapping("qna.do")
	public String qna() {
		return "qna/qna";
	}
}
