<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<%@ include file="../include/header.jsp"%>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Daily Shop | Register</title>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>

<script>
	$(function() {
		$("#emailconfirm").click(function() {
			document.form8.action = "${path}/email/send.do";
			document.form8.submit();
		});
	});
</script>
<script>
	$(function() {
		$("#checkdo").click(function() {
			var num = $("#emailcheck").val();
			var result = "";
			var emailnum = $
			{
				number
			}
			;
			if (num == emailnum) {
				alert("이메일 인증이 완료되었습니다.");
			} else {
				alert("인증번호가 일치하지 않습니다.");
				$("#emailcheck").focus();
				return;
			}
			document.form8.action = "${path}/member/register.do"
			document.form8.submit();
		});

		$("#checkdo").click(function() {
			var num = $("#emailcheck").val();
			var emailnum = $
			{
				number
			}
			;
			if (num != emailnum) {
				alert("인증번호가 일치하지 않습니다.");
				$("#emailcheck").focus();
				return;
			}
		});
	});
</script>


<!-- Font awesome -->
<link href="${path}/resources/css/custom.css" rel="stylesheet">
<link href="${path}/resources/css/font-awesome.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="${path}/resources/css/bootstrap.css" rel="stylesheet">
<!-- SmartMenus jQuery Bootstrap Addon CSS -->
<link href="${path}/resources/css/jquery.smartmenus.bootstrap.css"
	rel="stylesheet">
<!-- Product view slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/jquery.simpleLens.css">
<!-- slick slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/slick.css">
<!-- price picker slider -->
<link rel="stylesheet" type="text/css"
	href="${path}/resources/css/nouislider.css">
<!-- Theme color -->
<link id="switcher"
	href="${path}/resources/css/theme-color/default-theme.css"
	rel="stylesheet">
<!-- <link id="switcher" href="${path}/resources/css/theme-color/bridge-theme.css" rel="stylesheet"> -->
<!-- Top Slider CSS -->
<link href="${path}/resources/css/sequence-theme.modern-slide-in.css"
	rel="stylesheet" media="all">

<!-- Main style sheet -->
<link href="${path}/resources/css/style.css" rel="stylesheet">

<!-- Google Font -->
<link href='https://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway'
	rel='stylesheet' type='text/css'>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Daily||Show||EmailCheck</title>
</head>
<body>
	<%@ include file="../include/menu.jsp"%>

	<div class="container">
		<form method="post" action="register.do" id="form8" name="form8">
			<table class="table table-bordered table-hover"
				style="width: 760px; text-align: cetner; border: 1px solid #dddddd">
				<thead>
					<tr>
						<th style="width: 550px" colspan="3">
							<h4>이메일 인증</h4>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 150px;"><h5>이메일</h5></td>
						<td colspan="2"><input class="form-inline; form-control"
							type="email" placeholder="example@gmail.com" style="width: 350px"
							id="receiveMail" name="email" maxLength="20"> <input
							class="btn btn-primary" type="button" id="emailconfirm"
							value="이메일인증"></td>
					</tr>
					<tr>
						<td style="width: 150px;"><h5>이메일 인증번호</h5></td>
						<td colspan="2"><input class="form-inline; form-control"
							type="text" placeholder="인증번호 4자리 입력해주세요." style="width: 350px"
							id="emailcheck" name="emailcheck" maxLength="20"> <input
							class="btn btn-primary" type="button" id="checkdo" value="인증하기"></td>

					</tr>
				</tbody>
			</table>
		</form>
	</div>
	<%@ include file="../include/Footer.jsp"%>















</body>
</html>