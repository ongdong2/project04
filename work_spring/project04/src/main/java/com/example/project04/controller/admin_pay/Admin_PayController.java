package com.example.project04.controller.admin_pay;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.admin_pay.dto.Admin_PayDTO;
import com.example.project04.service.admin_pay.Admin_PayService;

@Controller
@RequestMapping("admin_pay/*")
public class Admin_PayController {
	
	@Inject
	Admin_PayService adminpayService;

	
	@RequestMapping("pay_list.do")
	public ModelAndView paylist(ModelAndView mav) {
		mav.setViewName("admin_pay/admin_pay");
		mav.addObject("list", adminpayService.paylist());
		mav.addObject("list2", adminpayService.dispatchlist());
		mav.addObject("list3", adminpayService.dispatchlist2());
		mav.addObject("list4", adminpayService.dispatchlist3());
		mav.addObject("list5", adminpayService.dispatchlist4());
		return mav;
	}
	@RequestMapping("order_detail/{odseq}")
	public ModelAndView view(@PathVariable("odseq") int odseq,ModelAndView mav) {
		mav.setViewName("admin_pay/order_detail");
		mav.addObject("dto", adminpayService.orderDetail(odseq));
		return mav;
	}
	@RequestMapping("confirmorder/{odseq}")
	public ModelAndView confirm(@PathVariable("odseq") int odseq,ModelAndView mav) {
		mav.setViewName("admin_pay/confirmorder");
		mav.addObject("dto", adminpayService.confirmorder(odseq));
		return mav;
	}
	@RequestMapping("cancleorder/{odseq}")
	public ModelAndView cancleorder(@PathVariable("odseq") int odseq,ModelAndView mav) {
		mav.setViewName("admin_pay/cancleorder");
		mav.addObject("dto", adminpayService.cancleorder(odseq));
		return mav;
	}
	@Transactional
	@RequestMapping("cancle.do")
	public String cancleconfirm(Admin_PayDTO dto) {
		adminpayService.cancleconfirm(dto);
		adminpayService.updateresult(dto);
		return "redirect:/admin_pay/pay_list.do";
	}
	@Transactional
	@RequestMapping("done.do")
	public String dispatch(Admin_PayDTO dto) {
		adminpayService.dispatch(dto);
		adminpayService.cutstock(dto);
		adminpayService.insertresult(dto);
		return "redirect:/admin_pay/pay_list.do";
	}
	
	
}
