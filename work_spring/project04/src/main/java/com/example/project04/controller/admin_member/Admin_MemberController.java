package com.example.project04.controller.admin_member;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.admin_dealer.dto.Admin_DealerDTO;
import com.example.project04.model.admin_member.dto.Admin_MemberDTO;
import com.example.project04.service.admin.Pager;
import com.example.project04.service.admin_member.Admin_MemberService;

@Controller
@RequestMapping("admin_member/*")
public class Admin_MemberController {
	
	@Inject
	Admin_MemberService memberService;
	@RequestMapping("list.do")
	public ModelAndView memberList(@RequestParam(defaultValue="1")int curPage) throws Exception{
		int count=memberService.countArticle();
		
		Pager pager=new Pager(count,curPage);
		int start=pager.getPageBegin();
		int end=pager.getPageEnd();
		List<Admin_MemberDTO> list=memberService.listMember(start,end);
		ModelAndView mav=new ModelAndView();
		HashMap<String,Object> map=new HashMap<>();
		map.put("list", list);
		map.put("count", list.size());
		map.put("pager", pager);
		mav.setViewName("admin_member/member_list");
		mav.addObject("map", map);
		System.out.println(mav);
		return mav;
	}
	@RequestMapping("memberView/{id}")
	public ModelAndView view(@PathVariable("id") String id,ModelAndView mav) {
		mav.setViewName("admin_member/member_view");
		mav.addObject("dto", memberService.viewMember(id));
		return mav;
	}
	@RequestMapping("delete.do")
	public String delete(String id) {
		memberService.deletemember(id);
		return "redirect:/admin_member/list.do";
	}
}
