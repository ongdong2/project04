package com.example.project04.controller.member;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.email.EmailDTO;
import com.example.project04.model.member.dao.MemberDAO;
import com.example.project04.model.member.dto.MemberDTO;
import com.example.project04.model.product.dto.ProductDTO;
import com.example.project04.service.email.EmailService;
import com.example.project04.service.member.MemberService;
import com.example.project04.service.product.ProductService;


@Controller
@RequestMapping("member/*")
public class MemberController {
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);
	@Inject
	MemberService memberService;
	@Inject
	MemberDAO memberDao;
	@Inject
	ProductService productService;
	@Inject
	EmailService emailService;
	
	// @RequestMapping("login.do")
	// public String login() {
	// System.out.println("여기까지 잘왔어요");
	// return "index/index";
	//
	// }
	@RequestMapping("login.do")
	public ModelAndView login_check(MemberDTO dto, HttpSession session, HttpServletRequest request, Model model) {
		System.out.println(dto);
		boolean result = memberService.loginCheck(dto, session);
		System.out.println(result);
		ModelAndView mav = new ModelAndView();
		
		
		List<ProductDTO> list = productService.home_list();
		model.addAttribute("list", list);
		System.out.println("로그인-상품리스트출력"+list);
		
		List<ProductDTO> men_list = productService.man_list();
		model.addAttribute("mlist",men_list);
		System.out.println("섭로로는 남자다!!!!!"+men_list);
		
		List<ProductDTO> woman_list = productService.woman_list();
		model.addAttribute("wlist",woman_list);
		System.out.println("섭로로는 여자다!!!!"+woman_list);
		
		List<ProductDTO> public_list = productService.woman_list();
		model.addAttribute("plist",public_list);
		System.out.println("섭로로는 공룡이다!!!!"+woman_list);
		
		
		if (result) {
			String referer=request.getHeader("referer");
			System.out.println(referer);
			mav.setViewName("home");
			mav.addObject("message", "ok");
		} else {
			mav.setViewName("home");
			mav.addObject("message", "error");
		}
		return mav;
	}
	
	@RequestMapping("registerEC.do")
	public String registerEmailCheck() {
		return "register/registerEC";
	}
	
	@RequestMapping("register.do")
	public String register(@ModelAttribute EmailDTO dto, MemberDTO dto2, Model model) {
	String receiveMail=dto2.getEmail();
		try {
			dto.setReceiveMail(receiveMail);
			emailService.sendMail(dto);
			model.addAttribute("receiveMail", dto.getReceiveMail());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "register/register";
	}
	
	@RequestMapping("logout.do")
	public ModelAndView logout(HttpSession session, ModelAndView mav, Model model) {

		List<ProductDTO> list = productService.home_list();
		model.addAttribute("list", list);
		System.out.println("로그아웃-상품리스트출력"+list);
		
		List<ProductDTO> men_list = productService.man_list();
		model.addAttribute("mlist",men_list);
		System.out.println("섭로로는 남자다!!!!!"+men_list);
		
		List<ProductDTO> woman_list = productService.woman_list();
		model.addAttribute("wlist",woman_list);
		System.out.println("섭로로는 여자다!!!!"+woman_list);
		
		List<ProductDTO> public_list = productService.woman_list();
		model.addAttribute("plist",public_list);
		System.out.println("섭로로는 공룡이다!!!!"+woman_list);
		
		memberService.logout(session);
		mav.setViewName("home");
		mav.addObject("message","logout");
		return mav;
	}

	@ResponseBody
	@RequestMapping(value="idcheck.do", produces="application/text;charset=utf8")
	public String searchid(String id) {
		System.out.println("아이디중복체크");
		String result="";
		String message="";
		result=memberService.idcheck(id);
		if(result == null) {
			message="사용가능한　　　　아이디입니다.";
		}else {
			message="사용불가능한　　　아이디입니다.";
		}
		return message;
	}

	@RequestMapping("join.do")
	public String join(MemberDTO dto, HttpSession session,Model model) {
		System.out.println("회원가입정보1"+dto);
		memberService.join(dto);
		System.out.println("회원가입정보2"+dto);

		String id=dto.getId();
		String name=dto.getMname();
		String email=dto.getEmail();
		String phone1=dto.getPhone1();
		String zip_num1=dto.getZip_num1();
		String address1=dto.getAddress1();
		
		System.out.println("1234"+id);
		System.out.println("1234"+name);
		System.out.println("1234"+phone1);
		
		
		model.addAttribute("id",id);
		model.addAttribute("name",name);
		model.addAttribute("email",email);
		model.addAttribute("phone1",phone1);
		model.addAttribute("zip_num1",zip_num1);
		model.addAttribute("address1",address1);
		
		
		
		return "joinone/join";
	}
	
	@RequestMapping("index.do")
	public String index() {
		return "home";
	}
	
	@RequestMapping("mypage.do")
	public String mypage() {
		return "mypage/mypage";
	}
	
	@RequestMapping("memberupdate1.do")
	public String memberupdate1() {
		return "mypage/mypageupdate1";
	}
	
	@RequestMapping("memberupdate2.do")
	public String memberupdate2() {
		return "mypage/mypageupdate2";
	}
	
	@RequestMapping("memberupdate3.do")
	public String memberupdate3() {
		return "mypage/mypageupdate2";
	}
	
	
	@RequestMapping("memberupdate11.do")
	public String memberupdate1(MemberDTO dto, HttpSession session) {
		memberService.memberupdate1(dto, session);
		return "redirect:mypage.do";
	}
	
	@RequestMapping("memberupdate22.do")
	public String memberupdate2(MemberDTO dto, HttpSession session) {
//		System.out.println("바뀐주소100000"+dto);
		memberService.memberupdate2(dto, session);
//		System.out.println("바뀐주소111111"+dto);
		return "redirect:mypage.do";
	}
	
	@RequestMapping("memberupdate33.do")
	public String memberupdate3(MemberDTO dto, HttpSession session) {
		//System.out.println("바뀐주소1"+dto);
		memberService.memberupdate3(dto, session);
		//System.out.println("바뀐주소2"+dto);
		return "redirect:mypage.do";
	}
	
	@RequestMapping("find.do")
	public String find() {
		return "find/findid";
	}
	
	@RequestMapping("findid.do")
	public ModelAndView findid(MemberDTO dto) {
		
		String name=dto.getMname();
		String[] items1=name.split(",");
		name=items1[0];
		dto.setMname(name);
		
		String email=dto.getEmail();
		String[] items2=email.split(",");
		email=items2[0];
		dto.setEmail(email);
		
		//System.out.println("아이디찾기"+dto);
		String result=memberService.memberfindid(dto);
		ModelAndView mav = new ModelAndView();
		System.out.println("검색된아이디"+result);
		if( result == null) {
			mav.setViewName("find/findidlogin");
			mav.addObject("message", "notfind");
		} else {
			mav.setViewName("find/findidlogin");
			mav.addObject("message", "findid");
			mav.addObject("result", result);
		}
		return mav;
	}
	
	@RequestMapping("findpwdEC.do")
	public String findpwdEC() {
		return "find/findpwdEC";
	}
	@RequestMapping("FindPwdEmailCheck.do")
	public String findpwdEmailCheck(MemberDTO dto,Model model) {
		memberService.temporaryPW(dto);
		model.addAttribute("id",dto.getId());
		model.addAttribute("passwd",dto.getPasswd());
		dto.getPasswd();
		return "find/findpwdlogin";
	}
	
	@RequestMapping("findpwd.do")
	public ModelAndView findpwd(MemberDTO dto) {
		System.out.println(dto);
		
		String name=dto.getMname();
		String[] items1=name.split(",");
		name=items1[1];
		dto.setMname(name);
		
		String email=dto.getEmail();
		String[] items2=email.split(",");
		email=items2[1];
		dto.setEmail(email);
		
		System.out.println("비번찾기 : "+dto);
		String result=memberService.memberfindpwd(dto);
		ModelAndView mav = new ModelAndView();
		System.out.println("검색된비밀번호"+result);
		if( result == null) {
			mav.setViewName("find/findpwdEC");
			mav.addObject("message", "notfind");
		} else {
			mav.setViewName("find/findpwdEC");
			mav.addObject("message", "findpwd");
			mav.addObject("result", result);
		}
		return mav;
	}	
	
	@RequestMapping("deletepage.do")
	public String deletepage() {
		return "register/deletepage";
	}
	
	@RequestMapping("delete.do")
	public String delete(String id, String passwd,Model model) {
		boolean result=memberService.deletecheck(id,passwd);
		if(result) {
			memberService.deleteEnd(id);
			return "redirect:/member/deleteEnd.do";
		}else {
			model.addAttribute("message","비밀번호를 다시 확인해 주시기 바랍니다.");
			model.addAttribute("dto",memberService.viewMember(id));
		return "register/deletepage";
		}
	}
	
	@RequestMapping("deleteEnd.do")
	public ModelAndView deleteEnd(HttpSession session, ModelAndView mav) {
		memberService.logout(session);
		mav.setViewName("register/deleteEnd");
		mav.addObject("message","delete");
		return mav;
	}


	@RequestMapping("test1.do")
	public String test1() {
		return "find/find_id";
	}
	
	@RequestMapping("test.do")
	public String test() {
		return "joinone/join";
	}

	@RequestMapping("joinview.do")
	public ModelAndView joinview(ModelAndView mav, MemberDTO dto, @RequestParam String id) {
		List<MemberDTO> list = memberDao.joinview(dto);
				
		mav.setViewName("joinone/join");
		mav.addObject("list", list);
		System.out.println("JOINVIEW리스트:" + list);
		return mav;
		
		
		
		
		
	}
}