<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script src="http://code.jquery.com/jquery-3.2.1.min.js"></script>
<script>
	function daumZipCode() {
		new daum.Postcode(
				{
					oncomplete : function(data) {
						// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

						// 각 주소의 노출 규칙에 따라 주소를 조합한다.
						// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
						var fullAddr = ''; // 최종 주소 변수
						var extraAddr = ''; // 조합형 주소 변수

						// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
						if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
							fullAddr = data.roadAddress;

						} else { // 사용자가 지번 주소를 선택했을 경우(J)
							fullAddr = data.jibunAddress;
						}

						// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
						if (data.userSelectedType === 'R') {
							//법정동명이 있을 경우 추가한다.
							if (data.bname !== '') {
								extraAddr += data.bname;
							}
							// 건물명이 있을 경우 추가한다.
							if (data.buildingName !== '') {
								extraAddr += (extraAddr !== '' ? ', '
										+ data.buildingName : data.buildingName);
							}
							// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
							fullAddr += (extraAddr !== '' ? ' (' + extraAddr
									+ ')' : '');
						}

						// 우편번호와 주소 정보를 해당 필드에 넣는다.
						document.getElementById('zip_num').value = data.zonecode; //5자리 새우편번호 사용
						document.getElementById('address1').value = fullAddr;

						// 커서를 상세주소 필드로 이동한다.
						document.getElementById('address2').focus();
					}
				}).open();
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
	
	// 상품 등록 유효성검사
	$("#addBtn").click(function() {
		var dseq= $("#dseq").val();
		var dname = $("#dname").val();
		var phone = $("#phone").val();
		var zip_num = $("#zip_num").val();
		var address1 = $("#address1").val();
		var address2 = $("#address2").val();
		

		if (dseq == "") {
			alert("상품명을 입력해주세요");
			dseq.foucs();
		} else if (dname == "") {
			alert("상품 판매가를 입력해주세요");
			dname.focus();
		} else if (phone == "") {
			alert("상품 원가를 입력해주세요");
			phone.focus();
		} else if (zip_num == "") {
			alert("상품 분류코드를 입력해주세요");
			zip_num.focus();
		} else if (address1 == "") {
			alert("상품 설명을 입력해주세요");
			address1.focus();
		}else if (address2 == "") {
			alert("상품 설명을 입력해주세요");
			address2.focus();
		}
		// 상품 정보 전송
		document.form1.action = "${path}/project04/admin_dealer/dealer_update.do";
		document.form1.submit();
	});
	// 상품 목록이동
	$("#listBtn").click(function() {
		location.href = '${path}/project04/admin_dealer/list.do';
	});
});
</script>

<head>
<title>Admin Home Page</title>

<%@ include file="../include/header.jsp"%>
<%@ include file="../include/AdminHeader.jsp"%>



<div class="row-fluid">
	<div class="span6">
		<!-- block -->
		<div class="block">
			<div class="navbar navbar-inner block-header">
				<div class="muted pull-left">거래처수정</div>
				<div class="pull-right">
					<span class="badge badge-info"></span>

				</div>
			</div>
			<div class="block-content collapse in">
				
				<form name="form1" method="post">
				<table class="table table-striped">
					<thead>
							
							<td align="center">
							
							<tr>
							<th>거래처코드</th>
							<td><input  name="dseq" id="dseq" value="${dto.dseq}" ></td>
							</tr>
							<tr>
							<th>거래처명</th>
							<td><input type="text" name="dname" id="dname" value="${dto.dname}" ></td>
							</tr>
							<tr>
							<th>전화번호</th>
							<td><input type="text" name="phone" id="phone" value="${dto.phone}"></td>
							</tr>
							<tr>
							<th>우편번호</th>
							<td><input type="text" name="zip_num" id="zip_num" onclick="daumZipCode()" value="${dto.zip_num}"></td>
							</tr>
							<tr>
							<th>주소</th>
							<td><input  name="address1" id="address1" value="${dto.address1}" ></td>
							</tr>
							<tr>
							<th>상세주소</th>
							<td><input type="text" name="address2" id="address2" value="${dto.address2}"></td>
							</tr>
							
							<tr>
							<td colspan="2" align="center">
								<input type="hidden" value="${dto.dseq }" name="dseq" id="dseq">
								<input type="button" value="수정" id="addBtn">
                				<input type="button" value="목록" id="listBtn">
								
							</td>

						</tr>
					</thead>
					
				</table>
				</form>
			</div>
		</div>
		<!-- /block -->
	</div>
	
<hr>
<footer>
	
</footer>
</div>
<!--/.fluid-container-->
<script
	src="${path}/resources/Bootstrap-Admin/vendors/jquery-1.9.1.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/bootstrap/js/bootstrap.min.js"></script>
<script
	src="${path}/resources/Bootstrap-Admin/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
<script src="${path}/resources/Bootstrap-Admin/assets/scripts.js"></script>
<script>
	$(function() {
		// Easy pie charts
		$('.chart').easyPieChart({
			animate : 1000
		});
	});
</script>
</body>

</html>