package com.example.project04.service.pro_admin;

import java.util.List;

import com.example.project04.model.admin_newproduct.dto.Admin_NewProductDTO;
import com.example.project04.model.pro_admin.dto.Admin_ProductDTO;

public interface Admin_ProductService {
	List<Admin_ProductDTO> listProduct(int start,int end);
	public Admin_ProductDTO detailProduct(int pseq);
	public void insertProduct(Admin_ProductDTO dto);
	public Admin_ProductDTO viewProduct(int pseq);
	public void deleteProduct(int pseq);
	public void updateProduct(Admin_ProductDTO dto);
	public String fileInfo(int pseq);
	public int countArticle() throws Exception;
	public Admin_ProductDTO orderProduct(int pseq);
	public List<Admin_ProductDTO> ordermoney();
	public void deleteOrder(int pseq);
	public List<Admin_ProductDTO> needlist();
	
	
}
