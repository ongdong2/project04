package com.example.project04.service.admin_member;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.example.project04.model.admin_member.dao.Admin_MemberDAO;
import com.example.project04.model.admin_member.dto.Admin_MemberDTO;

@Service
public class Admin_MemberServiceImpl implements Admin_MemberService {
	@Inject
	Admin_MemberDAO memberDao;

	@Override
	public List<Admin_MemberDTO> listMember(int start,int end) {
		// TODO Auto-generated method stub
		return memberDao.listMember(start,end);
	}

	@Override
	public void insertmember(Admin_MemberDTO dto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Admin_MemberDTO viewMember(String id) {
		// TODO Auto-generated method stub
		return memberDao.viewMember(id);
	}

	@Override
	public void deletemember(String id) {
		memberDao.deletemember(id);
		
	}

	@Override
	public void updateMember(Admin_MemberDTO dto) {
		memberDao.updateMember(dto);
		
	}

	@Override
	public boolean checkPw(String id, String passwd) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int countArticle() throws Exception {
		// TODO Auto-generated method stub
		return memberDao.countArticle();
	}

}
