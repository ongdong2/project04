package com.example.project04.controller.product;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.project04.model.dealer.dto.DealerDTO;
import com.example.project04.service.dealer.DealerService;
import com.example.project04.service.product.BrandService;

@Controller
@RequestMapping("brand/*")
public class BrandController {

	@Inject
	BrandService brandService;
	@Inject
	DealerService dealerService;
	
	@RequestMapping("nike.do")
	public ModelAndView list1(ModelAndView mav) {
		mav.setViewName("brand/nike_list");
		mav.addObject("nike", brandService.listBrand1());
		return mav;
	}
	
	@RequestMapping("fila.do")
	public ModelAndView list2(ModelAndView mav) {
		mav.setViewName("brand/fila_list");
		mav.addObject("fila", brandService.listBrand2());
		return mav;
	}
	
	@RequestMapping("kolca.do")
	public ModelAndView list3(ModelAndView mav) {
		mav.setViewName("brand/kolca_list");
		mav.addObject("kolca", brandService.listBrand3());
		return mav;
	}
	
	@RequestMapping("adidas.do")
	public ModelAndView list4(ModelAndView mav) {
		mav.setViewName("brand/adidas_list");
		mav.addObject("adidas", brandService.listBrand4());
		return mav;
	}
	
	@RequestMapping("puma.do")
	public ModelAndView list5(ModelAndView mav) {
		mav.setViewName("brand/puma_list");
		mav.addObject("puma", brandService.listBrand5());
		return mav;
	}
	
	@RequestMapping("descente.do")
	public ModelAndView list6(ModelAndView mav) {
		mav.setViewName("brand/descente_list");
		mav.addObject("descente", brandService.listBrand6());
		return mav;
	}
	
	@RequestMapping("vans.do")
	public ModelAndView list7(ModelAndView mav) {
		mav.setViewName("brand/vans_list");
		mav.addObject("vans", brandService.listBrand7());
		return mav;
	}
	
	@RequestMapping("converse.do")
	public ModelAndView list8(ModelAndView mav) {
		mav.setViewName("brand/converse_list");
		mav.addObject("converse", brandService.listBrand8());
		return mav;
	}
	
	@RequestMapping("excelsior.do")
	public ModelAndView list9(ModelAndView mav) {
		mav.setViewName("brand/excelsior_list");
		mav.addObject("excelsior", brandService.listBrand9());
		return mav;
	}
	
	@RequestMapping("newbalance.do")
	public ModelAndView list10(ModelAndView mav) {
		mav.setViewName("brand/newbalance_list");
		mav.addObject("newbalance", brandService.listBrand10());
		return mav;
	}
	
	@RequestMapping("men.do")
	public ModelAndView list11(ModelAndView mav) {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/men_list");
		mav.addObject("men", brandService.listBrand11());
		return mav;
	}
	
	@RequestMapping("women.do")
	public ModelAndView list12(ModelAndView mav) {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/women_list");
		mav.addObject("women", brandService.listBrand12());
		return mav;
	}
	@RequestMapping("unisex.do")
	public ModelAndView list13(ModelAndView mav) {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/unisex_list");
		mav.addObject("unisex", brandService.listBrand13());
		return mav;
	}
	
	@RequestMapping("catg1.do")
	public ModelAndView catg1(ModelAndView mav, int dseq) throws Exception {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/catg1_list");
		mav.addObject("dseq", dseq);
		mav.addObject("catg", brandService.listCatg1(dseq));
		return mav;
	}
	
	@RequestMapping("catg2.do")
	public ModelAndView catg2(ModelAndView mav, int dseq) throws Exception {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/catg2_list");
		mav.addObject("dseq", dseq);
		mav.addObject("catg", brandService.listCatg2(dseq));
		return mav;
	}
	
	@RequestMapping("catg3.do")
	public ModelAndView catg3(ModelAndView mav, int dseq) throws Exception {
		List<DealerDTO> list=dealerService.listDealer();
		mav.addObject("dealer", list);
		mav.setViewName("brand/catg3_list");
		mav.addObject("dseq", dseq);
		mav.addObject("catg", brandService.listCatg3(dseq));
		return mav;
	}
	
	@RequestMapping("dseq1.do")
	public ModelAndView dseq1(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq1_list");
		mav.addObject("dseq", brandService.listDseq1(sort));
		return mav;
	}
	
	@RequestMapping("dseq2.do")
	public ModelAndView dseq2(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq2_list");
		mav.addObject("dseq", brandService.listDseq2(sort));
		return mav;
	}
	
	@RequestMapping("dseq3.do")
	public ModelAndView dseq3(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq3_list");
		mav.addObject("dseq", brandService.listDseq3(sort));
		return mav;
	}
	
	@RequestMapping("dseq4.do")
	public ModelAndView dseq4(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq4_list");
		mav.addObject("dseq", brandService.listDseq4(sort));
		return mav;
	}
	
	@RequestMapping("dseq5.do")
	public ModelAndView dseq5(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq5_list");
		mav.addObject("dseq", brandService.listDseq5(sort));
		return mav;
	}
	
	@RequestMapping("dseq6.do")
	public ModelAndView dseq6(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq6_list");
		mav.addObject("dseq", brandService.listDseq6(sort));
		return mav;
	}
	
	@RequestMapping("dseq7.do")
	public ModelAndView dseq7(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq7_list");
		mav.addObject("dseq", brandService.listDseq7(sort));
		return mav;
	}
	
	@RequestMapping("dseq8.do")
	public ModelAndView dseq8(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq8_list");
		mav.addObject("dseq", brandService.listDseq8(sort));
		return mav;
	}
	
	@RequestMapping("dseq9.do")
	public ModelAndView dseq9(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq9_list");
		mav.addObject("dseq", brandService.listDseq9(sort));
		return mav;
	}
	
	@RequestMapping("dseq10.do")
	public ModelAndView dseq10(ModelAndView mav, String sort) throws Exception {
		mav.setViewName("brand/dseq10_list");
		mav.addObject("dseq", brandService.listDseq10(sort));
		return mav;
	}
}
